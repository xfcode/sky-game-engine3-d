﻿import sys


def get_project_root_dir():
    project_path: str = sys.argv[0]
    project_path = project_path[0: project_path.find('Script\\DevTool')]
    return project_path
