import os
from DevTool.Common.Paths import get_project_root_dir

project_path: str = get_project_root_dir()

print('project_path: ' + project_path)

module_name = input('ModuleName: ')

src_path = project_path + '/Engine/Src/Runtime'
cmake_template_path = project_path + '/Script/CMake/CMakeLists.txt'

with open(cmake_template_path, 'r', encoding='utf8') as f:
    cmake_template_str = f.read()

cmake_list_content = cmake_template_str.replace('@ModuleName@', module_name)

module_cpp_name = module_name + 'ModuleInterface.cpp'
module_h_name = module_name + 'ModuleInterface.h'
cmake_list_content = cmake_list_content.replace('@ModuleCPPFile@', '{module_cpp} {module_h}'.format(
    module_cpp=module_cpp_name, module_h=module_h_name
))

module_path_dir = src_path + '/' + module_name
os.mkdir(module_path_dir)
os.mkdir(module_path_dir + '/Private')
os.mkdir(module_path_dir + '/Public')

with open(module_path_dir + '/CMakeLists.txt', 'w', encoding='utf8') as f:
    f.write(cmake_list_content)

with open(module_path_dir + '/Private/' + module_cpp_name, 'w', encoding='utf8') as f:
    pass

with open(module_path_dir + '/Public/' + module_h_name, 'w', encoding='utf8') as f:
    pass
