import os
import sys

module_path = sys.argv[1]
module_name = sys.argv[2]

module_def_file_path = module_path + '/' + module_name + 'ModuleDef.h'
has_def_file = os.path.exists(module_def_file_path)
if not has_def_file:
    file = open(module_def_file_path, 'w')
    file.close()
    print('create module def file' + module_def_file_path)
