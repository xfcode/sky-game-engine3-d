import sys
import io

SlnFilePath = sys.argv[1]
NewSlnFilePath = sys.argv[2]

print('---------------BuildToolPy ------------------------')
print('SlnFilePath: ', SlnFilePath)
print('NewSlnFilePath: ', NewSlnFilePath)

SlnFileContnet = ''
with io.open(SlnFilePath, mode='r', encoding = 'utf-8') as F:
    SlnFileContnet = F.read() # .decode('utf-8')
    SlnFileContnet = SlnFileContnet.replace('"Engine\\', '"Build\\Engine\\')
    SlnFileContnet = SlnFileContnet.replace('ALL_BUILD.vcxproj', 'Build\\ALL_BUILD.vcxproj')
    SlnFileContnet = SlnFileContnet.replace('INSTALL.vcxproj', 'Build\\INSTALL.vcxproj')
    SlnFileContnet = SlnFileContnet.replace('ZERO_CHECK.vcxproj', 'Build\\ZERO_CHECK.vcxproj')

with io.open(NewSlnFilePath, mode='w', encoding = 'utf-8') as F:
    F.write(SlnFileContnet)

print('BuildToolPy Done')
