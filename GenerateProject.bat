cmake -G "Visual Studio 17 2022" -A x64 -S . -B Build
.\Script\Python\Scripts\python.exe .\Script\BuildTool\BuildTool.py "./Build/SkyGameEngine3d.sln" "./SkyGameEngine3d.sln"
@cmake --graphviz=./Engine/Build/Dot/Project.dot
@dot -Tpng -o ./Engine/Build/Dot/Project.png ./Engine/Build/Dot/Project.dot 
@echo off
IF NOT ERRORLEVEL 0 (
    ECHO An error occurred!
    PAUSE
)
PAUSE