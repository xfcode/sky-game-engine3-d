#pragma once

#include "SpriteBatch.h"

#include <cstddef>
#include <cstdint>
#include <memory>


namespace DirectX
{
    inline namespace DX12
    {
        class SpriteFont
        {
        public:
            struct Glyph;

            SpriteFont(ID3D12Device* device, ResourceUploadBatch& upload,
                       _In_z_ const wchar_t* fileName,
                       D3D12_CPU_DESCRIPTOR_HANDLE cpuDescriptorDest, D3D12_GPU_DESCRIPTOR_HANDLE gpuDescriptor,
                       bool forceSRGB = false);
            SpriteFont(ID3D12Device* device, ResourceUploadBatch& upload,
                       _In_reads_bytes_(dataSize) const uint8_t* dataBlob, size_t dataSize,
                       D3D12_CPU_DESCRIPTOR_HANDLE cpuDescriptorDest, D3D12_GPU_DESCRIPTOR_HANDLE gpuDescriptor,
                       bool forceSRGB = false);
            SpriteFont(D3D12_GPU_DESCRIPTOR_HANDLE texture, XMUINT2 textureSize,
                       _In_reads_(glyphCount) const Glyph* glyphs, size_t glyphCount, float lineSpacing);

            SpriteFont(SpriteFont&&) noexcept;
            SpriteFont& operator=(SpriteFont&&) noexcept;

            SpriteFont(const SpriteFont&) = delete;
            SpriteFont& operator=(const SpriteFont&) = delete;

            virtual ~SpriteFont();

            // Wide-character / UTF-16LE
            void XM_CALLCONV DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ const wchar_t* text,
                                        const XMFLOAT2& position, FXMVECTOR color = Colors::White, float rotation = 0,
                                        const XMFLOAT2& origin = Float2Zero, float scale = 1,
                                        SpriteEffects effects = SpriteEffects_None, float layerDepth = 0) const;
            void XM_CALLCONV DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ const wchar_t* text,
                                        const XMFLOAT2& position, FXMVECTOR color, float rotation,
                                        const XMFLOAT2& origin, const XMFLOAT2& scale,
                                        SpriteEffects effects = SpriteEffects_None, float layerDepth = 0) const;
            void XM_CALLCONV DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ const wchar_t* text, FXMVECTOR position,
                                        FXMVECTOR color = Colors::White, float rotation = 0,
                                        FXMVECTOR origin = g_XMZero, float scale = 1,
                                        SpriteEffects effects = SpriteEffects_None, float layerDepth = 0) const;
            void XM_CALLCONV DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ const wchar_t* text, FXMVECTOR position,
                                        FXMVECTOR color, float rotation, FXMVECTOR origin, GXMVECTOR scale,
                                        SpriteEffects effects = SpriteEffects_None, float layerDepth = 0) const;

            XMVECTOR XM_CALLCONV MeasureString(_In_z_ const wchar_t* text, bool ignoreWhitespace = true) const;

            RECT __cdecl MeasureDrawBounds(_In_z_ const wchar_t* text, const XMFLOAT2& position,
                                           bool ignoreWhitespace = true) const;
            RECT XM_CALLCONV MeasureDrawBounds(_In_z_ const wchar_t* text, FXMVECTOR position,
                                               bool ignoreWhitespace = true) const;

            // UTF-8
            void XM_CALLCONV DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ const char* text,
                                        const XMFLOAT2& position, FXMVECTOR color = Colors::White, float rotation = 0,
                                        const XMFLOAT2& origin = Float2Zero, float scale = 1,
                                        SpriteEffects effects = SpriteEffects_None, float layerDepth = 0) const;
            void XM_CALLCONV DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ const char* text,
                                        const XMFLOAT2& position, FXMVECTOR color, float rotation,
                                        const XMFLOAT2& origin, const XMFLOAT2& scale,
                                        SpriteEffects effects = SpriteEffects_None, float layerDepth = 0) const;
            void XM_CALLCONV DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ const char* text, FXMVECTOR position,
                                        FXMVECTOR color = Colors::White, float rotation = 0,
                                        FXMVECTOR origin = g_XMZero, float scale = 1,
                                        SpriteEffects effects = SpriteEffects_None, float layerDepth = 0) const;
            void XM_CALLCONV DrawString(_In_ SpriteBatch* spriteBatch, _In_z_ const char* text, FXMVECTOR position,
                                        FXMVECTOR color, float rotation, FXMVECTOR origin, GXMVECTOR scale,
                                        SpriteEffects effects = SpriteEffects_None, float layerDepth = 0) const;

            XMVECTOR XM_CALLCONV MeasureString(_In_z_ const char* text, bool ignoreWhitespace = true) const;

            RECT __cdecl MeasureDrawBounds(_In_z_ const char* text, const XMFLOAT2& position,
                                           bool ignoreWhitespace = true) const;
            RECT XM_CALLCONV MeasureDrawBounds(_In_z_ const char* text, FXMVECTOR position,
                                               bool ignoreWhitespace = true) const;

            // Spacing properties
            float __cdecl GetLineSpacing() const noexcept;
            void __cdecl SetLineSpacing(float spacing);

            // Font properties
            wchar_t __cdecl GetDefaultCharacter() const noexcept;
            void __cdecl SetDefaultCharacter(wchar_t character);

            bool __cdecl ContainsCharacter(wchar_t character) const;

            // Custom layout/rendering
            const Glyph* __cdecl FindGlyph(wchar_t character) const;
            D3D12_GPU_DESCRIPTOR_HANDLE __cdecl GetSpriteSheet() const noexcept;
            XMUINT2 __cdecl GetSpriteSheetSize() const noexcept;

            // Describes a single character glyph.
            struct Glyph
            {
                uint32_t Character;
                RECT Subrect;
                float XOffset;
                float YOffset;
                float XAdvance;
            };

        private:
            // Private implementation.
            class Impl;

            std::unique_ptr<Impl> pImpl;

            static const XMFLOAT2 Float2Zero;
        };
    }
}
