#pragma once

#include "HAL/PlatformDefine.h"

#ifdef ASSETCORE
    #define ASSETCORE_API DLL_EXPORT
#else
    #define ASSETCORE_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameAssetCore = L"AssetCore";
