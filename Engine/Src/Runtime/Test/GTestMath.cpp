#include "gtest/gtest.h"
#include "Math/Vector.h"
#include "Math/Matrix.h"
#include "Math/SimpleShape.h"

namespace TestMath
{
    TEST(VectorTest, Vector4D)
    {
        using namespace SkyGameEngine3d;
        Vector4D Value(1, 2, 3, 4);
        EXPECT_EQ(MathLib::IsEq(Value.GetLength(), 5.47722557, 0.000001), true);
        EXPECT_EQ(MathLib::IsEq(Value.GetLengthSquared(), 30, 0.000001), true);

        Vector4D TestValue(1, 2, 3, 4);
        EXPECT_EQ(TestValue == Value, true);
        EXPECT_EQ(TestValue != Value, false);

        Vector4D TestOpValue(2, 4, 6, 8);
        EXPECT_EQ((TestValue + Value) == TestOpValue, true);
        EXPECT_EQ((TestOpValue - Value) == TestValue, true);

        Vector4D TestOpScalarValue(3, 6, 9, 12);
        EXPECT_EQ(Value * 3 == TestOpScalarValue, true);
        EXPECT_EQ(TestOpScalarValue / 3 == Value, true);
    }

    TEST(MatTest, Mat4X4)
    {
        using namespace SkyGameEngine3d;
        Matrix4X4 Mat{
            { 2, 1, 2 },
            { 2, 1, 2 },
            { 2, 1, 2 },
            { 2, 1, 2 },
        };

        auto Ret = Mat * Mat;
        auto Vec = Ret.GetCompVector<0>();
        EXPECT_EQ((Vec == Vector4D{2 * 2 + 2 * 1 + 2 * 2, 2 + 1 + 2, 2 * 2 + 2 * 1 + 2 * 2, 0}), true);
    }

    TEST(Box3D, SplitBox)
    {
        using namespace SkyGameEngine3d;

        const Box3D TestBox{
            Vector3D(10, 10, 10),
            Vector3D(70, 70, 70),
        };

        const auto Ret = Box3D::SplitBox<2, 2, 2>(TestBox);

        EXPECT_EQ(Ret[0].GetMin() == Vector3D(10, 10, 10), true);
        EXPECT_EQ(Ret[1].GetMin() == Vector3D(10, 10, 30), true);
        EXPECT_EQ(Ret[2].GetMin() == Vector3D(10, 10, 50), true);


        EXPECT_EQ(Ret[3].GetMin() == Vector3D(10, 30, 10), true);
        EXPECT_EQ(Ret[4].GetMin() == Vector3D(10, 30, 30), true);
        EXPECT_EQ(Ret[5].GetMin() == Vector3D(10, 30, 50), true);
    }

}
