﻿#include "gtest/gtest.h"
#include "Misc/Memory.h"

using namespace SkyGameEngine3d;

TEST(MemoryLibTest, AlignUpTest)
{
    {
        constexpr uint64_t Addr = 126;
        const auto AlignAddr = MemoryLib::AlignUp(Addr, 128);
        ASSERT_EQ(AlignAddr, 128);
    }
    
    {
        constexpr uint64_t Addr = 243;
        const auto AlignAddr = MemoryLib::AlignUp(Addr, 128);
        ASSERT_EQ(AlignAddr, 256);
    }
}


TEST(MemoryLibTest, AlignDownTest)
{
    {
        constexpr uint64_t Addr = 126;
        const auto AlignAddr = MemoryLib::AlignDown(Addr, 128);
        ASSERT_EQ(AlignAddr, 0);
    }
    
    {
        constexpr uint64_t Addr = 243;
        const auto AlignAddr = MemoryLib::AlignDown(Addr, 128);
        ASSERT_EQ(AlignAddr, 128);
    }
}