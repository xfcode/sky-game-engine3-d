﻿#include <filesystem>

#include "gtest/gtest.h"
#include "Texture/Public/ImageLoader.h"

using namespace SkyGameEngine3d;
using namespace std;

filesystem::path RootPath = L"Z:/MyProject/sky-game-engine3-d";

TEST(ImageLoaderTest, LoadNPGNoFile)
{
    const auto Manager = CImageLoaderManager::GetInstance();
    std::wstring OutErrorStr;
    const auto Loader = Manager->LoadTextureByFile(filesystem::path{ L"dddddd.png" }, OutErrorStr);
    ASSERT_EQ(Loader, nullptr);
    ASSERT_TRUE(!OutErrorStr.empty()) << OutErrorStr;
}


TEST(ImageLoaderTest, LoadNPGFile8)
{
    const auto Manager = CImageLoaderManager::GetInstance();
    std::wstring OutErrorStr;
    const auto Loader = Manager->LoadTextureByFile(
        RootPath / L"Engine/Resource/TestImage_8.png",
        OutErrorStr
    );

    ASSERT_TRUE(Loader != nullptr) << OutErrorStr;
    ASSERT_TRUE(OutErrorStr.empty()) << OutErrorStr;

    ASSERT_TRUE(Loader->GetRawDataPixelFormat() == ETextureRawDataPixelFormat::RGBA8)
            << L"RawDataPixelFormat 格式不符合预期";

    std::vector<uint8_t> OutRawData;
    Loader->GetSubRawData(OutRawData);
    ASSERT_TRUE(!OutRawData.empty()) << L"不应该没有数据";
    uint32_t Index = 1;
    for (auto Value : OutRawData)
    {
        if (Index % 4 == 0)
        {
            ASSERT_EQ(Value, 255) << L"A- Channel must is 255 : " << static_cast<uint32_t>(Value);
        }
        else
        {
            ASSERT_EQ(Value, 128) << L"不应该有其他颜色值 : " << Value;
        }
        Index++;
    }
}

TEST(ImageLoaderTest, LoadNPGFile16)
{
    const auto Manager = CImageLoaderManager::GetInstance();
    std::wstring OutErrorStr;
    const auto Loader = Manager->LoadTextureByFile(
        RootPath / L"Engine/Resource/TestImage_16.png",
        OutErrorStr
    );

    ASSERT_TRUE(Loader != nullptr) << OutErrorStr;
    ASSERT_TRUE(OutErrorStr.empty()) << OutErrorStr;

    ASSERT_TRUE(Loader->GetRawDataPixelFormat() == ETextureRawDataPixelFormat::RGBA16)
            << L"RawDataPixelFormat 格式不符合预期";

    std::vector<uint8_t> OutRawData;
    Loader->GetSubRawData(OutRawData);
    ASSERT_TRUE(!OutRawData.empty()) << L"不应该没有数据";
    uint32_t Index = 0;
    const auto Start = reinterpret_cast<uint16_t*>(OutRawData.data());
    for (; Index < OutRawData.size() / 2; Index++)
    {
        auto Value = static_cast<float>(*(Start + Index));

        if (Index % 4 == 3)
        {
            ASSERT_NEAR(Value / std::numeric_limits<uint16_t>::max(), 0.5, 0.01)
                    << L"A- Channel must is 1 : " << static_cast<uint32_t>(Value);
        }
        else if (Index % 4 == 0)
        {
            ASSERT_NEAR(Value / std::numeric_limits<uint16_t>::max(),
                        1,
                        0.01
            ) << L" R - Channel must is 1 : " << Value;
        }
        else
        {
            ASSERT_EQ(Value, 0) << L"GB - Channel must is 0 " << Value;
        }
    }
}

TEST(ImageLoaderTest, LoadDdsFile)
{
    const auto Manager = CImageLoaderManager::GetInstance();
    std::wstring OutErrorStr;
    const auto Loader = Manager->LoadTextureByFile(
        RootPath / L"Engine/Resource/TestImage_16.dds",
        OutErrorStr
    );

    ASSERT_TRUE(Loader != nullptr) << OutErrorStr;
    ASSERT_TRUE(OutErrorStr.empty()) << OutErrorStr;

    const auto& ImageInfo = Loader->GetImageInfo();
    EXPECT_EQ(ImageInfo.ArrayCount, 1) << "Image Array Count == 1, Now Is : " << ImageInfo.ArrayCount;
    EXPECT_EQ(ImageInfo.MipmapCount, 11) << "Image Mipmap Count == 11, Now Is : " << ImageInfo.MipmapCount;
    EXPECT_EQ(ImageInfo.RawDataPixelFormat, ETextureRawDataPixelFormat::BC7)
            << "Image RawDataPixelFormat == BC7, Now Is : "
            << static_cast<uint32_t>(ImageInfo.RawDataPixelFormat);

    {
        std::vector<uint8_t> OutData;
        const bool IsOk = Loader->GetRawData(OutData);
        EXPECT_TRUE(IsOk) << "GetRawData must succeed";
        if (IsOk)
        {
            EXPECT_TRUE(!OutData.empty());
        }
    }

    {
        std::vector<uint8_t> OutData;
        const bool IsOk = Loader->GetSubRawData(OutData, 5, 0);
        EXPECT_TRUE(IsOk) << "GetSubRawData must succeed";
        if (IsOk)
        {
            const auto ExpectSize = ImageHelper::CalculateImageSize(Loader->GetImageInfo(), 5);
            EXPECT_EQ(OutData.size(), ExpectSize);
        }
    }
    
    {
        std::vector<uint8_t> OutData;
        const bool IsOk = Loader->GetSubRawData(OutData, 500, 0);
        EXPECT_FALSE(IsOk);
    }
}

// https://learn.microsoft.com/en-us/windows/win32/direct3ddds/dds-file-layout-for-textures
namespace ImageHelperTest
{
    TEST(CalculateImageSize, BC1)
    {
        constexpr CImageInfo ImageInfo
        {
            .MipmapCount = 10,
            .ArrayCount = 5,
            .RawDataPixelFormat = ETextureRawDataPixelFormat::BC1,
            .Width = 256,
            .Height = 64,
            .Depth = 1,
            .ImageDimension = CImageInfo::Dimension2D, 
        };

        uint32_t Size = 0;
        {
            Size = ImageHelper::CalculateImageSize(ImageInfo, 0);
            EXPECT_EQ(Size, 8192);
        }

        {
            Size = ImageHelper::CalculateImageSize(ImageInfo, 1);
            EXPECT_EQ(Size, 2048);
        }

        {
            Size = ImageHelper::CalculateImageSize(ImageInfo, 8);
            EXPECT_EQ(Size, 8);
        }
    }

    TEST(CalculateImageSize, BC3)
    {
        constexpr CImageInfo ImageInfo
        {
            .MipmapCount = 10,
            .ArrayCount = 5,
            .RawDataPixelFormat = ETextureRawDataPixelFormat::BC3,
            .Width = 256,
            .Height = 64,
            .Depth = 1,
            .ImageDimension = CImageInfo::Dimension2D, 
        };

        uint32_t Size = 0;
        {
            Size = ImageHelper::CalculateImageSize(ImageInfo, 0);
            EXPECT_EQ(Size, 16384);
        }

        {
            Size = ImageHelper::CalculateImageSize(ImageInfo, 1);
            EXPECT_EQ(Size, 4096);
        }

        {
            Size = ImageHelper::CalculateImageSize(ImageInfo, 8);
            EXPECT_EQ(Size, 16);
        }
    }

    TEST(CalculateImageSize, RGB8)
    {
        constexpr CImageInfo ImageInfo
        {
            .MipmapCount = 10,
            .ArrayCount = 5,
            .RawDataPixelFormat = ETextureRawDataPixelFormat::RGB8,
            .Width = 256,
            .Height = 256,
            .Depth = 1,
            .ImageDimension = CImageInfo::Dimension2D, 
        };

        uint32_t Size = 0;
        {
            Size = ImageHelper::CalculateImageSize(ImageInfo, 0);
            EXPECT_EQ(Size, 196608);
        }

        {
            Size = ImageHelper::CalculateImageSize(ImageInfo, 1);
            EXPECT_EQ(Size, 49152);
        }

        {
            Size = ImageHelper::CalculateImageSize(ImageInfo, 8);
            EXPECT_EQ(Size, 3);
        }
    }
}

