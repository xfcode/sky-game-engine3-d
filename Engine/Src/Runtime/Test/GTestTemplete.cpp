﻿#include "gtest/gtest.h"
#include "Template/BitsSet.h"

using namespace SkyGameEngine3d;

char TestArray[16]{ };

TEST(BitsSetTest, SetValue)
{
    using TestType = TBitsSet<70>;
    constexpr uint32_t MemSize = TestType::BitsArrayCount * sizeof(TestType::IntType);

    {
        *reinterpret_cast<uint64_t*>(TestArray) = 0;
        *(reinterpret_cast<uint64_t*>(TestArray) + 1) = 1;

        TestType Set;
        Set.SetValue(0, true);
        EXPECT_EQ(memcmp(&Set, &TestArray, MemSize), 0) << "Set.SetValue(0, true)";
    }

    {
        *reinterpret_cast<uint64_t*>(TestArray) = ~0;
        *(reinterpret_cast<uint64_t*>(TestArray) + 1) = ~1;

        TestType Set;
        Set.Fill();
        Set.SetValue(0, false);
        EXPECT_EQ(memcmp(&Set, &TestArray, MemSize), 0) << "Set.SetValue(0, false)";
    }
    
    {
        *reinterpret_cast<uint64_t*>(TestArray) = 0x20;
        *(reinterpret_cast<uint64_t*>(TestArray) + 1) = 0;

        TestType Set;
        Set.SetValue(69, true);
        EXPECT_EQ(memcmp(&Set, &TestArray, MemSize), 0) << "Set.SetValue(69, true)";
    }
}

TEST(BitsSetTest, FindFirstValueIndex)
{
    using TestType = TBitsSet<70>;

    {
        TestType Set;
        Set.SetValue(20, true);
        EXPECT_EQ(Set.FindFirstValueIndex<true>(), 20) << "Set.SetValue(20, true)";
    }

    {
        TestType Set;
        EXPECT_EQ(Set.FindFirstValueIndex<true>(), -1) << "Not Set Any Value";
    }
    
    {
        TestType Set;
        Set.Fill();
        Set.SetValue(20, false);
        EXPECT_EQ(Set.FindFirstValueIndex<false>(), 20) << "Set.SetValue(20, false)";
    }
}