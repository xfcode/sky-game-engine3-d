﻿#pragma once

#include <array>
#include <memory>

#include "EngineModuleDef.h"
#include "Math/SimpleShape.h"

namespace SkyGameEngine3d
{
    struct ScenePrimitive
    {
        Box3D BoundBox;
    };

    constexpr int32_t MaxPrimitiveCountPreNode = 32;

    struct ScenePrimitiveNode
    {
        std::array<std::shared_ptr<ScenePrimitiveNode>, 4> ChildrenArray;

        std::vector<std::shared_ptr<ScenePrimitive>> Primitives;

        Box3D BoundBox;

        bool HasChildren = false;
    };

    using ScenePrimitiveNodePtr = std::shared_ptr<ScenePrimitiveNode>;
    using ScenePrimitivePtr = std::shared_ptr<ScenePrimitive>;

    class ScenePrimitiveQuadTree
    {
    public:
        explicit ScenePrimitiveQuadTree(const Box3D& WorldBox)
        {
            Root = std::make_shared<ScenePrimitiveNode>();
            Root->BoundBox = WorldBox;
        }

        void AddScenePrimitive(const ScenePrimitivePtr& ScenePrimitive)
        {
            this->AddScenePrimitiveToNode(Root, ScenePrimitive);
        }

        void RemoveScenePrimitive(const ScenePrimitivePtr& ScenePrimitive)
        {
            this->RemoveScenePrimitiveFromNode(Root, ScenePrimitive);
        }

    private:
        void AddScenePrimitiveToNode(const ScenePrimitiveNodePtr& Node, const ScenePrimitivePtr& ScenePrimitive);

        void RemoveScenePrimitiveFromNode(const ScenePrimitiveNodePtr& Node, const ScenePrimitivePtr& ScenePrimitive);

    private:
        std::shared_ptr<ScenePrimitiveNode> Root;
    };


    // 游戏场景
    class Scene
    {
    };
}
