﻿#pragma once

#include "EngineModuleDef.h"
#include "RHITypes.h"
#include "Interface/ISceneRenderTarget.h"
#include "Interface/IViewport.h"
#include "Math/Vector.h"
#include "Misc/UtilityTypes.h"

namespace SkyGameEngine3d
{
    class RHICommandList;
    class CRHIInstance;
    class CWindowBase;

    /**
     * \brief 负责视口的绘制
     */
    class ENGINE_API SceneViewport :
        public IViewportInputListener,
        public IViewportFrame,
        public CNonCopyable,
        public std::enable_shared_from_this<SceneViewport>
    {
    public:
        virtual ~SceneViewport();

        void Draw();

        /**
         * \brief 把 Viewport 注册到引擎，Viewport 在这个时候做一些初始化的操作
         * \param RHI 当前的 RHI 实例
         */
        virtual void OnRegister(const std::shared_ptr<CRHIInstance>& RHI);

        virtual void OnUnregister();

    protected:
        virtual void BeginRender(RHICommandList* CommandList);

        virtual void EndRender(RHICommandList* CommandList);

        #pragma region IViewportInputListener

    protected:
        virtual InputReply OnMouseButtonDown(const InputPointerEvent& Event) override
        {
            return InputReply::UnHandle();
        }

        #pragma endregion

        #pragma region IViewportFrame

    protected:
        virtual void OnResize(const Vector2DInt& NewSize) override;

        virtual std::shared_ptr<CRHITexture> GetSceneRenderTarget() const override;

        #pragma endregion

    private:
        std::shared_ptr<CRHITexture> SceneDrawRenderTarget;

        std::shared_ptr<CRHIInstance> RHIInstancePtr;

        Vector2DInt ViewportSize{ 1280, 720 };
    };
}
