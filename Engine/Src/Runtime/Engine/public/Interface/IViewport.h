#pragma once

#include "EngineModuleDef.h"
#include "Input/InputTypes.h"
#include "Math/Vector.h"

namespace SkyGameEngine3d
{
    class ENGINE_API IViewportInputListener
    {
    public:
        virtual InputReply OnMouseButtonDown(const InputPointerEvent& Event) = 0;
    };


    class ENGINE_API IViewportFrame
    {
        virtual void OnResize(const Vector2DInt& NewSize) = 0;

        virtual std::shared_ptr<class CRHITexture> GetSceneRenderTarget() const = 0;
    };
}
