﻿#pragma once

#include <memory>

#include "EngineModuleDef.h"

namespace SkyGameEngine3d
{
    class ISceneRenderTarget
    {
    protected:
        ~ISceneRenderTarget() = default;

    public:
        virtual std::shared_ptr<class CRHITexture> RenderTarget2D() = 0;
    };
}
