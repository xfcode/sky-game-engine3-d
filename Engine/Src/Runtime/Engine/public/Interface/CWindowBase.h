﻿#pragma once

#include <memory>

#include "EngineModuleDef.h"
#include "IRHIResourceFactory.h"
#include "RHI.h"
#include "RHICoreModuleInterface.h"
#include "Math/Vector.h"


namespace SkyGameEngine3d
{
    class ISceneRenderTarget;

    class RHIViewport;

    class ENGINE_API CWindowBase
    {
    public:

        CWindowBase() = default;

        CWindowBase(const CWindowBase& Other) = delete;
        
        CWindowBase& operator=(const CWindowBase& Other) = delete;
        
        virtual ~CWindowBase();

        [[nodiscard]] virtual Vector2D GetSize() const = 0;

        [[nodiscard]] virtual void* GetNativeHandle() const = 0;

        virtual void SetSceneRenderTarget(std::weak_ptr<ISceneRenderTarget> RenderTarget) = 0;

        virtual std::shared_ptr<RHIViewport> GetRHIViewport() = 0;

    protected:
        
        std::shared_ptr<RHIViewport> CreateRHIViewport();
    };
}
