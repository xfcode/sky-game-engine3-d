﻿#pragma once

#include <memory>
#include <chrono>
#include <map>

#include "EngineModuleDef.h"
#include "Subsystem.h"


namespace SkyGameEngine3d
{
    class SceneViewport;
}

namespace SkyGameEngine3d
{
    class SceneRenderPipeline;
    class AppRenderPipeline;
}

namespace SkyGameEngine3d
{
    /**
     * \brief 引擎的时钟类
     */
    struct ENGINE_API EngineClock : public TSingletonInstance<EngineClock>
    {
        /**
         * \brief 开始计时
         */
        void Start();

        /**
         * \brief 暂停时钟，有暂停游戏的效果
         * \param Pause 
         */
        void SetPause(bool Pause = true);

        /**
         * \brief 获取当前帧的时间增量
         * \return 
         */
        [[nodiscard]] double GetFrameDt() const;


        /**
         * \brief 设置时间缩放比例
         * \param InTimeScale 
         */
        void SetTimeScale(float InTimeScale);

        /**
         * \brief 获取总时间，不计算暂停的时间
         * \return 
         */
        [[nodiscard]] double GetTotalTime() const;

        /**
         * \brief 获取帧率
         * \return 
         */
        [[nodiscard]] uint32_t GetFps() const;


        /**
         * \brief 计算在固定帧率的情况下需要等待多久
         * \param Fps 最大的 FPS
         * \return 
         */
        [[nodiscard]] std::chrono::duration<double> CalculateWaitTime(double Fps) const;

        /**
         * \brief 更新时间间隔
         */
        void Tick();

    private:
        std::chrono::time_point<std::chrono::high_resolution_clock> LastPoint;

        double FrameDt = 0;

        double TotalTime = 0;

        float TimeScale = 1.f;

        bool bPause = false;
    };

    class ENGINE_API EngineBase
        : public std::enable_shared_from_this<EngineBase>,
          public CNonCopyable
    {
    public:
        virtual ~EngineBase() = default;

        EngineBase();

        virtual void Init();

        virtual void Tick(double Dt);

        void SetMaxFps(const uint32_t InMaxFps)
        {
            MaxFps = InMaxFps;
        }

        std::shared_ptr<SceneRenderPipeline> GetSceneRenderPipeline() const
        {
            return SceneRenderPipeline;
        }


        void RegisterSceneViewport(std::wstring_view SceneViewportName, std::shared_ptr<SceneViewport> InViewport);

        void UnregisterSceneViewport(std::wstring_view SceneViewportName);

        template <typename SubsystemType>
            requires std::is_base_of_v<EngineSubsystem, SubsystemType>
        SubsystemType* GetSubSystem() const
        {
            return SubsystemContainer.GetSubsystem<SubsystemType>();
        }

    private:
        void HandleFixedFps(double Dt);

    private:
        uint32_t MaxFps = 60;

        void* WindowsHandle = nullptr;

        std::shared_ptr<class RHIViewport> Viewport;

        SubsystemContainer<ESubsystemType::Engine, EngineSubsystem> SubsystemContainer;

        std::shared_ptr<SceneRenderPipeline> SceneRenderPipeline;

        std::shared_ptr<AppRenderPipeline> AppRenderPipeline;

        struct SceneViewportInfo
        {
            std::wstring Name;
            std::shared_ptr<SceneViewport> ViewportPtr;
        };

        std::vector<SceneViewportInfo> SceneViewportArray;
    };

    ENGINE_API void RegisterEngine(const std::shared_ptr<EngineBase>& EngineInstance);

    ENGINE_API EngineBase* GetEngine();
}
