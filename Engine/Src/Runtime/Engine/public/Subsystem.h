﻿#pragma once

#include "EngineModuleDef.h"
#include "Misc/UtilityTypes.h"
#include  <typeinfo>
#include <unordered_map>
#include <string>
#include <typeindex>
#include <unordered_set>

namespace SkyGameEngine3d
{
    class SubsystemBase;

    namespace Subsystem
    {
        struct Info
        {
            SubsystemBase* Prototype = nullptr;

            std::type_index TypeId;
        };
    }

    struct SubsystemContainerBase
    {
    public:
        virtual ~SubsystemContainerBase() = default;

        template <class SubsystemType>
        SubsystemType* DependSubsystem()
        {
            return this->DependSubSystemImp(typeid(SubsystemType));
        }

    private:
        virtual SubsystemBase* DependSubSystemImp(std::type_index TypeIndex) = 0;
    };

    enum class ESubsystemType
    {
        None,
        Engine,
    };

    class ENGINE_API SubsystemBase : public CNonCopyable
    {
    public:
        SubsystemBase() = default;

        virtual ~SubsystemBase() = default;

        virtual void Init(const SubsystemContainerBase& InitContext)
        {
        }

        virtual void Destroy()
        {
        };

        virtual SubsystemBase* Clone() const = 0;

        virtual ESubsystemType GetType() const = 0;
    };


    class ENGINE_API SubsystemManager
        : public TSingletonInstance<SubsystemManager>
    {
    public:
        template <class SubsystemType>
        void RegisterSubsystem()
        {
            const auto& TypeId = typeid(SubsystemType);

            // 如果已经存在 Id，说明这个类已经被替换
            if (SubsystemPrototypeMap.contains(TypeId))
            {
                return;
            }

            SubsystemPrototypeMap.emplace(TypeId, new SubsystemType);
        }

        template <class OldSubsystemType, class NewSubsystemType>
            requires std::is_base_of_v<OldSubsystemType, NewSubsystemType>
        void ReplaceSubsystemPrototype()
        {
            const auto& TypeId = typeid(OldSubsystemType);
            SubsystemPrototypeMap.emplace(TypeId, new NewSubsystemType);
        }

        std::vector<Subsystem::Info> GetSubsystemPrototypes(ESubsystemType Type)
        {
            std::vector<Subsystem::Info> Ret;
            for (auto& V : SubsystemPrototypeMap)
            {
                if (V.second->GetType() == Type)
                {
                    Ret.push_back(Subsystem::Info{ V.second, V.first });
                }
            }
            return Ret;
        }

    private:
        std::unordered_map<std::type_index, SubsystemBase*> SubsystemPrototypeMap;
    };


    template <ESubsystemType SubsystemType, typename BaseSubsystemType>
    struct SubsystemContainer : public SubsystemContainerBase
    {
        using BaseSysType = BaseSubsystemType;

        void CreateSubsystem()
        {
            auto Instance = SubsystemManager::GetInstance();
            auto Prototypes = Instance->GetSubsystemPrototypes(SubsystemType);

            for (const auto& ProtoInfo : Prototypes)
            {
                if (Instances.contains(ProtoInfo.TypeId))
                {
                    continue;
                }

                this->CreateInstance(ProtoInfo.Prototype, ProtoInfo.TypeId);
            }
        }

        template <typename RetSubsystemType>
            requires std::is_base_of_v<BaseSysType, RetSubsystemType>
        RetSubsystemType* GetSubsystem() const
        {
            auto Find = std::ranges::find_if(this->Instances,
                                             [](const auto& V) { return V.first == typeid(RetSubsystemType); }
            );


            if (Find != this->Instances.end())
            {
                return dynamic_cast<RetSubsystemType*>(Find->second);
            }

            return nullptr;
        }

    private:
        SubsystemBase* CreateInstance(const SubsystemBase* Prototype, std::type_index TypeIndex)
        {
            auto Ins = Prototype->Clone();
            Ins->Init(*this);
            Instances.emplace(TypeIndex, Ins);
            return Ins;
        }

        virtual SubsystemBase* DependSubSystemImp(std::type_index TypeIndex) override
        {
            if (!Instances.contains(TypeIndex))
            {
                auto Find = Instances.find(TypeIndex);
                if (Find != Instances.end())
                {
                    return this->CreateInstance(Find->second, Find->first);
                }
                return nullptr;
            }
            return Instances[TypeIndex];
        }

    private:
        std::unordered_map<std::type_index, SubsystemBase*> Instances;
    };

    template <typename SubsystemType>
    struct SubsystemRegister
    {
        SubsystemRegister()
        {
            SubsystemManager::GetInstance()->RegisterSubsystem<SubsystemType>();
        }
    };

    template <typename OldSubsystemType, typename NewSubsystem>
    struct SubsystemRegisterAndReplace
    {
        SubsystemRegisterAndReplace()
        {
            SubsystemManager::GetInstance()->ReplaceSubsystemPrototype<OldSubsystemType, NewSubsystem>();
        }
    };

    class ENGINE_API EngineSubsystem : public SubsystemBase
    {
    private:
        [[nodiscard]] virtual ESubsystemType GetType() const override
        {
            return ESubsystemType::Engine;
        }
    };
}
