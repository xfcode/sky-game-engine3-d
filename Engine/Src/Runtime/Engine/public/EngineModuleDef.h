#pragma once

#include "HAL/PlatformDefine.h"

#ifdef ENGINE
    #define ENGINE_API DLL_EXPORT
#else
    #define ENGINE_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameEngine = L"Engine";
