﻿#include "Viewport.h"

#include "EngineBase.h"
#include "Interface/CWindowBase.h"
#include "RHI.h"
#include "RHICommandList.h"
#include "SceneRenderPipeline.h"

namespace SkyGameEngine3d
{
    SceneViewport::~SceneViewport()
    {
    }

    void SceneViewport::BeginRender(RHICommandList* CommandList)
    {
    }

    void SceneViewport::EndRender(RHICommandList* CommandList)
    {
    }

    void SceneViewport::Draw()
    {
        // const auto CommandList = RHIInstancePtr->GetCommandList();
        // this->BeginRender(CommandList);
        // 
        // const auto RenderPipline = GetEngine()->GetSceneRenderPipeline();
        // 
        // SceneView MySceneView;
        // MySceneView.RenderTarget = SceneDrawRenderTarget;
        // 
        // RenderPipline->Render(MySceneView, CommandList);
        // 
        // this->EndRender(CommandList);
    }

    void SceneViewport::OnRegister(const std::shared_ptr<CRHIInstance>& RHI)
    {
        RHIInstancePtr = RHI;
    }

    void SceneViewport::OnUnregister()
    {
        RHIInstancePtr = nullptr;
    }

    void SceneViewport::OnResize(const Vector2DInt& NewSize)
    {
        // 重新生成场景绘制的RT

        // const RHITextureCreateArgs CreateRTDesc =
        //     RHITextureCreateArgs::Make2DTextureArgs(L"SceneDrawRenderTarget")
        //     .SetFormat(EPixelFormat::A2B10G10R10F)
        //     .SetFlags(ETextureFlags::RenderTarget)
        //     .SetSize(NewSize);
        // 
        // SceneDrawRenderTarget = GetRHIInstance()->CreateTexture(CreateRTDesc);
    }

    std::shared_ptr<CRHITexture> SceneViewport::GetSceneRenderTarget() const
    {
        return SceneDrawRenderTarget;
    }
}
