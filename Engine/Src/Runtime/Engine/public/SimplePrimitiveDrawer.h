﻿#pragma once

#include <vector>

#include "EngineModuleDef.h"
#include "PrimitiveDraw.h"
#include "Math/Color.h"
#include "Math/Vector.h"

namespace SkyGameEngine3d
{
    struct SimplePrimitiveVertex
    {
        Vector3D Pos;
        Vector2D TextureCoordinate;
        ColorLinear Color;
    };


    struct SimplePrimitiveDrawer final : IPrimitiveDrawInterface
    {
        virtual void DrawLine(Vector3D& Start, Vector3D& End, ColorLinear& Color, float Thickness) override;

        virtual void DrawPoint(Vector3D& Pos, ColorLinear& ColorLinear, float PointSize) override;

        virtual void DrawMesh(const PrimitiveMeshData& MeshData) override;

        // void Draw();

    private:
        struct LineDesc
        {
            Vector3D Start;
            Vector3D End;
            ColorLinear ColorLinear;
            float Thickness = 0.f;
        };

        struct PointDesc
        {
            Vector3D Pos;
            ColorLinear Color;
            float Size = 1.0;
        };


        struct MeshDesc
        {
        };

        std::vector<SimplePrimitiveVertex> DrawLineVertexList;

        std::vector<LineDesc> DrawLineList;

        std::vector<PointDesc> DrawPointList;

        std::vector<MeshDesc> DrawMeshList;
    };
}
