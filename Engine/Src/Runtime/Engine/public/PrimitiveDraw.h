﻿#pragma once

#include "EngineModuleDef.h"

namespace SkyGameEngine3d
{
    struct ColorLinear;

    class Vector3D;

    struct PrimitiveMeshData
    {
    };

    struct ENGINE_API IPrimitiveDrawInterface
    {
        virtual ~IPrimitiveDrawInterface();

        /**
         * \brief 绘制一条直线
         * \param Start 起点
         * \param End 终点
         * \param Color 线条颜色
         * \param Thickness 线条粗细
         */
        virtual void DrawLine(Vector3D& Start, Vector3D& End, ColorLinear& Color, float Thickness = 0.f) = 0;

        /**
         * \brief 绘制一个点
         * \param Pos 位置
         * \param ColorLinear 颜色 
         * \param PointSize 点的大小
         */
        virtual void DrawPoint(Vector3D& Pos, ColorLinear& ColorLinear, float PointSize) = 0;


        /**
         * \brief 绘制 Mesh
         * \param MeshData Mesh 数据
         */
        virtual void DrawMesh(const PrimitiveMeshData& MeshData) = 0;
    };
}
