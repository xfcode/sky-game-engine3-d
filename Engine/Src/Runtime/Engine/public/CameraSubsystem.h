﻿#pragma once

#include "Subsystem.h"
#include "Math/MathLib.h"

namespace SkyGameEngine3d
{
    //! 相机的投影模式
    enum class ECameraProjectionMode
    {
        //! 透视
        Perspective,
        //! 正交
        Orthographic
    };

    struct CameraViewInfo
    {
        ECameraProjectionMode ProjectionMode = ECameraProjectionMode::Perspective;
        float Fov = 0.25f * PI;
    };

    class ENGINE_API CameraSubsystem
        : public EngineSubsystem
    {
    private:
        virtual void Init(const SubsystemContainerBase& InitContext) override;

        virtual void Destroy() override;

        virtual SubsystemBase* Clone() const override;

    public:
    };
}
