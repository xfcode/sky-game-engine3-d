#include "CameraSubsystem.h"

namespace SkyGameEngine3d
{
    void CameraSubsystem::Init(const SubsystemContainerBase& InitContext)
    {
    }

    void CameraSubsystem::Destroy()
    {
    }

    SubsystemBase* CameraSubsystem::Clone() const
    {
        return new CameraSubsystem;
    }

    static SubsystemRegister<CameraSubsystem> AutoRegister;
}
