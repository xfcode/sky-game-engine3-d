﻿#include "Interface/CWindowBase.h"

namespace SkyGameEngine3d
{
    CWindowBase::~CWindowBase()
    {
    
    }

    std::shared_ptr<RHIViewport> CWindowBase::CreateRHIViewport()
    {
        const auto RHIInstance = GetRHIInstance();
        if (!RHIInstance)
        {
            return nullptr;
        }
            
        RHIViewportCreateArg CreateArg;
        CreateArg.WindowHandle = this->GetNativeHandle();
            
        const auto& WindowSize = this->GetSize();
            
        CreateArg.WindowW = static_cast<int32_t>(WindowSize.X);
        CreateArg.WindowH = static_cast<int32_t>(WindowSize.Y);

        CreateArg.BackBufferFormat = EPixelFormat::A2B10G10R10F;
        
        const auto ResourceFactory = RHIInstance->GetResourceFactory();
        auto RHIViewportPtr = ResourceFactory->CreateRHIViewport(CreateArg);
        return RHIViewportPtr;
    }
}
