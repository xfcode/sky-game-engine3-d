#include "EngineBase.h"
#include <memory>

#include "Viewport.h"
#include "public/RHI.h"

static std::shared_ptr<SkyGameEngine3d::EngineBase> Engine = nullptr;

namespace SkyGameEngine3d
{
    EngineBase* GetEngine()
    {
        return Engine.get();
    }

    void RegisterEngine(const std::shared_ptr<EngineBase>& EngineInstance)
    {
        Engine = EngineInstance;
    }

    void EngineClock::Start()
    {
        LastPoint = std::chrono::steady_clock::now();
    }

    void EngineClock::SetPause(const bool Pause)
    {
        bPause = Pause;
    }

    double EngineClock::GetFrameDt() const
    {
        return FrameDt;
    }

    void EngineClock::SetTimeScale(const float InTimeScale)
    {
        TimeScale = InTimeScale;
    }

    double EngineClock::GetTotalTime() const
    {
        return TotalTime;
    }

    uint32_t EngineClock::GetFps() const
    {
        return static_cast<uint32_t>(std::round(static_cast<double>(1) / FrameDt));
    }

    std::chrono::duration<double> EngineClock::CalculateWaitTime(const double Fps) const
    {
        using namespace std::literals;
        using namespace std::chrono;

        const auto Now = high_resolution_clock::now();
        const duration<double> Dt = Now - LastPoint;
        return std::chrono::duration<double>(1 / Fps) - Dt;
    }

    void EngineClock::Tick()
    {
        using namespace std::literals;
        using namespace std::chrono;
        const auto Now = high_resolution_clock::now();
        FrameDt = std::chrono::duration<double>(Now - LastPoint).count();
        LastPoint = Now;

        if (bPause)
        {
            FrameDt = 0;
        }
        else
        {
            FrameDt = static_cast<double>(TimeScale) * FrameDt;
        }

        TotalTime += FrameDt;
    }

    EngineBase::EngineBase()
    {
    }

    void EngineBase::Init()
    {
        SubsystemContainer.CreateSubsystem();
    }

    void EngineBase::Tick(double Dt)
    {
        // 绘制场景


        // 绘制 UI


        this->HandleFixedFps(Dt);
    }

    void EngineBase::RegisterSceneViewport(
        std::wstring_view SceneViewportName,
        std::shared_ptr<SceneViewport> InViewport)
    {
        this->SceneViewportArray.push_back(SceneViewportInfo{ SceneViewportName.data(), InViewport });

        // const auto RHIIns = GetRHIInstance();
        // InViewport->OnRegister(RHIIns, )
    }

    void EngineBase::UnregisterSceneViewport(std::wstring_view SceneViewportName)
    {
    }

    void EngineBase::HandleFixedFps(const double Dt)
    {
        using namespace std::literals;
        using namespace std::chrono;
        if (MaxFps != 0)
        {
            const auto SleepTime = EngineClock::GetInstance()->CalculateWaitTime(MaxFps);
            if (SleepTime.count() > 0)
            {
                const auto Now = high_resolution_clock::now();
                const auto SleepEndTime = Now + std::chrono::duration_cast<high_resolution_clock::duration>(SleepTime);

                std::this_thread::sleep_for(SleepTime - 0.002s);

                do
                {
                    std::this_thread::yield();
                }
                while (high_resolution_clock::now() < SleepEndTime);
            }
        }
    }
}
