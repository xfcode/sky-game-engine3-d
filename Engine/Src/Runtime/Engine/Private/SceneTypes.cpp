﻿#include "SceneTypes.h"

#include <algorithm>

namespace SkyGameEngine3d
{
    void ScenePrimitiveQuadTree::AddScenePrimitiveToNode(
        const ScenePrimitiveNodePtr& Node,
        const ScenePrimitivePtr& ScenePrimitive)
    {
        if (Node->BoundBox.IntersectBox(ScenePrimitive->BoundBox))
        {
            if (Node->HasChildren)
            {
                for (const auto& Children : Node->ChildrenArray)
                {
                    this->AddScenePrimitiveToNode(Children, ScenePrimitive);
                }
            }
            else
            {
                if (Node->Primitives.size() < MaxPrimitiveCountPreNode)
                {
                    Node->Primitives.push_back(ScenePrimitive);
                }
                else
                {
                    // 分裂为 4 个子节点

                    const auto SplitBoxArray = Box3D::SplitBox<1, 1, 0>(Node->BoundBox);
                    assert(SplitBoxArray.size() == 4);
                    for (int Index = 0; Index < 4; ++Index)
                    {
                        const auto NewNode = std::make_shared<ScenePrimitiveNode>();
                        NewNode->BoundBox = SplitBoxArray[Index];
                        Node->ChildrenArray[Index] = NewNode;
                    }

                    Node->HasChildren = true;

                    // 搬用之前的数据
                    for (const auto& Primitive : Node->Primitives)
                    {
                        AddScenePrimitiveToNode(Node, Primitive);
                    }

                    Node->Primitives.clear();

                    // 再次对这个 ScenePrimitive 节点进行添加
                    AddScenePrimitiveToNode(Node, ScenePrimitive);
                }
            }
        }
    }

    // ReSharper disable once CppMemberFunctionMayBeStatic
    void ScenePrimitiveQuadTree::RemoveScenePrimitiveFromNode(
        const ScenePrimitiveNodePtr& Node,
        const ScenePrimitivePtr& ScenePrimitive)
    {
        if (Node->BoundBox.IntersectBox(ScenePrimitive->BoundBox))
        {
            if (Node->HasChildren)
            {
                for (const auto& ChildrenNode : Node->ChildrenArray)
                {
                    RemoveScenePrimitiveFromNode(ChildrenNode, ScenePrimitive);
                }
            }
            else
            {
                std::erase(Node->Primitives, ScenePrimitive);
            }
        }
    }
}
