#include "EngineModuleDef.h"
#include "ModuleManager/ModuleInterface.h"

class ENGINE_API EngineModule : public IModuleInterface
{
public:
    virtual ~EngineModule() override
    {
    }

    virtual void OnLoad() override
    {
    }

    virtual void OnUnload() override
    {
    }

    virtual ModuleSpec GetSpec() override
    {
        return ModuleSpec{ };
    }
};

EXPORT_MODULE(Engine, EngineModule);
