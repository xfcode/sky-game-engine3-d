﻿#include "SimplePrimitiveDrawer.h"

namespace SkyGameEngine3d
{
    void SimplePrimitiveDrawer::DrawLine(Vector3D& Start, Vector3D& End, ColorLinear& Color, float Thickness)
    {
        if (Thickness == 0.f)
        {
            this->DrawLineVertexList.push_back(SimplePrimitiveVertex{
                    .Pos = Start,
                    .TextureCoordinate = Vector2D{ },
                    .Color = Color,
                }
            );
        }
        else
        {
            this->DrawLineList.push_back(LineDesc{
                    .Start = Start,
                    .End = End,
                    .ColorLinear = Color,
                    .Thickness = Thickness,
                }
            );
        }
    }

    void SimplePrimitiveDrawer::DrawPoint(Vector3D& Pos, ColorLinear& ColorLinear, float PointSize)
    {
        this->DrawPointList.push_back(PointDesc{
                .Pos = Pos,
                .Color = ColorLinear,
                .Size = PointSize,
            }
        );
    }

    void SimplePrimitiveDrawer::DrawMesh(const PrimitiveMeshData& MeshData)
    {
    }
}
