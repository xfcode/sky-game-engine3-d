﻿#pragma once

#include "EngineModuleDef.h"
#include "RenderResource.h"
#include "RHITypes.h"
#include "Subsystem.h"



namespace SkyGameEngine3d
{
    class CImageLoader;

    struct CTextureMipmapPlatformData
    {
        std::vector<uint8_t> Data;
        uint32_t MipIndex = 0;
    };

    struct CTextureArrayPlatformData
    {
        std::vector<CTextureMipmapPlatformData> MipmapDataArray;
    };

    struct CTexturePlatformData
    {
        EPixelFormat Format = EPixelFormat::None;
        uint32_t Width = 0;
        uint32_t Height = 0;
        uint32_t Depth = 1;
        uint32_t MipmapCount = 1;
        std::vector<CTextureArrayPlatformData> TextureArray;
    };

    class ENGINE_API CTexture : public CRenderResource
    {
    public:
        bool Init(const std::shared_ptr<CImageLoader>& ImageLoader);

    protected:

        virtual void InitRHIResource(IRHIResourceFactory* Factory) override;

        virtual void ReleaseRHIResource() override;
        
    protected:
        
        std::shared_ptr<CTexturePlatformData> PlatformData;

        RHITexturePtr RHITexture;
    };

    class ENGINE_API CTexture2D : public CTexture
    {
        
    };

    
    class ENGINE_API CTextureSubsystem : public EngineSubsystem
    {
    public:
        
        std::shared_ptr<CTexture2D> CreateTexture(const std::filesystem::path& FilePath);
        
    protected:
        [[nodiscard]] virtual SubsystemBase* Clone() const override;
        virtual void Init(const SubsystemContainerBase& InitContext) override;
        virtual void Destroy() override;
    };
}
