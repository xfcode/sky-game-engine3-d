﻿#pragma once

namespace SkyGameEngine3d
{
    /**
     * \brief　纹理的裸数据像素格式 
     */
    enum class ETextureRawDataPixelFormat
    {
        None,
        G8,
        RGB8,
        RGBA8,

        /**
         * \brief Radiance HDR 格式 https://en.wikipedia.org/wiki/RGBE_image_format
         *  1. **红色分量(R)**:红色通道的强度
         *  2. **绿色分量(G)**:绿色通道的强度
         *  3. **蓝色分量(B)**:蓝色通道的强度
         *  4. **指数(E)**:决定像素整体亮度或亮度的指数或比例因子,它被用来表示大范围的强度值
         */
        RGBE8,

        RGB16,
        RGBA16,
        RGBA16F,
        G16,
        RGBA32F,
        R16F,
        R32F,

        BC1,
        BC2,
        BC3,
        BC4,
        BC5,
        BC6,
        BC7,
    };


    enum class EImageType
    {
        DDS,
        Png,
    };
}
