﻿#pragma once

#include <fstream>
#include <memory>
#include <vector>

#include "EngineModuleDef.h"
#include "TextureTypes.h"
#include "Misc/UtilityTypes.h"


namespace SkyGameEngine3d
{
    enum class EImageType;
    enum class ETextureRawDataPixelFormat;
    
    struct CImageInfo
    {
        uint32_t MipmapCount = 1;
        uint32_t ArrayCount = 1;
        ETextureRawDataPixelFormat RawDataPixelFormat  = ETextureRawDataPixelFormat::None;
        uint32_t Width = 0;
        uint32_t Height = 0;
        uint32_t Depth = 1;
        
        enum EDimension
        {
            Dimension1D,
            Dimension2D,
            Dimension3D,
        };
        
        EDimension ImageDimension = Dimension2D;
    };
    
    class ENGINE_API CImageLoader : public CNonCopyable
    {
    public:
        virtual ~CImageLoader();

        virtual bool GetRawData(std::vector<uint8_t>& OutRawData) = 0;
        
        virtual bool GetSubRawData(std::vector<uint8_t>& OutRawData, uint32_t MipmapIndex = 0, uint32_t ArrayIndex = 0) =0;

        virtual CImageInfo GetImageInfo() = 0;
        
        [[nodiscard]] virtual ETextureRawDataPixelFormat GetRawDataPixelFormat() const = 0;

        virtual bool LoadImage(const std::filesystem::path& FilePath, std::wstring& OutError) = 0;
    };

    class ENGINE_API CImageLoaderFactory : public CNonCopyable
    {
    public:
        virtual ~CImageLoaderFactory();

        [[nodiscard]] virtual bool IsSupportImageType(EImageType ImageType)
        {
            return false;
        }

        [[nodiscard]] virtual std::shared_ptr<CImageLoader> Create()
        {
            return nullptr;
        }
    };

    class ENGINE_API CImageLoaderManager :
        public TSingletonInstance<CImageLoaderManager>
    {
    public:
        std::shared_ptr<CImageLoader> LoadTextureByFile(const std::filesystem::path& FilePath, std::wstring& ErrorMsg);

        void RegisterLoaderFactory(std::unique_ptr<CImageLoaderFactory>&& Factory);

    private:
        [[nodiscard]] CImageLoaderFactory* FindFactory(const std::wstring_view ExtensionStr) const;

    private:
        std::vector<std::unique_ptr<CImageLoaderFactory>> FactoryArray;
    };


    namespace ImageHelper
    {
        ENGINE_API uint32_t CalculateImageSize(const CImageInfo& ImageInfo, uint32_t MipmapIndex = 0);
    };
}
