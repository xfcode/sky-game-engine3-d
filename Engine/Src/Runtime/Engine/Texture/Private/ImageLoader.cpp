﻿#include "Texture/Public/ImageLoader.h"

#include <filesystem>
#include <unordered_map>

#include "Math/MathLib.h"
#include "Misc/Memory.h"

namespace SkyGameEngine3d
{
    CImageLoader::~CImageLoader()
    {
    }

    CImageLoaderFactory::~CImageLoaderFactory()
    {
    }

    // ReSharper disable once CppMemberFunctionMayBeConst
    std::shared_ptr<CImageLoader> CImageLoaderManager::LoadTextureByFile(const std::filesystem::path& FilePath,
                                                                         std::wstring& ErrorMsg)
    {
        using namespace std;

        if (!exists(FilePath))
        {
            ErrorMsg = format(L"文件不存在 {0}", FilePath.c_str());
            return nullptr;
        }

        const auto Factory = this->FindFactory(FilePath.extension().c_str());
        if (!Factory)
        {
            ErrorMsg = format(L"文件: {0}, 无法找到支持该格式的 Factory", FilePath.c_str());
            return nullptr;
        }

        auto Loader = Factory->Create();
        if (!Loader->LoadImage(FilePath, ErrorMsg))
        {
            return nullptr;
        }

        return Loader;
    }

    void CImageLoaderManager::RegisterLoaderFactory(std::unique_ptr<CImageLoaderFactory>&& Factory)
    {
        FactoryArray.emplace_back(std::move(Factory));
    }

    CImageLoaderFactory* CImageLoaderManager::FindFactory(const std::wstring_view ExtensionStr) const
    {
        static std::unordered_map<std::wstring_view, EImageType> ExtensionStrToEnum
        {
            { L".png", EImageType::Png },
            { L".dds", EImageType::DDS },
        };

        const auto Find = ExtensionStrToEnum.find(ExtensionStr);
        if (Find != ExtensionStrToEnum.end())
        {
            const auto Type = Find->second;
            for (const auto& Factory : FactoryArray)
            {
                if (Factory->IsSupportImageType(Type))
                {
                    return Factory.get();
                }
            }
        }

        return nullptr;
    }

    uint32_t ImageHelper::CalculateImageSize(const CImageInfo& ImageInfo, uint32_t MipmapIndex)
    {
        if (MipmapIndex >= ImageInfo.MipmapCount)
        {
            assert(false);
            return 0;
        }

        using namespace MathLib;
        const uint32_t Width = ImageInfo.Width >> (1 * MipmapIndex);
        const uint32_t Height = ImageInfo.Height >> (1 * MipmapIndex);
        switch (ImageInfo.RawDataPixelFormat)
        {
        case ETextureRawDataPixelFormat::G8:
            return Height * Width;
        case ETextureRawDataPixelFormat::RGB8:
            return Height * Width * 3;
        case ETextureRawDataPixelFormat::RGBA8:
        case ETextureRawDataPixelFormat::RGBE8:
            return Height * Width * 4;
        case ETextureRawDataPixelFormat::RGB16:
            return Height * Width * 2 * 3;
        case ETextureRawDataPixelFormat::RGBA16:
        case ETextureRawDataPixelFormat::RGBA16F:
            return Height * Width * 2 * 4;
        case ETextureRawDataPixelFormat::RGBA32F:
            return Height * Width * 4 * 4;
        case ETextureRawDataPixelFormat::G16:
        case ETextureRawDataPixelFormat::R16F:
            return Height * Width * 2;
        case ETextureRawDataPixelFormat::R32F:
            return Height * Width * 4;
        case ETextureRawDataPixelFormat::BC1:
        case ETextureRawDataPixelFormat::BC4:
            // HBlockNum * WBlockNum * BlockSize
            // https://learn.microsoft.com/zh-cn/windows/win32/direct3d11/texture-block-compression-in-direct3d-11
            return Max(1u, DivisionCeilResult(Height, 4)) * Max(1u, DivisionCeilResult(Width, 4)) * 8;
        case ETextureRawDataPixelFormat::BC2:
        case ETextureRawDataPixelFormat::BC3:
        case ETextureRawDataPixelFormat::BC5:
        case ETextureRawDataPixelFormat::BC6:
        case ETextureRawDataPixelFormat::BC7:
            return Max(1u, DivisionCeilResult(Height, 4)) * Max(1u, DivisionCeilResult(Width, 4)) * 16;
        case ETextureRawDataPixelFormat::None:
        default:
            assert(false);
            return 0;
        }
    }
}
