﻿#pragma once
#include <optional>

#include "Texture/Public/ImageLoader.h"
#include "DDS.h"

namespace SkyGameEngine3d
{
    class CDdsImageLoaderFactory :
        public CImageLoaderFactory
    {
    public:
        [[nodiscard]] virtual bool IsSupportImageType(EImageType ImageType) override;
        [[nodiscard]] virtual std::shared_ptr<CImageLoader> Create() override;
    };

    class CDdsImageLoader : public CImageLoader
    {
    public:
        virtual ~CDdsImageLoader() override;

        virtual bool GetRawData(std::vector<uint8_t>& OutRawData) override;

        virtual bool GetSubRawData(std::vector<uint8_t>& OutRawData, uint32_t MipmapIndex, uint32_t ArrayIndex) override;

        virtual CImageInfo GetImageInfo() override;

        [[nodiscard]] virtual ETextureRawDataPixelFormat GetRawDataPixelFormat() const override;

        virtual bool LoadImage(const std::filesystem::path& FilePath, std::wstring& OutError) override;

    private:
        DirectX::DDS_HEADER DDSHeader = { };
        std::optional<DirectX::DDS_HEADER_DXT10> HeaderDxt10;
        std::vector<uint8_t> RawData;
        
        CImageInfo ImageInfo = { };

        bool bIsInit = false;
    };
}
