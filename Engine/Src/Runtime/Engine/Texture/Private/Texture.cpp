﻿#include "Texture/Public/Texture.h"

#include "RHI.h"
#include "RHICoreModuleInterface.h"
#include "Log/Log.h"
#include "Texture/Public/ImageLoader.h"


DEFINE_STATIC_LOG_CATEGORY(LogTexture);

namespace SkyGameEngine3d
{
    [[maybe_unused]] static SubsystemRegister<CTextureSubsystem> AutoRegister;


    EPixelFormat Convert(ETextureRawDataPixelFormat RawFormat)
    {
        switch (RawFormat)
        {
        case ETextureRawDataPixelFormat::None:
        case ETextureRawDataPixelFormat::G8:
        case ETextureRawDataPixelFormat::RGB8:
        case ETextureRawDataPixelFormat::RGBA8:
        case ETextureRawDataPixelFormat::RGBE8:
        case ETextureRawDataPixelFormat::RGB16:
        case ETextureRawDataPixelFormat::RGBA16:
        case ETextureRawDataPixelFormat::RGBA16F:
        case ETextureRawDataPixelFormat::G16:
        case ETextureRawDataPixelFormat::RGBA32F:
        case ETextureRawDataPixelFormat::R16F:
        case ETextureRawDataPixelFormat::R32F:
            assert(false);
            return EPixelFormat::None;
        case ETextureRawDataPixelFormat::BC1:
            return EPixelFormat::BC1;
        case ETextureRawDataPixelFormat::BC2:
            return EPixelFormat::BC2;
        case ETextureRawDataPixelFormat::BC3:
            return EPixelFormat::BC3;
        case ETextureRawDataPixelFormat::BC4:
            return EPixelFormat::BC4;
        case ETextureRawDataPixelFormat::BC5:
            return EPixelFormat::BC5;
        case ETextureRawDataPixelFormat::BC6:
            return EPixelFormat::BC6;
        case ETextureRawDataPixelFormat::BC7:
            return EPixelFormat::BC7;
        }
        return EPixelFormat::None;
    }

    void CTexture::InitRHIResource(IRHIResourceFactory* Factory)
    {
        const RHITextureCreateArgs Args
        {
            .Dimension = ETextureDimension::Texture2D,
            .Flags = ETextureFlags::None,
            .Format = PlatformData->Format,
            .Size =
            {
                static_cast<int32_t>(PlatformData->Width),
                static_cast<int32_t>(PlatformData->Height)
            },
            .MipsCount = PlatformData->MipmapCount,
            .SampleCount = 1,
            .DebugName = L"TestTexture"
        };

        RHITexture = Factory->CreateTexture(Args);

        for (uint32_t ArrayIndex = 0; ArrayIndex < PlatformData->TextureArray.size(); ++ArrayIndex)
        {
            const auto& ArrayInfo = PlatformData->TextureArray.at(ArrayIndex);
            for (uint32_t MipmapIndex = 0; MipmapIndex < PlatformData->MipmapCount; MipmapIndex++)
            {
                const auto& MipmapData = ArrayInfo.MipmapDataArray.at(MipmapIndex);
                uint32_t RowInByte = 0;
                const auto LockData = RHITexture->Lock(MipmapIndex, ArrayIndex, RowInByte);
                std::memcpy(LockData, MipmapData.Data.data(), MipmapData.Data.size());
                RHITexture->UnLock(MipmapIndex, ArrayIndex);
            }
        }
    }

    void CTexture::ReleaseRHIResource()
    {
    }

    bool CTexture::Init(const std::shared_ptr<CImageLoader>& ImageLoader)
    {
        PlatformData = std::make_shared<CTexturePlatformData>();
        const auto& ImageInfo = ImageLoader->GetImageInfo();
        PlatformData->Format = Convert(ImageInfo.RawDataPixelFormat);
        PlatformData->TextureArray.resize(ImageInfo.ArrayCount);
        for (uint32_t ArrayIndex = 0; ArrayIndex < ImageInfo.ArrayCount; ++ArrayIndex)
        {
            auto& ArrayTextureDataRef = PlatformData->TextureArray.at(ArrayIndex);
            ArrayTextureDataRef.MipmapDataArray.resize(ImageInfo.MipmapCount);
            for (uint32_t MipmapIndex = 0; MipmapIndex < ImageInfo.MipmapCount; MipmapIndex++)
            {
                auto& MipmapDataRef = ArrayTextureDataRef.MipmapDataArray.at(MipmapIndex);
                MipmapDataRef.MipIndex = MipmapIndex;
                const auto Ok = ImageLoader->GetSubRawData(MipmapDataRef.Data, MipmapIndex, ArrayIndex);
                if (!Ok)
                {
                    assert(false);
                    PlatformData = nullptr;
                    return false;
                }
            }
        }

        PlatformData->Width = ImageInfo.Width;
        PlatformData->Height = ImageInfo.Height;
        PlatformData->Depth = ImageInfo.Depth;
        PlatformData->MipmapCount = ImageInfo.MipmapCount;

        this->InitResource(GetRHIInstance()->GetResourceFactory());

        return true;
    }

    std::shared_ptr<CTexture2D> CTextureSubsystem::CreateTexture(const std::filesystem::path& FilePath)
    {
        std::wstring ErrorStr;
        const auto ImageLoad = CImageLoaderManager::GetInstance()->LoadTextureByFile(FilePath, ErrorStr);
        if (!ImageLoad)
        {
            SE_ERROR(GLogTexture, L"Load Texture Error, {0}, {1}", FilePath.c_str(), ErrorStr);
            return nullptr;
        }

        auto Texture = std::make_shared<CTexture2D>();
        if (!Texture->Init(ImageLoad))
        {
            SE_ERROR(GLogTexture, L"Texture Init Error, {0}", FilePath.c_str());
            return nullptr;
        }

        return Texture;
    }

    SubsystemBase* CTextureSubsystem::Clone() const
    {
        return new CTextureSubsystem;
    }

    void CTextureSubsystem::Init(const SubsystemContainerBase& InitContext)
    {
        EngineSubsystem::Init(InitContext);
    }

    void CTextureSubsystem::Destroy()
    {
        EngineSubsystem::Destroy();
    }
}
