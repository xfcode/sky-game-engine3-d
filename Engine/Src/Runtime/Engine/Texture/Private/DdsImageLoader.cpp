﻿#include "DdsImageLoader.h"

#include <cassert>
#include <dxgiformat.h>
#include <filesystem>

#include "Math/MathLib.h"

namespace
{
    using namespace SkyGameEngine3d;

    ETextureRawDataPixelFormat Convert(DirectX::DXGI_FORMAT Format)
    {
        switch (static_cast<DXGI_FORMAT>(Format))
        {
        case DXGI_FORMAT_R8G8B8A8_UINT:
            return ETextureRawDataPixelFormat::RGBA8;
        case DXGI_FORMAT_R16G16B16A16_UINT:
            return ETextureRawDataPixelFormat::RGBA16;
        case DXGI_FORMAT_R8_UINT:
            return ETextureRawDataPixelFormat::G8;
        case DXGI_FORMAT_R16_UINT:
            return ETextureRawDataPixelFormat::G16;
        case DXGI_FORMAT_BC1_UNORM:
            return ETextureRawDataPixelFormat::BC1;
        case DXGI_FORMAT_BC2_UNORM:
            return ETextureRawDataPixelFormat::BC2;
        case DXGI_FORMAT_BC3_UNORM:
            return ETextureRawDataPixelFormat::BC3;
        case DXGI_FORMAT_BC4_UNORM:
            return ETextureRawDataPixelFormat::BC4;
        case DXGI_FORMAT_BC5_UNORM:
            return ETextureRawDataPixelFormat::BC5;
        case DXGI_FORMAT_BC6H_UF16:
            return ETextureRawDataPixelFormat::BC6;
        case DXGI_FORMAT_BC7_UNORM:
            return ETextureRawDataPixelFormat::BC7;
        default:
            assert(false);
            return ETextureRawDataPixelFormat::None;
        }
    }
    
    CImageInfo::EDimension ConvertDimension(uint32_t Dimension)
    {
        switch (Dimension)
        {
        case DirectX::DDS_DIMENSION_TEXTURE1D:
            return CImageInfo::Dimension1D;
        case DirectX::DDS_DIMENSION_TEXTURE2D:
            return CImageInfo::Dimension2D;
        case DirectX::DDS_DIMENSION_TEXTURE3D:
            return CImageInfo::Dimension3D;
        default:
            assert(false);
            return CImageInfo::Dimension2D;
        }
    }
}

namespace SkyGameEngine3d
{
    using namespace std;

    TStaticLazyExec RegisterFactory([]
        {
            CImageLoaderManager::GetInstance()->RegisterLoaderFactory(std::make_unique<CDdsImageLoaderFactory>());
        }
    );

    bool CDdsImageLoaderFactory::IsSupportImageType(EImageType ImageType)
    {
        return ImageType == EImageType::DDS;
    }

    std::shared_ptr<CImageLoader> CDdsImageLoaderFactory::Create()
    {
        return std::make_shared<CDdsImageLoader>();
    }

    CDdsImageLoader::~CDdsImageLoader()
    {
    }

    bool CDdsImageLoader::GetRawData(std::vector<uint8_t>& OutRawData)
    {
        if (bIsInit)
        {
            OutRawData.resize(RawData.size());
            std::memcpy(OutRawData.data(), RawData.data(), RawData.size());
            return true;
        }
        return false;
    }

    bool CDdsImageLoader::GetSubRawData(std::vector<uint8_t>& OutRawData, uint32_t MipmapIndex, uint32_t ArrayIndex)
    {
        if (bIsInit)
        {
            if (MipmapIndex >= ImageInfo.MipmapCount || ArrayIndex >= ImageInfo.ArrayCount)
            {
                return false;
            }
   
            auto PreImageSizeCall = [this, MipmapCount = ImageInfo.MipmapCount]()
            {
                uint32_t Size = 0;
                for (uint32_t MipIndex = 0; MipIndex < MipmapCount; MipIndex++)
                {
                    Size += ImageHelper::CalculateImageSize(ImageInfo, MipIndex);
                }
                return Size;
            };
            
            
            auto MipmapOffsetCall = [this](uint32_t Index)
            {
                uint32_t Size = 0;
                for (uint32_t MipMapIndex = 0; MipMapIndex < Index; MipMapIndex++)
                {
                    Size += ImageHelper::CalculateImageSize(ImageInfo, MipMapIndex);
                }
                return Size;
            };

            uint32_t Offset = 0;
            if (ArrayIndex > 0)
            {
                Offset += ArrayIndex * PreImageSizeCall();
            }

            Offset += MipmapOffsetCall(MipmapIndex);

            const auto MipmapSize = ImageHelper::CalculateImageSize(ImageInfo, MipmapIndex);
            assert(MipmapSize <= (RawData.size() - Offset));
            if (MipmapSize > (RawData.size() - Offset))
            {
                return false;
            }
            
            OutRawData.resize(MipmapSize);
            std::memcpy(OutRawData.data(), RawData.data() + Offset, OutRawData.size());
            return true;
        }
        return false;
    }

    CImageInfo CDdsImageLoader::GetImageInfo()
    {
        return ImageInfo;
    }

    ETextureRawDataPixelFormat CDdsImageLoader::GetRawDataPixelFormat() const
    {
        return ImageInfo.RawDataPixelFormat;
    }

    bool CDdsImageLoader::LoadImage(const std::filesystem::path& FilePath, std::wstring& OutError)
    {
        assert(std::filesystem::exists(FilePath));
        std::fstream DdsFileStream;
        DdsFileStream.open(FilePath, ios_base::in);
        if (!DdsFileStream.is_open())
        {
            OutError = format(L"打开文件失败 {0}", FilePath.c_str());
            return false;
        }

        TScopeExec CloseFile([]
                             {
                             },
                             [&]
                             {
                                 DdsFileStream.close();
                             }
        );

        constexpr auto MinHeadSize = sizeof(uint32_t) + sizeof(DDSHeader);
        constexpr auto HeadSizeDX10Ex = MinHeadSize + sizeof(DirectX::DDS_HEADER_DXT10);

        const auto FileSize = file_size(FilePath);
        if (FileSize < MinHeadSize)
        {
            OutError = format(L"无法解析文件头 {0}", FilePath.c_str());
            return false;
        }

        // https://learn.microsoft.com/zh-cn/windows/win32/direct3ddds/dx-graphics-dds-pguide

        // 首先是一个32位的魔数
        uint32_t Magic = 0;
        DdsFileStream.read(reinterpret_cast<char*>(&Magic), sizeof(Magic));

        if (Magic != DirectX::DDS_MAGIC)
        {
            OutError = format(L"无法解析文件 {0}", FilePath.c_str());
            return false;
        }

        // 解析头部
        DdsFileStream.read(reinterpret_cast<char*>(&DDSHeader), sizeof(DDSHeader));

        // 头部的大小必须匹配
        // 像素格式的大小也必须匹配
        if (DDSHeader.size != sizeof(DDSHeader)
            || DDSHeader.ddspf.size != sizeof(DirectX::DDS_PIXELFORMAT))
        {
            OutError = format(L"无法解析文件 {0}, 文件格式损坏", FilePath.c_str());
            return false;
        }

        if (!MathLib::IsPowerOfTow(DDSHeader.width) || !MathLib::IsPowerOfTow(DDSHeader.height))
        {
            OutError = format(L"无法解析文件 {0}, 不支持非2的次幂的图", FilePath.c_str());
            return false;
        }

        // 检查有没有Dx10的扩展数据
        if ((DDSHeader.ddspf.flags & DDS_FOURCC)
            && MAKEFOURCC('D', 'X', '1', '0') == DDSHeader.ddspf.fourCC)
        {
            if (FileSize < HeadSizeDX10Ex)
            {
                OutError = format(L"无法解析文件 {0}, 文件格式损坏", FilePath.c_str());
                return false;
            }

            DirectX::DDS_HEADER_DXT10 TempHeaderDxt10{ };
            DdsFileStream.read(reinterpret_cast<char*>(&TempHeaderDxt10), sizeof(TempHeaderDxt10));
            HeaderDxt10.emplace(TempHeaderDxt10);
        }
        else
        {
            assert(false);
            return false;
        }

        const auto DataSize = FileSize - (HeaderDxt10.has_value() ? HeadSizeDX10Ex : MinHeadSize);
        assert(DataSize >0);

        RawData.resize(DataSize);
        DdsFileStream.read(reinterpret_cast<char*>(RawData.data()), static_cast<uint32_t>(DataSize));

        ImageInfo.MipmapCount = DDSHeader.mipMapCount;
        if (HeaderDxt10.has_value())
        {
            ImageInfo.ArrayCount = HeaderDxt10->arraySize;
            ImageInfo.RawDataPixelFormat = Convert(HeaderDxt10->dxgiFormat);
            ImageInfo.ImageDimension = ConvertDimension(HeaderDxt10->resourceDimension);
        }

        ImageInfo.Width = DDSHeader.width;
        ImageInfo.Height = DDSHeader.height;
        ImageInfo.Depth = DDSHeader.depth;
        
        bIsInit = true;
        return true;
    }
}
