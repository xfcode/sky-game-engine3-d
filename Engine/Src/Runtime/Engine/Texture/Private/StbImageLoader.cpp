﻿#include "StbImageLoader.h"

#include <cassert>
#include <filesystem>
#include <set>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "Misc/StringUtility.h"

namespace SkyGameEngine3d
{
    namespace
    {
        [[maybe_unused]] TStaticLazyExec RegisterFactory([]() -> void
            {
                CImageLoaderManager::GetInstance()->RegisterLoaderFactory(std::make_unique<CStbImageLoaderFactory>());
            }
        );

        std::set<std::wstring> SupportExtension{
            L".png",
        };
    }

    bool CStbImageLoaderFactory::IsSupportImageType(EImageType ImageType)
    {
        static std::set SupportTypes{
            EImageType::Png,
        };
        return SupportTypes.contains(ImageType);
    }

    std::shared_ptr<CImageLoader> CStbImageLoaderFactory::Create()
    {
        return std::make_shared<CStbImageLoader>();
    }

    CStbImageLoader::~CStbImageLoader()
    {
    }

    bool CStbImageLoader::GetRawData(std::vector<uint8_t>& OutRawData)
    {
        if (bIsInit)
        {
            OutRawData.resize(RawData.size());
            std::memcpy(OutRawData.data(), RawData.data(), RawData.size());
            return true;
        }
        return false;
    }

    ETextureRawDataPixelFormat CStbImageLoader::GetRawDataPixelFormat() const
    {
        return ImageInfo.RawDataPixelFormat;
    }

    bool CStbImageLoader::LoadImage(const std::filesystem::path& FilePath, std::wstring& OutError)
    {
        if (!exists(FilePath))
        {
            OutError = std::format(L"文件 {0} 不存在", FilePath.c_str());
            return false;
        }

        const auto Extension = FilePath.extension();
        if (!SupportExtension.contains(Extension))
        {
            OutError = std::format(L"文件 {0} 不支持", FilePath.c_str());
            return false;
        }

        const auto FilePathString = WCharToAnsi(FilePath.c_str());
        const bool Is16Bit = stbi_is_16_bit(FilePathString.c_str());
        int32_t X = 0;
        int32_t Y = 0;
        int32_t ChannelNum = 0;
        if (!stbi_info(FilePathString.c_str(), &X, &Y, &ChannelNum))
        {
            OutError = std::format(L"文件 {0} 获取信息失败", FilePath.c_str());
            return false;
        }

        assert(ChannelNum != 0);

        std::vector<uint8_t> Data;
        const int32_t BytesSize = Y * X * ChannelNum * (Is16Bit ? 2 : 1);
        assert(BytesSize >= 0 && "数据一定大于等于0");
        Data.resize(static_cast<std::vector<uint8_t>::size_type>(BytesSize), 0);
        // NOLINT(bugprone-misplaced-widening-cast)

        uint8_t* LoadDataPtr = nullptr;
        if (Is16Bit)
        {
            auto TempX = 0;
            auto TempY = 0;
            auto TempChannelNum = 0;
            if (const auto LoadValue = stbi_load_16(FilePathString.c_str(), &TempX, &TempY, &TempChannelNum, 0))
            {
                assert(TempX == X);
                assert(TempY == Y);
                assert(TempChannelNum == ChannelNum);
                LoadDataPtr = reinterpret_cast<uint8_t*>(LoadValue);
            }
        }
        else
        {
            auto TempX = 0;
            auto TempY = 0;
            auto TempChannelNum = 0;
            if (const auto LoadValue = stbi_load(FilePathString.c_str(), &TempX, &TempY, &TempChannelNum, 0))
            {
                assert(TempX == X);
                assert(TempY == Y);
                assert(TempChannelNum == ChannelNum);
                LoadDataPtr = reinterpret_cast<uint8_t*>(LoadValue);
            }
        }

        assert(LoadDataPtr != nullptr);
        if (LoadDataPtr)
        {
            std::memcpy(Data.data(), LoadDataPtr, BytesSize);
            stbi_image_free(LoadDataPtr);
        }
        else
        {
            OutError = std::format(L"文件 {0} 加载失败", FilePath.c_str());
            return false;
        }

        RawData = std::move(Data);

        auto& TextureRawDataPixelFormat = ImageInfo.RawDataPixelFormat;
        switch (ChannelNum)
        {
        case 1:
            TextureRawDataPixelFormat = Is16Bit ? ETextureRawDataPixelFormat::G16 : ETextureRawDataPixelFormat::G8;
            break;
        case 3:
            TextureRawDataPixelFormat = Is16Bit ? ETextureRawDataPixelFormat::RGB16 : ETextureRawDataPixelFormat::RGB8;
            break;
        case 4:
            TextureRawDataPixelFormat =
                Is16Bit ? ETextureRawDataPixelFormat::RGBA16 : ETextureRawDataPixelFormat::RGBA8;
            break;
        default:
            assert(false && "没有实现");
            OutError = std::format(L"文件 {0} 未实现的格式", FilePath.c_str());
            return false;
        }

        ImageInfo.ImageDimension = CImageInfo::Dimension2D;
        ImageInfo.MipmapCount = 1;
        ImageInfo.ArrayCount = 1;
        
        bIsInit = true;
        return true;
    }

    bool CStbImageLoader::GetSubRawData(std::vector<uint8_t>& OutRawData, uint32_t MipmapIndex, uint32_t ArrayIndex)
    {
        if (MipmapIndex == 0 && ArrayIndex == 0)
        {
            return GetRawData(OutRawData);
        }
        return false;
    }

    CImageInfo CStbImageLoader::GetImageInfo()
    {
        return ImageInfo;
    }
}
