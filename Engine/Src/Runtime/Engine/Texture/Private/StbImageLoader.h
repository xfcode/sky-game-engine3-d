﻿#pragma once
#include "Texture/Public/ImageLoader.h"

namespace SkyGameEngine3d
{
    class CStbImageLoaderFactory :
        public CImageLoaderFactory
    {
    public:
        [[nodiscard]] virtual bool IsSupportImageType(EImageType ImageType) override;
        [[nodiscard]] virtual std::shared_ptr<CImageLoader> Create() override;
    };

    class CStbImageLoader : public CImageLoader
    {
    public:
        virtual ~CStbImageLoader() override;

        virtual bool GetRawData(std::vector<uint8_t>& OutRawData) override;

        [[nodiscard]] virtual ETextureRawDataPixelFormat GetRawDataPixelFormat() const override;

        virtual bool LoadImage(const std::filesystem::path& FilePath, std::wstring& OutError) override;
        
        virtual bool GetSubRawData(std::vector<uint8_t>& OutRawData, uint32_t MipmapIndex, uint32_t ArrayIndex) override;

        virtual CImageInfo GetImageInfo() override;

    private:
        std::vector<uint8_t> RawData;
        
        CImageInfo ImageInfo = {};
        
        bool bIsInit = false;
    };
}
