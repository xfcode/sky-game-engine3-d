﻿#include "GeometryGenerator.h"

namespace SkyGameEngine3d
{
    GeometryGenerator::MeshData GeometryGenerator::CreateCylinder(
        float BottomRadius,
        float TopRadius,
        float Height,
        uint32_t SliceCount,
        uint32_t StackCount
    )
    {
        MeshData RetData;

        float StackHeight = Height / static_cast<float>(StackCount);
        float RadiusStep = (TopRadius - BottomRadius) / static_cast<float>(StackCount);
        uint32_t RingCount = StackCount + 1;

        float BaseR = BottomRadius;
        float BaseZ = -0.5f * Height;

        // 每个纵向分块的角度
        float Theta = PI2 / SliceCount;

        struct SliceData
        {
            float SinValue;
            float ConsValue;
        };
        std::vector<SliceData> SliceDataArray;
        SliceDataArray.reserve(SliceCount);

        for (int I = 0; I < SliceCount; ++I)
        {
            SliceDataArray.push_back(SliceData{ sin(Theta), cos(Theta) });
        }

        for (int RingIndex = 0; RingIndex < RingCount; ++RingIndex)
        {
            float R = BaseR + RadiusStep * static_cast<float>(RingIndex);
            float Z = BaseZ + StackHeight * static_cast<float>(RingIndex);

            // 计算每个环上的顶点
            for (int SliceIndex = 0; SliceIndex < SliceCount; ++SliceIndex)
            {
                Vertex Vertex;

                const auto& [SinValue, CosValue] = SliceDataArray[SliceIndex];
                Vertex.Pos = { CosValue * R, SinValue * R, Z };
                Vertex.Tex = Vector2D{
                    static_cast<float>(SliceIndex) / static_cast<float>(SliceCount),
                    1.0f - static_cast<float>(RingIndex) / static_cast<float>(RingCount),
                };
                Vertex.Tangent = { -CosValue, SinValue, 0 };

                // 计算副切线
                float Dr = BottomRadius - TopRadius;
                Vector3D Bitangent(Dr * CosValue, Dr * SinValue, -Height);

                // 计算切线
                Vertex.Normal = MathLib::CrossProduct(Vertex.Tangent, Bitangent);
                Vertex.Normal.Normalize();

                RetData.Vertices.push_back(Vertex);
            }
        }

        // 让开头的和最后一个顶点重合
        uint32_t RingVertexCount = SliceCount + 1;

        for (int StackIndex = 0; StackIndex < StackCount; ++StackIndex)
        {
            for (int SliceIndex = 0; SliceIndex < SliceCount; ++SliceIndex)
            {
                RetData.Indices.push_back(StackIndex * RingVertexCount + SliceIndex);
                RetData.Indices.push_back(StackIndex + 1 * RingVertexCount + SliceIndex);
                RetData.Indices.push_back(StackIndex + 1 * RingVertexCount + SliceIndex + 1);

                RetData.Indices.push_back(StackIndex * RingVertexCount + SliceIndex);
                RetData.Indices.push_back(StackIndex + 1 * RingVertexCount + SliceIndex + 1);
                RetData.Indices.push_back(StackIndex * RingVertexCount + SliceIndex + 1);
            }
        }

        {
            //计算 顶面的三角形
            float TopZ = 0.5 * Height;
            uint16_t BaseIndex = RetData.Vertices.size();
            for (int SliceIndex = 0; SliceIndex < SliceCount; ++SliceIndex)
            {
                const auto& [SinValue, CosValue] = SliceDataArray[SliceIndex];
                float X = TopRadius * CosValue;
                float Y = TopRadius * SinValue;

                Vertex VertexValue;
                VertexValue.Pos = { X, Y, TopZ };
                VertexValue.Normal = { 0.f, 0.f, 1.f };
                VertexValue.Tangent = { 1.f, 0.f, 0.f };
                VertexValue.Tex = Vector2D{
                    X / Height + 0.5f,
                    Y / Height + 0.5f,
                };

                RetData.Vertices.push_back(VertexValue);
            }

            // 中心点
            RetData.Vertices.push_back(Vertex{
                    .Pos = { 0.f, 0.f, TopZ },
                    .Normal = { 0.f, 0.f, 1.f },
                    .Tangent = { 1.f, 0.f, 0.f },
                    .Tex = { 0.5f, 0.5f },
                }
            );

            for (int I = 0; I < SliceCount; ++I)
            {
                RetData.Indices.push_back(RetData.Vertices.size());
                RetData.Indices.push_back(BaseIndex + I + 1);
                RetData.Indices.push_back(BaseIndex + I);
            }
        }

        {
            //计算 底面的三角形
            float BottomZ = -0.5 * Height;
            uint16_t BaseIndex = RetData.Vertices.size();
            for (int SliceIndex = 0; SliceIndex < SliceCount; ++SliceIndex)
            {
                const auto& [SinValue, CosValue] = SliceDataArray[SliceIndex];
                float X = TopRadius * CosValue;
                float Y = TopRadius * SinValue;

                Vertex VertexValue;
                VertexValue.Pos = { X, Y, BottomZ };
                VertexValue.Normal = { 0.f, 0.f, -1.f };
                VertexValue.Tangent = { 1.f, 0.f, 0.f };
                VertexValue.Tex = Vector2D{
                    X / Height + 0.5f,
                    Y / Height + 0.5f,
                };

                RetData.Vertices.push_back(VertexValue);
            }

            // 中心点
            RetData.Vertices.push_back(Vertex{
                    .Pos = { 0.f, 0.f, BottomZ },
                    .Normal = { 0.f, 0.f, -1.f },
                    .Tangent = { 1.f, 0.f, 0.f },
                    .Tex = { 0.5f, 0.5f },
                }
            );

            for (int I = 0; I < SliceCount; ++I)
            {
                RetData.Indices.push_back(RetData.Vertices.size());
                RetData.Indices.push_back(BaseIndex + I + 1);
                RetData.Indices.push_back(BaseIndex + I);
            }
        }

        return RetData;
    }
}
