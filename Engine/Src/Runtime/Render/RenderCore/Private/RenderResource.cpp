#include "RenderResource.h"

namespace SkyGameEngine3d
{
    void CRenderResource::InitResource(IRHIResourceFactory* Factory)
    {
        RHIResourceFactory = Factory;
        this->InitRHIResource(Factory);
    }

    void CRenderResource::ReleaseResource()
    {
        this->ReleaseRHIResource();

        RHIResourceFactory = nullptr;
    }

    CRenderResource::~CRenderResource()
    {
    }
}
