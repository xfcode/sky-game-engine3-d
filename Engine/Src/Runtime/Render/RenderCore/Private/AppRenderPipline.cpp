﻿#include "AppRenderPipeline.h"
#include "IRHIResourceFactory.h"
#include "RHI.h"
#include "RHICoreModuleInterface.h"
#include "UIRenderTypes.h"

namespace SkyGameEngine3d
{
    class UIElement
    {
    public:
        void AddVertex(const UIVertex& Vertex)
        {
            VertexArray.push_back(Vertex);
        }

        void AddIndex(uint32_t Index)
        {
            IndexArray.push_back(Index);
        }

    private:
        std::vector<UIVertex> VertexArray;
        std::vector<uint32_t> IndexArray;
    };


    struct DefaultAppUIElementCollector : public IAppUIElementCollector
    {
        virtual void AddImage(const Geometry& InGeometry, const UITextureProxy& TextureProxy) override
        {
            UIElement Element;
            Element.AddVertex(UIVertex::Make(Vector2D{ 0, 0 }, Vector2D{ 0, 0 }));
            Element.AddVertex(UIVertex::Make(Vector2D{ 1, 0 }, Vector2D{ 1, 0 }));
            Element.AddVertex(UIVertex::Make(Vector2D{ 1, 1 }, Vector2D{ 1, 0 }));
            Element.AddVertex(UIVertex::Make(Vector2D{ 0, 1 }, Vector2D{ 0, 1 }));

            Element.AddIndex(0);
            Element.AddIndex(1);
            Element.AddIndex(2);

            Element.AddIndex(0);
            Element.AddIndex(2);
            Element.AddIndex(3);

            UIElements.push_back(Element);
        }

    private:
        std::vector<UIElement> UIElements;
    };

    std::shared_ptr<AppRenderPipeline> AppRenderPipeline::CreatePipeline(const CreatePipelineArg& Arg)
    {
        return nullptr;
    }

    void AppRenderPipeline::Render(RHICommandList* CommandList)
    {
        return;
        
        CommandList->CreateUIRootSignature();

        const std::shared_ptr<RHIVertexDeclare> Declare = std::make_shared<RHIVertexDeclare>();
        Declare->AddDeclare(VERTEX_ELEMENT_DECLARE(0, 0, UIVertex, Pos));
        Declare->AddDeclare(VERTEX_ELEMENT_DECLARE(1, 0, UIVertex, UV));

        const RHIShaderCreateArgs VsShaderCreateArgs =
        {
            .ShaderType = EShaderType::Vertex,
            .ShaderPath = L"",
            .MainName = "VS"
        };

        const RHIShaderCreateArgs PsShaderCreateArgs =
        {
            .ShaderType = EShaderType::Pixel,
            .ShaderPath = L"",
            .MainName = "PS"
        };

        const auto RHIIns = GetRHIInstance();

        const auto ResFactory = RHIIns->GetResourceFactory();

        RHIGraphicsPipelineState PipelineState;
        PipelineState.ShaderStateInput = RHIGraphicsShaderStateInput
        {
            .VertexDeclare = Declare,
            .VS = ResFactory->CreateShader(VsShaderCreateArgs),
            .PS = ResFactory->CreateShader(PsShaderCreateArgs),
        };

        CommandList->SetGraphicsPipelineState(PipelineState);
    }
}
