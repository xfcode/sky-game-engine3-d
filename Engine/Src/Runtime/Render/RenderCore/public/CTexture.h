﻿#pragma once

#include "CRHIResource.h"
#include "RenderCoreModuleDef.h"
#include "RHITypes.h"

namespace SkyGameEngine3d
{
    class CTexture : public CRHIResource
    {
    public:


    private:
        RHITexturePtr RHITexture;
    };

    class CTexture2D : public CTexture
    {
    };

    class CTextureRenderTargetBase : public CTexture
    {
    };

    class CTextureRenderTarget2D : public CTextureRenderTargetBase
    {
    };
}
