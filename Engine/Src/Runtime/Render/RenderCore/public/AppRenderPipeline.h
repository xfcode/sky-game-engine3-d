﻿#pragma once

#include <memory>

#include "RenderCoreModuleDef.h"
#include "RHICommandList.h"
#include "RHITypes.h"
#include "Misc/UtilityTypes.h"

namespace SkyGameEngine3d
{
    class Geometry;
    class RHICommandList;

    struct UITextureProxy
    {
        RHITexturePtr RHITexture;
    };


    struct RENDERCORE_API IAppUIElementCollector : public CNonCopyable
    {
        virtual ~IAppUIElementCollector() = default;

        virtual void AddImage(const Geometry& InGeometry, const UITextureProxy& TextureProxy) = 0;
    };


    class AppRenderPipeline : public CNonCopyable
    {
        struct CreatePipelineArg
        {
        };

        static std::shared_ptr<AppRenderPipeline> CreatePipeline(const CreatePipelineArg& Arg);

    public:
        virtual IAppUIElementCollector* GetUIElementCollector()
        {
            return ElementCollector.get();
        }

        virtual ~AppRenderPipeline() = default;


        virtual void Render(RHICommandList* CommandList);

    private:
        std::shared_ptr<IAppUIElementCollector> ElementCollector;
    };
}
