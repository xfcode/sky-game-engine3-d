#pragma once

#include "HAL/PlatformDefine.h"

#ifdef RENDER
    #define RENDER_API DLL_EXPORT
#else
#define RENDER_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameRender = L"Render";
