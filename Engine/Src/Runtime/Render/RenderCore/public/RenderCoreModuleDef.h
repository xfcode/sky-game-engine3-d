#pragma once

#include "HAL/PlatformDefine.h"

#ifdef RENDERCORE
    #define RENDERCORE_API DLL_EXPORT
#else
    #define RENDERCORE_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameRenderCore = L"RenderCore";
