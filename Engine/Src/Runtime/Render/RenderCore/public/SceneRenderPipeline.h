﻿#pragma once

#include <memory>

#include "RenderCoreModuleDef.h"
#include "Misc/UtilityTypes.h"

namespace SkyGameEngine3d
{
    class RHICommandList;
}

namespace SkyGameEngine3d
{
    // 场景的不同视图
    struct SceneView
    {
        std::shared_ptr<class CRHITexture> RenderTarget;
    };

    class RENDERCORE_API SceneRenderPipeline : public CNonCopyable
    {
    public:
        struct CreatePipelineArg
        {
        };

        static std::shared_ptr<SceneRenderPipeline> CreatePipeline(const CreatePipelineArg& Arg)
        {
            return nullptr;
        }

    public:
        virtual ~SceneRenderPipeline() = default;

        virtual void Render(const SceneView& InSceneView, const RHICommandList* InCommandList) = 0;
    };
}
