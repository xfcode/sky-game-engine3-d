﻿#pragma once

#include "Math/Vector.h"

namespace SkyGameEngine3d
{
    class GeometryGenerator
    {
        struct Vertex
        {
            Vector3D Pos;
            Vector3D Normal;
            Vector3D Tangent;
            Vector2D Tex;
        };

        class MeshData
        {
        public:
            std::vector<Vertex> Vertices;

            std::vector<uint16_t> Indices;


            [[nodiscard]] const std::vector<Vertex>& GetVertices() const
            {
                return Vertices;
            }

            [[nodiscard]] const std::vector<uint16_t>& GetIndices() const
            {
                return Indices;
            }
        };

        //! 创建一个圆柱几何数据，以 圆柱中心 为原点
        //! \param BottomRadius 底边的半径
        //! \param TopRadius 顶部的半径
        //! \param Height 高度
        //! \param SliceCount 纵向切分的块数
        //! \param StackCount 横向的层数
        //! \return 生成的 MeshData
        [[nodiscard]] static MeshData CreateCylinder(
            float BottomRadius, float TopRadius, float Height, uint32_t SliceCount, uint32_t StackCount
        );
    };
}
