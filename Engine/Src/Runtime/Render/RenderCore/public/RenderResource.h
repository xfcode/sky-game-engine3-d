#pragma once

#include <memory>

#include "CRHIResource.h"
#include "IRHIResourceFactory.h"
#include "RenderCoreModuleDef.h"


namespace SkyGameEngine3d
{
    class CRHIBuffer;
}

namespace SkyGameEngine3d
{
    // 对RHI层的资源进行封装
    class RENDERCORE_API CRenderResource
    {
    public:
        void InitResource(IRHIResourceFactory* Factory);

        void ReleaseResource();

        virtual ~CRenderResource();

    protected:
        virtual void InitRHIResource(IRHIResourceFactory* Factory) = 0;

        virtual void ReleaseRHIResource() = 0;

    protected:
        IRHIResourceFactory* RHIResourceFactory = nullptr;
    };


    class RENDERCORE_API CVertexBufferBase : public CRenderResource
    {
    protected:

        virtual void ReleaseRHIResource() override
        {
            RHIBufferPtr = nullptr;
        }

    public:

        [[nodiscard]] TRHIResourcePtr<CRHIBuffer> GetRHIBuffer()const
        {
            return RHIBufferPtr;
        }
        
    protected:
        
        TRHIResourcePtr<CRHIBuffer> RHIBufferPtr;
    };

    class RENDERCORE_API CIndexBufferBase : public CRenderResource
    {
    protected:
        virtual void ReleaseRHIResource() override
        {
            RHIBuffer = nullptr;
        }

    public:

        [[nodiscard]] TRHIResourcePtr<CRHIBuffer> GetRHIBuffer()const
        {
            return RHIBuffer;
        }

    protected:
        
        TRHIResourcePtr<CRHIBuffer> RHIBuffer;
    };
}
