﻿#pragma once

#include "RenderCoreModuleDef.h"
#include "RenderResource.h"
#include "Math/Vector.h"

namespace SkyGameEngine3d
{
    struct RENDERCORE_API UIVertex
    {
        Vector2D Pos;

        Vector2D UV;

        static UIVertex Make(const Vector2D& Pos, const Vector2D& UV)
        {
            UIVertex Ret;
            Ret.Pos = Pos;
            Ret.UV = UV;
            return Ret;
        }
    };


    template <typename VertexType>
    class TUIVertexBuffer : public CVertexBufferBase
    {
    protected:
        virtual void InitRHIResource(IRHIResourceFactory* Factory) override
        {
            this->Resize(100);
        }

    public:
        /**
         * \brief 预先分配缓存的大小
         * \param VertexCount 
         */
        void Resize(const uint32_t VertexCount)
        {
            const auto ResizeBufferSize = VertexCount * sizeof VertexType;
            if (ResizeBufferSize <= BufferSize)
            {
                return;
            }

            const RHICreateBufferArgs Args
            {
                .InBufferFlag = ERHIBufferFlag::VertexBuffer | ERHIBufferFlag::Dynamic,
                .InBufferSize = static_cast<uint32_t>(ResizeBufferSize),
                .InStride = sizeof(VertexType),
            };
            RHIBufferPtr = RHIResourceFactory->CreateBuffer(Args);

            BufferSize = ResizeBufferSize;
        }

        void FillBuffer(const VertexType* DataPtr, uint32_t DataCount)
        {
            this->Resize(DataCount);

            void* Ptr = RHIBufferPtr->Lock();
            assert(Ptr);
            std::memcpy(Ptr, DataPtr, DataCount * sizeof(VertexType));
            RHIBufferPtr->Unlock();
        }
        
    private:
        uint32_t BufferSize = 0;
    };

    class RENDERCORE_API CUIIndexBuffer : public CIndexBufferBase
    {
    protected:
        virtual void InitRHIResource(IRHIResourceFactory* Factory) override
        {
            this->Resize(50);
        }

    public:
        using UIIndexType = uint16_t;

        void Resize(const uint32_t IndexCount)
        {
            const auto ResizeBufferSize = IndexCount * sizeof(UIIndexType);
            if (ResizeBufferSize <= 0)
            {
                return;
            }

            const RHICreateBufferArgs Args
            {
                .InBufferFlag = ERHIBufferFlag::IndexBuffer | ERHIBufferFlag::Dynamic,
                .InBufferSize = static_cast<uint32_t>(ResizeBufferSize),
                .InStride = sizeof(UIIndexType),
            };

            RHIBuffer = RHIResourceFactory->CreateBuffer(Args);

            BufferSize = static_cast<uint32_t>(ResizeBufferSize);
        }

        void FillBuffer(const UIIndexType* DataPtr, uint32_t DataCount)
        {
            this->Resize(DataCount);

            void* Ptr = RHIBuffer->Lock();
            assert(Ptr);
            std::memcpy(Ptr, DataPtr, DataCount * sizeof(UIIndexType));
            RHIBuffer->Unlock();
        }

    private:
        uint32_t BufferSize = 0;
    };
}
