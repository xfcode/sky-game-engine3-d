#pragma once

#include "RHICoreModuleDef.h"

namespace SkyGameEngine3d
{
    RHICORE_API void InitHRI();

    RHICORE_API void ShotdownRHI();

    RHICORE_API class CRHIInstance* GetRHIInstance();
}
