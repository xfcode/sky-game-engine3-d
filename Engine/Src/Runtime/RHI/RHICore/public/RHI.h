#pragma once

#include "RHICoreModuleDef.h"
#include "ModuleManager/ModuleInterface.h"
#include <memory>

#include "RHITypes.h"

namespace SkyGameEngine3d
{
    class CRHITexture;

    class RHICORE_API IRHIModule : public IModuleInterface
    {
    public:
        virtual class CRHIInstance* CreateRHI() = 0;

        virtual void DestroyRHI(class CRHIInstance* RHI) = 0;
    };


    class RHICORE_API CRHIInstance : public CNonCopyable
    {
    public:
        virtual ~CRHIInstance() = default;

        virtual void Init() = 0;

        virtual void Shutdown() = 0;

        [[nodiscard]] virtual class IRHICommandContext* GetCommandContext() { return nullptr; }

        [[nodiscard]] virtual class IRHIResourceFactory* GetResourceFactory() const = 0;

    private:
        virtual void OnCleanupPreFrame() = 0;

    public:
        void CleanupPreFrame();
    };
}
