#pragma once

#include "RHICoreModuleDef.h"
#include "CRHIResource.h"

namespace SkyGameEngine3d
{
    class RHICORE_API RHIViewport
        : public CRHIResource
    {
    public:
        RHIViewport();
        virtual ~RHIViewport() override;
    };
}
