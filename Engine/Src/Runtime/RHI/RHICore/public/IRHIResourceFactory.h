#pragma once

#include <memory>

#include "RHICoreModuleDef.h"
#include "RHITypes.h"
#include "CRHIResource.h"
#include "RHIViewport.h"

namespace SkyGameEngine3d
{
    struct RHIViewportCreateArg
    {
        void* WindowHandle = nullptr;
        int32_t WindowW = 0;
        int32_t WindowH = 0;
        EPixelFormat BackBufferFormat = EPixelFormat::None;
    };

    struct RHITextureCreateArgs
    {
        ETextureDimension Dimension = ETextureDimension::Texture2D;

        ETextureFlags Flags = ETextureFlags::None;

        EPixelFormat Format = EPixelFormat::None;

        Vector2DInt Size;

        uint32_t MipsCount = 0;

        uint32_t SampleCount = 1;

        std::wstring DebugName;

        static RHITextureCreateArgs Make2DTextureArgs(std::wstring_view DebugName)
        {
            RHITextureCreateArgs Args;
            Args.Dimension = ETextureDimension::Texture2D;
            Args.DebugName = DebugName;
            return Args;
        }

        RHITextureCreateArgs& SetSize(const Vector2DInt& InSize)
        {
            this->Size = InSize;
            return *this;
        }

        RHITextureCreateArgs& SetFormat(const EPixelFormat InFormat)
        {
            this->Format = InFormat;
            return *this;
        }

        RHITextureCreateArgs& SetFlags(ETextureFlags InFlags)
        {
            this->Flags = InFlags;
            return *this;
        }
    };

    struct RHIShaderCreateArgs
    {
        EShaderType ShaderType;
        std::filesystem::path ShaderPath;
        std::string MainName;
    };

    struct RHICreateBufferArgs
    {
        ERHIBufferFlag InBufferFlag;
        uint32_t InBufferSize;
        uint32_t InStride;
    };

    class RHICORE_API IRHIResourceFactory
    {
    public:
        virtual ~IRHIResourceFactory();

        [[nodiscard]] virtual TRHIResourcePtr<RHIViewport> CreateRHIViewport(const RHIViewportCreateArg& Arg) = 0;

        [[nodiscard]] virtual TRHIResourcePtr<CRHITexture> CreateTexture(const RHITextureCreateArgs& Args) = 0;

        [[nodiscard]] virtual TRHIResourcePtr<CRHIShader> CreateShader(const RHIShaderCreateArgs& Args) = 0;

        [[nodiscard]] virtual TRHIResourcePtr<CRHIBuffer> CreateBuffer(const RHICreateBufferArgs& Args) = 0;
    };
};
