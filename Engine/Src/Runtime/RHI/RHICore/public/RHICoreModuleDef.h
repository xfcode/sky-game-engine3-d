#pragma once

#include "HAL/PlatformDefine.h"

#ifdef RHICORE
    #define RHICORE_API DLL_EXPORT
#else
    #define RHICORE_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameRHICore = L"RHICore";
