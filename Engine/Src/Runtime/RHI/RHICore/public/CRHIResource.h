﻿#pragma once

#include <filesystem>
#include <functional>
#include <memory>
#include <string>

#include "RHICoreModuleDef.h"
#include "RHITypes.h"
#include "Misc/UtilityTypes.h"

namespace SkyGameEngine3d
{
    struct CResourceReleaseManager : public TSingletonInstance<CResourceReleaseManager>
    {
        std::vector<class CRHIResource*> PendingReleaseQueue;
        // 正在释放的对象
        class CRHIResource* DoingReleaseObject = nullptr;

        void Flush();
    };

    class RHICORE_API CRHIResource :
        public std::enable_shared_from_this<CRHIResource>,
        public CNonCopyable
    {
    public:
        CRHIResource()
        {
        }

        // 释放资源
        virtual ~CRHIResource();

        // 发起释放一个资源的请求，RHI 会在适当的时机进行释放
        // 释放时会调用对象的析构函数
        static void RequestReleaseResource(CRHIResource* Resource);
    };


    template <typename ResourceType>
        requires(std::is_base_of_v<CRHIResource, ResourceType>)
    using TRHIResourcePtr = std::shared_ptr<ResourceType>;

    template <typename ResourceType>
        requires(std::is_base_of_v<CRHIResource, ResourceType>)
    TRHIResourcePtr<ResourceType> MakeRHIResource(const std::function<ResourceType*()>& Construct)
    {
        auto Res = Construct();

        static auto Deleter = [](ResourceType* Obj)
        {
            CRHIResource::RequestReleaseResource(Obj);
        };

        auto Ret = std::shared_ptr<ResourceType>(Res, Deleter);

        return Ret;
    }

    template <typename DesResourceType, typename SrcResourceType>
        requires(std::is_base_of_v<CRHIResource, DesResourceType> && std::is_base_of_v<CRHIResource, SrcResourceType>)
    TRHIResourcePtr<DesResourceType> ResourceCast(TRHIResourcePtr<SrcResourceType> SrcObject)
    {
        return std::static_pointer_cast<DesResourceType>(SrcObject);
    }

    enum class EResourceLockMode: uint8_t
    {
        ReadOnly,
        WriteOnly,
    };


    class RHICORE_API CRHIBuffer : public CRHIResource
    {
    public:
        virtual ~CRHIBuffer() override;

        //! 构造函数
        //! \param InBufferFlag buffer 的标志
        //! \param InBufferSize 缓存的大小
        //! \param InStride 数据的步长  StructuredBuffer
        CRHIBuffer(ERHIBufferFlag InBufferFlag, uint32_t InBufferSize, uint32_t InStride);

        virtual void* Lock(EResourceLockMode LockMode = EResourceLockMode::WriteOnly);

        virtual void Unlock();

        [[nodiscard]] uint32_t GetSize() const;

        [[nodiscard]] uint32_t GetStride() const;

    protected:
        uint32_t Stride = 0;
        uint32_t BufferSize = 0;
        ERHIBufferFlag BufferFlag = ERHIBufferFlag::None;
    };


    struct RHICORE_API CRHITextureDesc
    {
        #pragma region 公共定义
        ETextureDimension Dimension = ETextureDimension::Texture2D;

        ETextureFlags Flags = ETextureFlags::None;

        EPixelFormat Format = EPixelFormat::None;

        Vector2DInt Size;

        uint32_t MipsCount = 0;

        std::wstring DebugName;
        #pragma endregion
    };

    /**
     * \brief RHI 层的纹理表示
     */
    class RHICORE_API CRHITexture : public CRHIResource
    {
    public:
        explicit CRHITexture(const CRHITextureDesc& InDesc);

        virtual ~CRHITexture() override
        {
        }

        [[nodiscard]] virtual void* Lock(uint32_t MipIndex, uint32_t ArrayIndex, uint32_t& RowInBytes,
                                         EResourceLockMode LockMode = EResourceLockMode::WriteOnly);

        virtual void UnLock(uint32_t MipIndex, uint32_t ArrayIndex);

        const CRHITextureDesc& GetTextureDesc() const
        {
            return Desc;
        }

    protected:
        CRHITextureDesc Desc;
    };


    /**
     * \brief 着色器资源
     */
    class RHICORE_API CRHIShader : public CRHIResource
    {
    public:
        virtual ~CRHIShader() override = default;

        virtual void Init(const std::filesystem::path& ShaderPath, const std::string_view MainName)
        {
        }

    protected:
        explicit CRHIShader(const EShaderType InType)
            : ShaderType(InType)
        {
        }

    protected:
        EShaderType ShaderType = EShaderType::None;
    };
}
