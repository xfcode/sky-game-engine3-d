﻿#pragma once

#include <memory>
#include <vector>

#include "Math/Vector.h"
#include "MacroHelper.h"
#include "Misc/EnumClassFlag.h"
#include "RHICoreModuleDef.h"


// 定义顶点元素声明的宏
#define VERTEX_ELEMENT_DECLARE(Index, InputSlot, VertexType, MemberName) \
            SkyGameEngine3d::RHIVertexElementDeclare {                   \
                    Index,            \
                    (uint8_t) STRUCT_MEMBER_OFFSET(VertexType, MemberName),      \
                    SkyGameEngine3d::GetDataFormat<decltype(((VertexType*)nullptr)->MemberName)>(), \
                    InputSlot                                                    \
            }


namespace SkyGameEngine3d
{
    enum class ERHIVertexDataFormat : uint8_t
    {
        None,
        Float1,
        Float2,
        Float3,
        Float4,
        R8Uint,
        R16G16UInt,
        R32G32B32UInt,
        R8G8B8A8SInt,
        R8G8B8A8UInt,
    };

    enum class EPixelFormat: uint32_t
    {
        None,
        R32B32G32A32F,
        A2B10G10R10F,
        BC1,
        BC2,
        BC3,
        BC4,
        BC5,
        BC6,
        BC7,
        MaxCount,
    };

    struct CPixeFormatInfo
    {
        const wchar_t* Name = nullptr;
        EPixelFormat Format = EPixelFormat::None;

        #pragma region BlockInfo
        /*
            纹理使用基于Block的压缩技术，所以记录每种纹理格式的Block信息很重要
            可以参考 DX 的做法 https://learn.microsoft.com/en-us/windows/win32/direct3d10/d3d10-graphics-programming-guide-resources-block-compression
        */

        /**
         * \brief Block 各个轴的像素数量 x X y X z
         *  比如 BC1   BlockSizeX = 4, BlockSizeY = 4, BlockSizeZ = 1
         */
        uint32_t BlockSizeX = 0;
        uint32_t BlockSizeY = 0;
        uint32_t BlockSizeZ = 0;

        // 每个像素块占用的内存大小
        uint32_t BlockInBytes = 0;

        #pragma endregion

        uint32_t NumComponents = 0;
    };

    RHICORE_API void SetupPixeFormatInfo(const std::function<void(CPixeFormatInfo& OutInfo)>& SetupCall);

    RHICORE_API const CPixeFormatInfo& QueryPixeFormatInfo(EPixelFormat InFormat);

    struct RHIVertexElementDeclare
    {
        uint8_t Index = 0;
        uint8_t Offset = 0;
        ERHIVertexDataFormat DataFormat = ERHIVertexDataFormat::None;
        uint8_t InputSlot = 0;
    };

    struct RHIVertexDeclare
    {
        void AddDeclare(const RHIVertexElementDeclare& Declare)
        {
            VertexElementDeclareList.push_back(Declare);
        }

        std::vector<RHIVertexElementDeclare> VertexElementDeclareList;
    };

    template <typename T>
    constexpr ERHIVertexDataFormat GetDataFormat()
    {
        using DataType = std::remove_pointer_t<std::remove_cvref_t<T>>;

        if constexpr (std::is_same_v<DataType, float>) return ERHIVertexDataFormat::Float1;
        else if constexpr (std::is_same_v<DataType, Vector2D>) return ERHIVertexDataFormat::Float2;
        else if constexpr (std::is_same_v<DataType, Vector3D>) return ERHIVertexDataFormat::Float3;
        else if constexpr (std::is_same_v<DataType, Vector4D>) return ERHIVertexDataFormat::Float4;
            // 继续添加类型
        else static_assert(!std::is_same_v<DataType, DataType>, "没有考虑到的情况");
        return ERHIVertexDataFormat::None;
    }

    enum class ERHIBufferFlag : uint32_t
    {
        None = 1 << 0,

        // 缓存的类型 参考 https://learn.microsoft.com/zh-cn/windows/win32/direct3d11/direct3d-11-advanced-stages-cs-resources
        IndexBuffer = 1 << 2,
        VertexBuffer = 1 << 3,
        StructuredBuffer = 1 << 4,
        ByteAddressBuffer = 1 << 5,

        // 动态的
        Dynamic = 1 << 10,
        // 静态数据，只在创建的时候初始化一次，之后不会修改
        Static = 1 << 11,

        // 可绑定到流水线供 Shader 访问
        ShaderResource = 1 << 20,
    };

    ENUM_CLASS_FLAGS(ERHIBufferFlag);

    // 纹理的维度
    enum class ETextureDimension: uint8_t
    {
        None,
        Texture2D,
        Texture3D,
        TextureCube,
    };

    // 纹理的标志
    enum class ETextureFlags: uint32_t
    {
        None = 0,
        RenderTarget = 1 << 0,
    };

    ENUM_CLASS_FLAGS(ETextureFlags);


    using RHITexturePtr = std::shared_ptr<class CRHITexture>;

    using RHIBufferPtr = std::shared_ptr<class CRHIBuffer>;

    enum class EShaderType: uint8_t
    {
        None,
        Vertex,
        Pixel,
    };

    class RHICORE_API RHIGraphicsShaderStateInput
    {
    public:
        std::shared_ptr<RHIVertexDeclare> VertexDeclare;
        std::shared_ptr<class CRHIShader> VS;
        std::shared_ptr<class CRHIShader> PS;
    };

    class RHICORE_API RHIGraphicsPipelineState
    {
    public:
        RHIGraphicsShaderStateInput ShaderStateInput;
    };

    enum class ERHICommandQueueType: uint8_t
    {
        Graphics,
        Copy,
        Compute,

        Count,
    };

    namespace Test
    {
        struct TestVertex
        {
            float Value;
            Vector4D Pos;
        };


        inline void Test()
        {
            RHIVertexDeclare Declare;
            Declare.AddDeclare(VERTEX_ELEMENT_DECLARE(0, 0, TestVertex, Value));
            Declare.AddDeclare(VERTEX_ELEMENT_DECLARE(1, 0, TestVertex, Pos));
        }
    }
}
