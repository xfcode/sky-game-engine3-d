﻿#pragma once

#include <cassert>
#include <cstdint>
#include <algorithm>
#include <set>
#include <vector>
#include <memory>

#include "Math/MathLib.h"
#include "RHICoreModuleDef.h"

namespace SkyGameEngine3d
{
    class RHICORE_API CBuddyAllocator
    {
    public:
        explicit CBuddyAllocator(const uint32_t InBlockMemSize, const uint32_t InMaxMemSize);

        /**
        * \brief 判断能不能分配内存
        * \param MemSize 需要的内存大小
        * \param Alignment 内存的对齐要求
        * \return 
        */
        [[nodiscard]] bool CanAllocate(const uint32_t MemSize, const uint32_t Alignment = 0) const;


        struct CAllocateData
        {
            uint32_t Order = 0;
            uint32_t BlockOffset = 0;

            // 内存的偏移，已经考虑过对齐问题了
            uint32_t AlignmentMemOffset = 0;
        };

        /**
         * \brief 尝试分配内存
         * \param MemSize 需要的内存大小
         * \param Alignment 内存的对齐字节
         * \return 如果分配失败，返回 nullptr
         */
        [[nodiscard]] std::shared_ptr<CAllocateData> TryAllocate(const uint32_t MemSize, const uint32_t Alignment = 0);

        /**
         * \brief 释放内存
         * \param AllocateData 
         */
        void Free(const std::shared_ptr<CAllocateData>& AllocateData);

        /**
         * \brief 获取分配器的最大容量
         * \return 
         */
        [[nodiscard]] uint32_t GetMaxMemSize() const
        {
            return MaxMemSize;
        }


        /**
         * \brief 获取已经使用的内存的大小
         * \return 
         */
        uint32_t GetUsedMemSize() const
        {
            return UsedMemSize;
        }

    private:
        /**
        * \brief 根据需要分配的数量，计算需要分配多少个单位BlocK的内存
        * \param InAllocateSize 需要的内存大小 
        * \return 
        */
        [[nodiscard]] uint32_t AllocateSizeToBlockCount(const uint32_t InAllocateSize) const;


        /**
        * \brief 根据需要的块数，计算属于第几阶
        * 阶数为 [0, MaxOrder], 每阶能表示 2 的 n 次方个 BlockCount
        * \param BlockCount 
        * \return 
        */
        static uint32_t BlockCountToOrder(const uint32_t BlockCount);

        /**
        * \brief 根据阶数，获取当前阶可表示的块数
        * \param Order 
        * \return 
        */
        static uint32_t OrderToBlockCount(const uint32_t Order);


        /**
        * \brief 根据当前的内存偏移。计算另外一个伙伴的偏移
        * \param Offset 当前的内存偏移
        * \param BlockCount
        * \return 
        */
        [[nodiscard]] static uint32_t GetBuddyOffset(const uint32_t Offset, const uint32_t BlockCount);


        void Free(const uint32_t BlockOffset, const uint32_t Order);

        uint32_t AllocateBlock(const uint32_t Order);

        [[nodiscard]] uint32_t CalculateNeedAllocateMem(const uint32_t MemSize, const uint32_t Alignment) const;

    private:
        uint32_t UsedMemSize = 0;

        uint32_t BlockMemSize = 0;

        uint32_t MaxMemSize = 0;

        uint32_t MaxOrder = 0;

        std::vector<std::set<uint32_t>> FreeList;
    };
}
