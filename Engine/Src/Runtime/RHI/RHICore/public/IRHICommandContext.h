#pragma once

#include "CRHIResource.h"
#include "RHICoreModuleDef.h"
#include "Misc/UtilityTypes.h"


namespace SkyGameEngine3d
{
    class CRHITexture;
    class RHIGraphicsPipelineState;
    class RHIViewport;

    struct CRHIDrawIndexInstancedArgs
    {
        CRHIDrawIndexInstancedArgs(RHIBufferPtr IndexBuffer,
                                   const uint32_t IndexCountPerInstance,
                                   const uint32_t BaseVertexLocation,
                                   const uint32_t BaseIndexLocation,
                                   const uint32_t BaseInstanceLocation,
                                   const uint32_t DrawInstanceCount = 1)
            : IndexBuffer{ std::move(IndexBuffer) }
            , BaseVertexLocation{ BaseVertexLocation }
            , BaseIndexLocation{ BaseIndexLocation }
            , IndexCountPerInstance{ IndexCountPerInstance }
            , BaseInstanceLocation{ BaseInstanceLocation }
            , DrawInstanceCount{ DrawInstanceCount }
        {
        }

        CRHIDrawIndexInstancedArgs(RHIBufferPtr IndexBuffer,
                                   const uint32_t IndexCountPerInstance,
                                   const uint32_t BaseVertexLocation = 0,
                                   const uint32_t BaseIndexLocation = 0)
            : IndexBuffer{ std::move(IndexBuffer) }
            , BaseVertexLocation{ BaseVertexLocation }
            , BaseIndexLocation{ BaseIndexLocation }
            , IndexCountPerInstance{ IndexCountPerInstance }
        {
        }
        
        RHIBufferPtr IndexBuffer;
        uint32_t BaseVertexLocation = 0;
        uint32_t BaseIndexLocation = 0;
        uint32_t IndexCountPerInstance = 0;
        uint32_t BaseInstanceLocation = 0;
        uint32_t DrawInstanceCount = 1;
    };

    class RHICORE_API IRHICommandContext : public CNonCopyable
    {
    public:
        virtual ~IRHICommandContext() = default;

        virtual void BeginDrawScene() = 0;
        virtual void EndDrawScene() = 0;

        virtual void BeginDrawViewport(std::shared_ptr<RHIViewport> InViewport) = 0;
        virtual void EndDrawViewport() = 0;

        virtual void BeginRenderPass(std::shared_ptr<CRHITexture> InRenderTarget) = 0;
        virtual void EndRenderPass() = 0;

        virtual void SetViewport(float MinX, float MinY, float MinDepth, float MaxX, float MaxY, float MaxDepth) = 0;

        virtual void SetRenderTarget(std::shared_ptr<CRHITexture> RenderTarget) = 0;

        virtual void SetGraphicsPipelineState(const RHIGraphicsPipelineState& PipelineState) =0;

        virtual void SetVertexBuffer(const RHIBufferPtr& RHIBuffer, uint32_t Offset = 0, uint32_t InputSlotIndex = 0) = 0;

        virtual void DrawIndexInstanced(const CRHIDrawIndexInstancedArgs& Args) = 0;

        virtual void CreateUIRootSignature() = 0;
    };
}
