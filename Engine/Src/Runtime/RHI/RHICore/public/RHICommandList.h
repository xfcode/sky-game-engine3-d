﻿#pragma once

#include <memory>

#include "IRHICommandContext.h"
#include "RHICoreModuleDef.h"

namespace SkyGameEngine3d
{
    class IRHICommandContext;
    class RHIGraphicsPipelineState;
    class CRHITexture;
}

namespace SkyGameEngine3d
{
    class RHICORE_API RHICommandList : public CNonCopyable
    {
    public:
        virtual void BeginDrawViewport(const std::shared_ptr<class RHIViewport>& InRHIViewport)
        {
        }

        virtual void EndDrawViewport(const std::shared_ptr<class RHIViewport>& InRHIViewport)
        {
        }

        virtual void SetRenderTarget(const std::shared_ptr<CRHITexture>& InRenderTarget)
        {
        }

    public:
        virtual void CreateUIRootSignature()
        {
            Context->CreateUIRootSignature();
        }

        virtual void SetGraphicsPipelineState(const RHIGraphicsPipelineState& PipelineState)
        {
            Context->SetGraphicsPipelineState(PipelineState);
        }

    protected:
        std::shared_ptr<IRHICommandContext> Context;
    };
}
