﻿#include "CRHIResource.h"

#include "RHI.h"

namespace SkyGameEngine3d
{
    void CResourceReleaseManager::Flush()
    {
        for (const auto Resource : PendingReleaseQueue)
        {
            DoingReleaseObject = Resource;
            delete Resource;
        }
        PendingReleaseQueue.clear();
    }

    CRHIResource::~CRHIResource()
    {
        // Res 对象只能通过对象释放管理器释放
        assert(CResourceReleaseManager::GetInstance()->DoingReleaseObject == this);
    }

    void CRHIResource::RequestReleaseResource(CRHIResource* Resource)
    {
        CResourceReleaseManager::GetInstance()->PendingReleaseQueue.push_back(Resource);
    }

    CRHIBuffer::~CRHIBuffer()
    {
    }

    CRHIBuffer::CRHIBuffer(SkyGameEngine3d::ERHIBufferFlag InBufferFlag, uint32_t InBufferSize, uint32_t InStride)
        : Stride(InStride)
        , BufferSize(InBufferSize)
        , BufferFlag(InBufferFlag)
    {
    }

    void* CRHIBuffer::Lock(EResourceLockMode LockMode)
    {
        return nullptr;
    }

    void CRHIBuffer::Unlock()
    {
    }

    uint32_t CRHIBuffer::GetSize() const
    {
        return BufferSize;
    }

    uint32_t CRHIBuffer::GetStride() const
    {
        return Stride;
    }

    CRHITexture::CRHITexture(const CRHITextureDesc& InDesc)
        : Desc{ InDesc }
    {
    }

    void* CRHITexture::Lock(uint32_t MipIndex, uint32_t ArrayIndex, uint32_t& RowInBytes, EResourceLockMode LockMode)
    {
        return nullptr;
    }

    void CRHITexture::UnLock(uint32_t MipIndex, uint32_t ArrayIndex)
    {
    }
};
