#include "public/RHI.h"
#include "CRHIResource.h"

namespace SkyGameEngine3d
{
    void CRHIInstance::CleanupPreFrame()
    {
        CResourceReleaseManager::GetInstance()->Flush();

        this->OnCleanupPreFrame();
    }
}
