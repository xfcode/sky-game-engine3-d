﻿#include "RHITypes.h"

#include <array>

namespace SkyGameEngine3d
{
    static std::array GImpPixeFormatInfoArray
    {
        CPixeFormatInfo{ L"None", EPixelFormat::None, 0, 0, 0, 0, 0 },
        CPixeFormatInfo{ L"R32B32G32A32F", EPixelFormat::R32B32G32A32F, 1, 1, 1, 16, 4 },
        CPixeFormatInfo{ L"A2B10G10R10F", EPixelFormat::A2B10G10R10F, 1, 1, 1, 4, 4 },
        CPixeFormatInfo{ L"BC1", EPixelFormat::BC1, 4, 4, 1, 8, 4 },
        CPixeFormatInfo{ L"BC2", EPixelFormat::BC2, 4, 4, 1, 16, 4 },
        CPixeFormatInfo{ L"BC3", EPixelFormat::BC3, 4, 4, 1, 16, 4 },
        CPixeFormatInfo{ L"BC4", EPixelFormat::BC4, 4, 4, 1, 8, 4 },
        CPixeFormatInfo{ L"BC5", EPixelFormat::BC5, 4, 4, 1, 16, 4 },
        CPixeFormatInfo{ L"BC6", EPixelFormat::BC6, 4, 4, 1, 16, 4 },
        CPixeFormatInfo{ L"BC7", EPixelFormat::BC7, 4, 4, 1, 16, 4 },
    };

    static bool GSetupImpPixeFormatInfoArray = false;

    void SetupPixeFormatInfo(const std::function<void(CPixeFormatInfo& OutInfo)>& SetupCall)
    {
        for (auto& InfoRef : GImpPixeFormatInfoArray)
        {
            SetupCall(InfoRef);
        }

        GSetupImpPixeFormatInfoArray = true;
    }

    const CPixeFormatInfo& QueryPixeFormatInfo(EPixelFormat InFormat)
    {
        assert(GSetupImpPixeFormatInfoArray == true);
        return GImpPixeFormatInfoArray[static_cast<uint32_t>(InFormat)];
    }
}
