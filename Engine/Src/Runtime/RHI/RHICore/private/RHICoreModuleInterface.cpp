#include "public/RHICoreModuleInterface.h"
#include "public/RHI.h"
#include "ModuleManager/ModuleManager.h"

namespace SkyGameEngine3d
{
    static CRHIInstance* RHIInstance = nullptr;

    class CRHIInstance* GetRHIInstance()
    {
        return RHIInstance;
    }

    void ShotdownRHI()
    {
    }

    void InitHRI()
    {
        auto ModuleInterface = ModuleManager::Get().GetModule<IRHIModule>(L"D3D12RHI");
        if (!ModuleInterface)
        {
            return;
        }

        RHIInstance = ModuleInterface->CreateRHI();

        RHIInstance->Init();
    }
}
