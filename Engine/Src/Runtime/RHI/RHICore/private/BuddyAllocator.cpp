﻿#include "BuddyAllocator.h"

namespace SkyGameEngine3d
{
    CBuddyAllocator::CBuddyAllocator(const uint32_t InBlockMemSize, const uint32_t InMaxMemSize)
        : BlockMemSize(InBlockMemSize)
        , MaxMemSize(InMaxMemSize)
    {
        //   分配器的要求：
        // MaxMemSize/BlockSize 一定是2的N次方
        assert(MaxMemSize / BlockMemSize * BlockMemSize == MaxMemSize);
        assert(MathLib::IsPowerOfTow(MaxMemSize / BlockMemSize));

        MaxOrder = BlockCountToOrder(AllocateSizeToBlockCount(InMaxMemSize));
        FreeList.resize(MaxOrder);
        FreeList.at(MaxOrder - 1).emplace(0);
    }

    bool CBuddyAllocator::CanAllocate(const uint32_t MemSize, const uint32_t Alignment) const
    {
        if (UsedMemSize + MemSize >= MaxMemSize)
        {
            return false;
        }

        const auto FinalAllocateMeme = CalculateNeedAllocateMem(MemSize, Alignment);

        // 计算需要在那个阶上分配内存
        const uint32_t NeedOrder = BlockCountToOrder(AllocateSizeToBlockCount(FinalAllocateMeme));

        // 只要当前阶以及以上的阶还有内存，那么就一定能分配
        for (uint32_t I = static_cast<uint32_t>(FreeList.size()) - 1; I >= NeedOrder; --I)
        {
            if (!FreeList.at(I).empty())
            {
                return true;
            }
        }

        return false;
    }

    std::shared_ptr<CBuddyAllocator::CAllocateData> CBuddyAllocator::TryAllocate(const uint32_t MemSize,
        const uint32_t Alignment)
    {
        if (!this->CanAllocate(MemSize, Alignment))
        {
            return nullptr;
        }

        auto AllocateData = std::make_shared<CAllocateData>();

        const auto FinalAllocateMem = CalculateNeedAllocateMem(MemSize, Alignment);

        const auto BlockCount = AllocateSizeToBlockCount(FinalAllocateMem);
        const auto Order = BlockCountToOrder(BlockCount);

        const auto Offset = AllocateBlock(Order);

        uint32_t MemOffset = Offset * BlockMemSize;

        AllocateData->Order = Order;
        AllocateData->BlockOffset = Offset;

        const auto AllocateMemSize = OrderToBlockCount(Order) * BlockMemSize;

        UsedMemSize += AllocateMemSize;

        // 考虑对齐问题，内存必须偏移在一个对齐的位置
        if (Alignment != 0 && MemOffset % Alignment != 0)
        {
            const uint32_t AlignmentMemOffset =
                static_cast<uint32_t>((static_cast<uint64_t>(MemOffset) + (Alignment - 1)) / Alignment) *
                Alignment;
            const uint32_t PendingSize = MemOffset - AlignmentMemOffset;

            assert(MemSize + PendingSize <= AllocateMemSize);

            MemOffset = AlignmentMemOffset;
        }

        AllocateData->AlignmentMemOffset = MemOffset;
        return AllocateData;
    }

    void CBuddyAllocator::Free(const std::shared_ptr<CAllocateData>& AllocateData)
    {
        if (!AllocateData)
        {
            return;
        }

        this->Free(AllocateData->BlockOffset, AllocateData->Order);

        const auto AllocateMemSize = OrderToBlockCount(AllocateData->Order) * BlockMemSize;
        this->UsedMemSize -= AllocateMemSize;
    }

    uint32_t CBuddyAllocator::AllocateSizeToBlockCount(const uint32_t InAllocateSize) const
    {
        // 计算可以拆分为多少个Block， 先上取值
        return (InAllocateSize + (BlockMemSize - 1)) / BlockMemSize;
    }

    uint32_t CBuddyAllocator::BlockCountToOrder(const uint32_t BlockCount)
    {
        unsigned long Result;
        _BitScanReverse(&Result, BlockCount + BlockCount - 1);
        return Result;
    }

    uint32_t CBuddyAllocator::OrderToBlockCount(const uint32_t Order)
    {
        return static_cast<uint32_t>(1) << Order;
    }

    uint32_t CBuddyAllocator::GetBuddyOffset(const uint32_t Offset, const uint32_t BlockCount)
    {
        /*
                这里用了一些很有趣的特性
    
                算法在才分内存的时候，其实是在 减阶，比如把5阶的内存拆分成2个4阶的伙伴内存
    
                比如 8         1000
                    16   0001 0010
                    24   0001 1010
    
                当前在3阶 Order = 3
                Offset 为 16
                BlockCount = 8
    
                16 ^ 8 = 24  另外一个的 偏移是 24
                24 ^ 8 = 16  也可以找到另外一个
    
                规律：
                    每次拆分出2块内存，这两个偏移 必然有一个 Offset 是 2 的N次方，一个 Offset 不是
                    然后用 异或运算 与 BlockCount 计算 只要知道一个 Offset，就能推出另外一个
    
                    本质就是 翻转 Offset 与 BlockCount 的最高位 对应的 位数 的位
            */
        return Offset ^ BlockCount;
    }

    void CBuddyAllocator::Free(const uint32_t BlockOffset, const uint32_t Order)
    {
        const auto BlockCount = OrderToBlockCount(Order);

        // 计算伙伴的偏移
        const auto BuddyOffset = GetBuddyOffset(BlockOffset, BlockCount);

        auto& FreeInfo = FreeList.at(Order);
        if (FreeInfo.contains(BuddyOffset))
        {
            // 可以合并, 选一个 他和伙伴的最小值，这些就可以把 2 块内存合并为 1 块大的内存
            Free(std::min(BuddyOffset, BlockOffset), Order + 1);
            FreeInfo.erase(BuddyOffset);
        }
        else
        {
            // 不能合并，先挂在链表上
            FreeInfo.emplace(BlockOffset);
        }
    }

    uint32_t CBuddyAllocator::AllocateBlock(const uint32_t Order)
    {
        if (Order > MaxOrder)
        {
            // 绝对不能出这种情况，分配的内存过大 应该在前面的步骤就 Check 出来了
            assert(false);
            return 0;
        }

        auto& OrderOffsetInfo = FreeList.at(Order);
        if (!OrderOffsetInfo.empty())
        {
            const auto OffsetIter = OrderOffsetInfo.begin();
            const auto Offset = *OffsetIter;
            OrderOffsetInfo.erase(OffsetIter);
            return Offset;
        }

        // 向上查找
        const auto Offset = AllocateBlock(Order + 1);

        // 拆分内存
        auto FreeOffset = Offset + OrderToBlockCount(Order);
        OrderOffsetInfo.emplace(FreeOffset);

        // 使用另外一半内存
        return Offset;
    }

    uint32_t CBuddyAllocator::CalculateNeedAllocateMem(const uint32_t MemSize, const uint32_t Alignment) const
    {
        uint32_t FinalAllocateMeme = MemSize;
        // 检查块的内存大小是不是和需要分配的内存对齐的，如果不对齐，还需要分配额外的内存使其对齐
        if (Alignment != 0 && BlockMemSize % Alignment != 0)
        {
            // 不对齐的情况，需要多分配 对齐大小的内存
            FinalAllocateMeme = MemSize + Alignment;
        }
        return FinalAllocateMeme;
    }
}
