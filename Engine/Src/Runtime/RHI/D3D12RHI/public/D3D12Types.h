﻿#pragma once

#include <wrl/client.h>
#include <dxgi.h>
#include <d3d12.h>
#include <d3dcompiler.h>
#include "directx/d3dx12.h"
#include "MacroHelper.h"
#include "RHITypes.h"

namespace SkyGameEngine3d
{
    // 定义 DX12 相关的各种 ComPtr 类型
    #define DEFINE_D3D_COM_PTR(T) using HELPER_JOIN(T, ComPtr) = Microsoft::WRL::ComPtr<T>;

    DEFINE_D3D_COM_PTR(IDXGIFactory1);
    DEFINE_D3D_COM_PTR(IDXGIAdapter1);
    DEFINE_D3D_COM_PTR(ID3D12Device);
    DEFINE_D3D_COM_PTR(IDXGISwapChain);
    DEFINE_D3D_COM_PTR(ID3D12CommandQueue);
    // DX12 三种不同的命令队列对应的CommandList 其实都是 ID3D12GraphicsCommandList
    DEFINE_D3D_COM_PTR(ID3D12GraphicsCommandList);
    DEFINE_D3D_COM_PTR(ID3D12Resource);
    DEFINE_D3D_COM_PTR(ID3DBlob);
    DEFINE_D3D_COM_PTR(ID3D12RootSignature);
    DEFINE_D3D_COM_PTR(ID3D12CommandAllocator);
    DEFINE_D3D_COM_PTR(ID3D12CommandList);
    DEFINE_D3D_COM_PTR(ID3D12Fence);
    DEFINE_D3D_COM_PTR(ID3D12DescriptorHeap);
    DEFINE_D3D_COM_PTR(ID3D12PipelineState);
    DEFINE_D3D_COM_PTR(ID3D12Heap);
    DEFINE_D3D_COM_PTR(ID3D12Debug);
    #undef DEFINE_D3D_COM_PTR


    inline DXGI_FORMAT ConvertFormPixelFormat(const EPixelFormat Format)
    {
        //  https://learn.microsoft.com/en-us/windows/win32/api/dxgiformat/ne-dxgiformat-dxgi_format
        // https://learn.microsoft.com/en-us/windows/win32/direct3d10/d3d10-graphics-programming-guide-resources-data-conversion
        switch (Format)
        {
        case EPixelFormat::R32B32G32A32F: return DXGI_FORMAT_R32G32B32A32_FLOAT;
        case EPixelFormat::A2B10G10R10F: return DXGI_FORMAT_R10G10B10A2_UNORM;
        case EPixelFormat::BC7: return DXGI_FORMAT_BC7_UNORM;
        case EPixelFormat::None: return DXGI_FORMAT_UNKNOWN;
        }
        return DXGI_FORMAT_UNKNOWN;
    }

    inline DXGI_FORMAT ConvertFormVertexFormat(const ERHIVertexDataFormat Format)
    {
        switch (Format)
        {
        case ERHIVertexDataFormat::Float1: return DXGI_FORMAT_R32_FLOAT;
        case ERHIVertexDataFormat::Float2: return DXGI_FORMAT_R32G32_FLOAT;
        case ERHIVertexDataFormat::Float3: return DXGI_FORMAT_R32G32B32_FLOAT;
        case ERHIVertexDataFormat::Float4: return DXGI_FORMAT_R32G32B32A32_FLOAT;
        // case ERHIVertexDataFormat::None: break;
        // case ERHIVertexDataFormat::R8Uint: break;
        // case ERHIVertexDataFormat::R16G16UInt: break;
        // case ERHIVertexDataFormat::R32G32B32UInt: break;
        // case ERHIVertexDataFormat::R8G8B8A8SInt: break;
        // case ERHIVertexDataFormat::R8G8B8A8UInt: break;
        default: return DXGI_FORMAT_UNKNOWN;
        }
    }

    inline D3D12_COMMAND_LIST_TYPE ConvertD3d12CommandListType(const ERHICommandQueueType Type)
    {
        switch (Type)
        {
        case ERHICommandQueueType::Graphics:
            return D3D12_COMMAND_LIST_TYPE_DIRECT;
        case ERHICommandQueueType::Copy:
            return D3D12_COMMAND_LIST_TYPE_COPY;
        case ERHICommandQueueType::Compute:
            return D3D12_COMMAND_LIST_TYPE_COMPUTE;
        }
        return D3D12_COMMAND_LIST_TYPE_DIRECT;
    }

    enum class ED3D12PipelineType: uint8_t
    {
        Graphics,
        Compute,
    };

    constexpr bool IsCpuWriteable(D3D12_HEAP_TYPE HeapType)
    {
        if (HeapType == D3D12_HEAP_TYPE_UPLOAD)
        {
            return true;
        }
        return false;
    }
}
