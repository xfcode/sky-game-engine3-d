﻿#pragma once


#include "D3d12Resource.h"
#include "D3D12Types.h"
#include "D3D12ViewPort.h"

namespace SkyGameEngine3d
{
    class CD3D12Device;

    class CD3D12ResourceManager :
        public std::shared_ptr<CD3D12ResourceManager>,
        public CNonCopyable,
        public IRHIResourceFactory
    {
    public:
        void Init(CD3D12Device* InDevice);

        CD3D12ResourceHandle AllocateUploadHeapBuffer(
            uint32_t InBufferSize,
            uint32_t InAlignment = 4) const;

        CD3D12ResourceHandle AllocateDefaultHeapBuffer(
            uint32_t InBufferSize,
            uint32_t InAlignment = 4) const;

        /**
         * \brief 快速分配的 UploadBuffer
         * \param InBufferSize 
         * \param InAlignment 
         * \return 
         */
        CD3D12ResourceHandle FastAllocateUploadHeapBuffer(
            uint32_t InBufferSize,
            uint32_t InAlignment = 4) const;
        
    public:
        [[nodiscard]] virtual TRHIResourcePtr<RHIViewport> CreateRHIViewport(const RHIViewportCreateArg& Arg) override;
        [[nodiscard]] virtual TRHIResourcePtr<CRHITexture> CreateTexture(const RHITextureCreateArgs& Args) override;
        [[nodiscard]] virtual TRHIResourcePtr<CRHIShader> CreateShader(const RHIShaderCreateArgs& Args) override;
        [[nodiscard]] virtual TRHIResourcePtr<CRHIBuffer> CreateBuffer(const RHICreateBufferArgs& Args) override;

    private:
        using BuddyAllocatorList = CD3D12AllocatorList<CD3D12BuddyAllocator>;
        std::unique_ptr<BuddyAllocatorList> UploadAllocate;
        std::unique_ptr<BuddyAllocatorList> DefaultAllocate;
        std::unique_ptr<CD3D12FastAllocator> FastAllocator;
        CD3D12Device* OwnerDevice = nullptr;
    };
}
