﻿#pragma once

#include "CRHIResource.h"
#include "D3D12DeviceChild.h"
#include "D3d12Resource.h"

namespace SkyGameEngine3d
{
    class CD3D12Texture :
        public CRHITexture,
        public CD3D12DeviceChildObject
    {
    public:
        explicit CD3D12Texture(CD3D12Device* InDevice, const CRHITextureDesc& InDesc)
            : CRHITexture{ InDesc }
            , CD3D12DeviceChildObject{ InDevice }
        {
        }

        bool Init();

        virtual ~CD3D12Texture() override;

        [[nodiscard]] virtual void* Lock(uint32_t MipIndex, uint32_t ArrayIndex, uint32_t& RowInBytes,
                                         EResourceLockMode LockMode) override;

        virtual void UnLock(uint32_t MipIndex, uint32_t ArrayIndex) override;

    protected:
        CD3D12ResourceHandle ResourceHandle;

        struct CLockData
        {
            CD3D12ResourceHandle UploadResourceHandle;
        };

        using SubResourceIndex = uint32_t;
        std::unordered_map<SubResourceIndex, CLockData> LockDataMap;
    };


    inline uint32_t GetSubResourceIndex(const CD3D12Texture& Texture, const uint32_t MipIndex,
                                        const uint32_t ArrayIndex = 0)
    {
        return MipIndex + ArrayIndex * Texture.GetTextureDesc().MipsCount;
    }
}
