#pragma once

#include <wrl/client.h>
#include <d3d12.h>
#include <span>
#include "Math/Vector.h"
#include "Math/Color.h"
#include "D3D12Helper.h"

struct VertexTest1
{
    SkyGameEngine3d::Vector3D Pos;
    SkyGameEngine3d::ColorLinear Color;
};


struct VertexTest2
{
    SkyGameEngine3d::Vector3D Pos;
    SkyGameEngine3d::Vector3D Normal;
    SkyGameEngine3d::Vector2D Tex0;
    SkyGameEngine3d::Vector2D Tex1;
};

inline auto CreateVertexTest(ID3D12Device* Device,
                             ID3D12GraphicsCommandList* CmdList,
                             Microsoft::WRL::ComPtr<ID3D12Resource>& UploadBuffer, uint32_t& OutVertexSize)
{
    using namespace SkyGameEngine3d;
    using namespace Microsoft::WRL;

    static VertexTest1 VertexData[] = {
        { Vector3D{ -1.f, -1.f, -1.f }, ColorLinear{ 1, 0.f, 0, 1.f } },
        { Vector3D{ -1.f, 1.f, -1.f }, ColorLinear{ 1, 0.f, 0.7, 1.f } },
        { Vector3D{ 1.f, 1.f, -1.f }, ColorLinear{ 1, 0.f, 0.2, 1.f } },
        { Vector3D{ 1.f, -1.f, -1.f }, ColorLinear{ 1, 1.f, 0.1, 1.f } },
        { Vector3D{ -1.f, -1.f, 1.f }, ColorLinear{ 0.5, 1.f, 0, 1.f } },
        { Vector3D{ -1.f, 1.f, 1.f }, ColorLinear{ 1, 1.f, 0, 1.f } },
        { Vector3D{ 1.f, 1.f, 1.f }, ColorLinear{ 0, 1.f, 1, 1.f } },
        { Vector3D{ 1.f, -1.f, 1.f }, ColorLinear{ 1, 1.f, 0.5, 1.f } },
    };

    OutVertexSize = std::span(VertexData).size_bytes();
    return D3D12Helper::CreateDefaultBuffer(Device, CmdList, std::span<VertexTest1>{ VertexData }, UploadBuffer);
}

inline std::span<VertexTest1> GetVertexTest()
{
    using namespace SkyGameEngine3d;
    using namespace Microsoft::WRL;

    static VertexTest1 VertexData[] = {
        { Vector3D{ -1.f, -1.f, -1.f }, ColorLinear{ 1, 0.f, 0, 1.f } },
        { Vector3D{ -1.f, 1.f, -1.f }, ColorLinear{ 1, 0.f, 0.7, 1.f } },
        { Vector3D{ 1.f, 1.f, -1.f }, ColorLinear{ 1, 0.f, 0.2, 1.f } },
        { Vector3D{ 1.f, -1.f, -1.f }, ColorLinear{ 1, 1.f, 0.1, 1.f } },
        { Vector3D{ -1.f, -1.f, 1.f }, ColorLinear{ 0.5, 1.f, 0, 1.f } },
        { Vector3D{ -1.f, 1.f, 1.f }, ColorLinear{ 1, 1.f, 0, 1.f } },
        { Vector3D{ 1.f, 1.f, 1.f }, ColorLinear{ 0, 1.f, 1, 1.f } },
        { Vector3D{ 1.f, -1.f, 1.f }, ColorLinear{ 1, 1.f, 0.5, 1.f } },
    };

    return VertexData;
}

inline std::span<uint16_t> GetIndexTest()
{
    using namespace SkyGameEngine3d;
    using namespace Microsoft::WRL;

    static uint16_t IndexBuffer[] = {
        0, 1, 2,
        0, 2, 3,

        4, 6, 5,
        4, 7, 6,

        4, 5, 1,
        4, 1, 0,

        3, 2, 6,
        3, 6, 7,

        1, 5, 6,
        1, 6, 2,

        4, 0, 3,
        4, 3, 7,
    };

    return IndexBuffer;
}

template <typename VertexType>
auto BindVertexTest(ID3D12GraphicsCommandList* CmdList, int32_t VertexNum,
                    Microsoft::WRL::ComPtr<ID3D12Resource> VertexBuffer)
{
    D3D12_VERTEX_BUFFER_VIEW VertexBufferView = {
        .BufferLocation = VertexBuffer->GetGPUVirtualAddress(),
        .SizeInBytes = VertexNum * sizeof(VertexType),
        .StrideInBytes = sizeof(VertexType),
    };

    CmdList->IASetVertexBuffers(0, 1, &VertexBufferView);
}

inline auto CreateIndexBufferTest(ID3D12Device* Device,
                                  ID3D12GraphicsCommandList* CmdList,
                                  Microsoft::WRL::ComPtr<ID3D12Resource>& UploadBuffer, uint32_t& OutIndexSize,
                                  uint32_t& OutIndexCount)
{
    using namespace SkyGameEngine3d;
    using namespace Microsoft::WRL;

    static uint16_t IndexBuffer[] = {
        0, 1, 2,
        0, 2, 3,

        4, 6, 5,
        4, 7, 6,

        4, 5, 1,
        4, 1, 0,

        3, 2, 6,
        3, 6, 7,

        1, 5, 6,
        1, 6, 2,

        4, 0, 3,
        4, 3, 7,
    };

    auto ArraySpan = std::span{ IndexBuffer };
    OutIndexSize = ArraySpan.size_bytes();
    OutIndexCount = ArraySpan.size();
    return D3D12Helper::CreateDefaultBuffer(Device, CmdList, std::span<uint16_t>(IndexBuffer), UploadBuffer);
}


inline auto BindIndexBuffer(ID3D12GraphicsCommandList* CmdList, int32_t IndexNum,
                            Microsoft::WRL::ComPtr<ID3D12Resource> VertexBuffer)
{
    D3D12_INDEX_BUFFER_VIEW IndexBufferView = {
        .BufferLocation = VertexBuffer->GetGPUVirtualAddress(),
        .SizeInBytes = static_cast<UINT>(IndexNum * sizeof(uint16_t)),
        .Format = DXGI_FORMAT_R16_UINT,
    };

    CmdList->IASetIndexBuffer(&IndexBufferView);
}
