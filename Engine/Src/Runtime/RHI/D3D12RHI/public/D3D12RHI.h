#pragma once

#include "D3D12Device.h"
#include "public/RHI.h"
#include "public/RHIViewport.h"
#include "D3D12Types.h"
#include "D3D12Helper.h"
#include "D3D12Test.h"
#include "IRHIResourceFactory.h"

namespace SkyGameEngine3d
{
    class CD3D12Device;
}

namespace SkyGameEngine3d
{
    class CD3D12Buffer;
}

namespace SkyGameEngine3d
{
    namespace Windows = Microsoft::WRL;

    struct ID3D12Interface
    {
        [[nodiscard]] virtual IDXGIFactory1ComPtr GetDXHIFactory() const = 0;

        [[nodiscard]] virtual IDXGIAdapter1ComPtr GetDXHIAdapter() const = 0;

        [[nodiscard]] virtual ID3D12DeviceComPtr GetDevice() const = 0;

        [[nodiscard]] virtual ID3D12CommandQueueComPtr GetCommandQueue() const = 0;

        [[nodiscard]] virtual ID3D12GraphicsCommandListComPtr GetGraphicsCommandList() const = 0;
    };


    class D3D12RHIImp
        : public CRHIInstance,
          public ID3D12Interface,
          public IRHIResourceFactory
    {
    public:
        [[nodiscard]] virtual IDXGIFactory1ComPtr GetDXHIFactory() const override;

        [[nodiscard]] virtual IDXGIAdapter1ComPtr GetDXHIAdapter() const override;

        [[nodiscard]] virtual ID3D12DeviceComPtr GetDevice() const override;

        [[nodiscard]] virtual ID3D12CommandQueueComPtr GetCommandQueue() const override;

        [[nodiscard]] virtual ID3D12GraphicsCommandListComPtr GetGraphicsCommandList() const override;

        virtual IRHIResourceFactory* GetResourceFactory() const override;

    protected:
        virtual void Init() override;

        virtual void Shutdown() override;

        virtual TRHIResourcePtr<RHIViewport> CreateRHIViewport(const RHIViewportCreateArg& Arg) override;

        // virtual void BeginDraw(const RHIBeginDrawArgs& Args);

        // virtual void EndDraw() override
        // {
        // }

        [[nodiscard]] virtual TRHIResourcePtr<CRHIShader> CreateShader(const RHIShaderCreateArgs& Args) override;

        virtual TRHIResourcePtr<CRHITexture> CreateTexture(const RHITextureCreateArgs& Args) override;

        virtual TRHIResourcePtr<CRHIBuffer> CreateBuffer(const RHICreateBufferArgs& Args) override;

        // virtual void TestSetValue(float InTheta, float InPhi, float InRadius) override
        // {
        //     Theta = InTheta;
        //     Phi = InPhi;
        //     Radius = InRadius;
        // }

        float Theta = 1.5f * PI;
        float Phi = PI / 4;
        float Radius = 5;

        void FlushCommandQueue();

    private:
        std::shared_ptr<RHIViewport> ViewPort;

        IDXGIFactory1ComPtr DXGIFactory;
        IDXGIAdapter1ComPtr DXGIAdapter;
        ID3D12DeviceComPtr Device;

        // 可多线程访问
        ID3D12CommandQueueComPtr CommandQueue;

        // 仅允许单线程
        ID3D12CommandAllocatorComPtr CommandAllocator;
        ID3D12GraphicsCommandListComPtr CommandList;

        ID3D12FenceComPtr Fence;

        int32_t CurrentFence = 0;

        using ObjectConstantsUploadBuffer = TUploadBuffer<ObjectConstants, true>;
        std::unique_ptr<ObjectConstantsUploadBuffer> ObjectCb;
        std::shared_ptr<CD3D12Buffer> ObjectCbBuffer;
        static constexpr bool bUsedCbBuffer = true;

        ID3D12DescriptorHeapComPtr CbDescriptorHeap;

        ID3D12RootSignatureComPtr RootSignature;

        ID3DBlobComPtr VsByteCode;
        ID3DBlobComPtr PsByteCode;

        std::vector<D3D12_INPUT_ELEMENT_DESC> InputLayout;

        using GeometryMeshTest1 = MeshGeometry<VertexTest1>;
        std::unique_ptr<GeometryMeshTest1> MeshGeometry;

        std::shared_ptr<class CD3D12Buffer> VerBuffer;
        std::shared_ptr<class CD3D12Buffer> IndexBuffer;

        constexpr static bool bMeshGeometry = false;

        ID3D12PipelineStateComPtr PipelineStateComPtr;

        Matrix4X4 WorldMat;
        Matrix4X4 ProjMat;
        Matrix4X4 ViewMat;
    };


    inline ID3D12Interface* CastToD3D12Interface(CRHIInstance* RHI)
    {
        return static_cast<D3D12RHIImp*>(RHI);
    }


    class CD3D12RHIInstance : public CRHIInstance
    {
    public:
        virtual void Init() override;

        virtual void Shutdown() override;

        [[nodiscard]] virtual IRHIResourceFactory* GetResourceFactory() const override;

        [[nodiscard]] virtual class IRHICommandContext* GetCommandContext() override;

    public:
        void AddPendingDeleteResource(std::shared_ptr<class CD3D12Resource>&& Resource);

    private:
        virtual void OnCleanupPreFrame() override;

    private:
        std::shared_ptr<CD3D12Device> RootDevice;

        std::vector<std::shared_ptr<class CD3D12Resource>> PendingDeleteResourcesArray;

    private:
        inline static CD3D12RHIInstance* Instance = nullptr;
        friend CD3D12RHIInstance* GetD3D12RHIInstance();
    };

    CD3D12RHIInstance* GetD3D12RHIInstance();
}
