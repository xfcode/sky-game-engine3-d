﻿#pragma once

#include <memory>

#include "D3D12DeviceChild.h"
#include "D3D12Types.h"
#include "IRHICommandContext.h"

namespace SkyGameEngine3d
{
    class CRHITexture;

    class CD3D12RHIContext :
        public IRHICommandContext,
        public CD3D12DeviceChildObject
    {
    public:
        explicit CD3D12RHIContext(CD3D12Device* InDevice);

    public:
        virtual void BeginDrawScene() override;

        virtual void EndDrawScene() override;

        virtual void BeginDrawViewport(std::shared_ptr<RHIViewport> InViewport) override;

        virtual void EndDrawViewport() override;

        virtual void BeginRenderPass(std::shared_ptr<CRHITexture> InRenderTarget) override;
        virtual void EndRenderPass() override;

        virtual void SetViewport(float MinX, float MinY, float MinDepth, float MaxX, float MaxY,
                                 float MaxDepth) override;


        virtual void SetRenderTarget(std::shared_ptr<CRHITexture> RenderTarget) override;

        virtual void CreateUIRootSignature() override;

        virtual void SetGraphicsPipelineState(const RHIGraphicsPipelineState& PipelineState) override;

        virtual void DrawIndexInstanced(const CRHIDrawIndexInstancedArgs& Args) override;

        virtual void SetVertexBuffer(const RHIBufferPtr& RHIBuffer, uint32_t Offset, uint32_t InputSlotIndex) override;
        
    public:
        
        ID3D12GraphicsCommandListComPtr GetGraphicsCommandList() const
        {
            // TODO 实现获取CommandList
            return nullptr;
        }
    
    private:
        class CD3D12Device* Device = nullptr;

        ID3D12DeviceComPtr DeviceComPtr;
        ID3D12RootSignatureComPtr UIRootSignature;
        ID3D12PipelineStateComPtr UIPso;
    };
}
