﻿#pragma once

#include "D3D12DeviceChild.h"
#include "D3D12Types.h"

namespace SkyGameEngine3d
{
    class CD3D12Device;

    class CD3D12CommandList
    {
    public:
        CD3D12CommandList(CD3D12Device* Device, ERHICommandQueueType QueueType);


        ID3D12GraphicsCommandListComPtr GetCommandList()
        {
            return CommandListComPtr;
        }

        ERHICommandQueueType GetQueueType() const
        {
            return QueueType;
        }

        void Open();

        void Close();

        bool IsOpen() const
        {
            return bIsOpen;
        }

    private:
        ERHICommandQueueType QueueType;

        ID3D12CommandAllocatorComPtr Allocator;

        ID3D12GraphicsCommandListComPtr CommandListComPtr;

        bool bIsOpen = true;
    };


    class CD3D12CommandQueue : public CD3D12DeviceChildObject
    {
    public:
        CD3D12CommandQueue(CD3D12Device* Device, ERHICommandQueueType Type);

        [[nodiscard]] ERHICommandQueueType GetQueueType() const;

        ID3D12CommandQueueComPtr GetQueueComPtr() const
        {
            return QueueComPtr;
        }

        ID3D12GraphicsCommandListComPtr GetCommandListComPtr()
        {
            if (!CurrentCommandList || !CurrentCommandList->IsOpen())
            {
                CurrentCommandList = this->AllocateCommandList();
            }
            return CurrentCommandList->GetCommandList();
        }

    private:
        [[nodiscard]] std::shared_ptr<CD3D12CommandList> AllocateCommandList();

        void FreeCommandList(std::shared_ptr<CD3D12CommandList>&& FreeCommandList);

    private:
        std::shared_ptr<CD3D12CommandList> CurrentCommandList;

        std::list<std::shared_ptr<CD3D12CommandList>> CommandListFreeList;

        ID3D12CommandAllocatorComPtr QueueAllocatorComPtr;

        ID3D12CommandQueueComPtr QueueComPtr;

        ERHICommandQueueType QueueType = ERHICommandQueueType::Graphics;
    };
}
