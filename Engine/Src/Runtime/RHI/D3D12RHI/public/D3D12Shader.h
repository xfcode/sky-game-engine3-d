#pragma once

#include "CRHIResource.h"
#include "D3D12Helper.h"
#include "D3D12RHIModuleDef.h"
#include "D3D12Types.h"
#include "RHITypes.h"
#include "Misc/Paths.h"

namespace SkyGameEngine3d
{
    class D3D12Shader
    {
    public:
        D3D12_SHADER_BYTECODE GetShaderBytecode() const
        {
            return
            {
                .pShaderBytecode = Code->GetBufferPointer(),
                .BytecodeLength = Code->GetBufferSize()
            };
        }

    protected:
        void CompileShader(const std::filesystem::path& ShaderPath, const std::string_view MainName)
        {
            Code = D3D12Helper::CompileShader((Paths::GetShaderDir() / ShaderPath).wstring(),
                                              nullptr,
                                              MainName,
                                              "vs_5_0"
            );
        }

    protected:
        ID3DBlobComPtr Code;
    };

    class D3D12VertexShader : public CRHIShader,
                              public D3D12Shader
    {
    public:
        D3D12VertexShader()
            : CRHIShader(EShaderType::Vertex)
        {
        }

        virtual void Init(const std::filesystem::path& ShaderPath, const std::string_view MainName) override
        {
            this->CompileShader(ShaderPath, MainName);
        }
    };


    class D3D12PixelShader : public CRHIShader,
                             public D3D12Shader
    {
    public:
        D3D12PixelShader()
            : CRHIShader(EShaderType::Vertex)
        {
        }

        virtual void Init(const std::filesystem::path& ShaderPath, const std::string_view MainName) override
        {
            this->CompileShader(ShaderPath, MainName);
        }
    };
}
