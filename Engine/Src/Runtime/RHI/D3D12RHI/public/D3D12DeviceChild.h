﻿#pragma once

namespace SkyGameEngine3d
{
    class CD3D12Device;

    class CD3D12DeviceChildObject
    {
    protected:
        explicit CD3D12DeviceChildObject(CD3D12Device* InOwnerDevice)
            : OwnerDevice{ InOwnerDevice }
        {
        }

    protected:
        CD3D12Device* OwnerDevice = nullptr;
    };
}
