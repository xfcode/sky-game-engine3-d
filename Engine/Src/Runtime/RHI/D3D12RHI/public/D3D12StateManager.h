﻿#pragma once
#include <memory>

#include "D3d12Resource.h"
#include "D3D12Types.h"
#include "Misc/UtilityTypes.h"

namespace SkyGameEngine3d
{
    class CD3D12StateManager : public CNonCopyable,
                               public CD3D12DeviceChildObject
    {
    public:
        explicit CD3D12StateManager(CD3D12Device* Device);

        ~CD3D12StateManager();

        void SetIndexBuffer(const CD3D12ResourceHandle& ResourceHandle, DXGI_FORMAT Format);

        void ApplyState(ED3D12PipelineType PipelineType)
        {
        }

    private:
        std::shared_ptr<struct CD3D12ComputeState> ComputeState = nullptr;
        std::shared_ptr<struct CD3D12GraphicsState> GraphicsState = nullptr;
        std::shared_ptr<struct CD3D12CommonState> CommonState = nullptr;
    };
}
