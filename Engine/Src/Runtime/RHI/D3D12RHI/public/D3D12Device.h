#pragma once

#include <array>

#include "D3D12CommandQueue.h"
#include "D3D12Types.h"

namespace SkyGameEngine3d
{
    class CD3D12StateManager;
    class CD3D12CommandQueue;
    class CD3D12RHIContext;
    class CD3D12ResourceManager;
}

namespace SkyGameEngine3d
{
    struct CD3D12FrameFence
    {
        ID3D12FenceComPtr FenceComPtr;
        uint64_t FenceCounter = 0;

        [[nodiscard]] uint64_t GetNextFenceValue() const
        {
            return FenceCounter + 1;
        }

        uint64_t GetCompleteValue() const
        {
            return FenceComPtr->GetCompletedValue();
        }
    };

    class CD3D12Device
    {
    public:
        [[nodiscard]] ID3D12DeviceComPtr GetDevice() const
        {
            return Device;
        }

        const CD3D12FrameFence& GetFrameFence() const
        {
            return FrameFence;
        }

        CD3D12FrameFence& GetFrameFence()
        {
            return FrameFence;
        }

        void Init();

        void Shutdown();

        [[nodiscard]] CD3D12ResourceManager* GetResourceManager() const;

        [[nodiscard]] CD3D12RHIContext* GetRHIContext() const
        {
            return RHIContext.get();
        }

        ID3D12GraphicsCommandListComPtr GetGraphicsCommandListComPtr()
        {
            return GetCommandQueue(ERHICommandQueueType::Graphics)->GetCommandListComPtr();
        }

        CD3D12CommandQueue* GetCommandQueue(ERHICommandQueueType Type);


        [[nodiscard]] IDXGIFactory1ComPtr GetDxgiFactory1() const
        {
            return DxgiFactory1;
        }

        [[nodiscard]] CD3D12StateManager* GetStateManager() const
        {
            return StateManager.get();
        }

    private:
        std::shared_ptr<CD3D12StateManager> StateManager;

        std::shared_ptr<CD3D12RHIContext> RHIContext;

        std::shared_ptr<CD3D12ResourceManager> ResourceManager;

        IDXGIFactory1ComPtr DxgiFactory1;

        IDXGIAdapter1ComPtr DxgiAdapter1;

        ID3D12DeviceComPtr Device;

        CD3D12FrameFence FrameFence;

        std::array<std::shared_ptr<CD3D12CommandQueue>, static_cast<uint8_t>(ERHICommandQueueType::Count)> CommandQueue;
    };
}
