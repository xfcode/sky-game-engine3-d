﻿#pragma once

#include "D3D12DeviceChild.h"
#include "D3D12Types.h"
#include "RHIViewport.h"

namespace SkyGameEngine3d
{
    struct CD3D12RHIViewportInitArg
    {
        HWND Hwnd = nullptr;
        int32_t WindowW = 0;
        int32_t WindowH = 0;
        DXGI_FORMAT BackBufferFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
    };

    class CD3D12Viewport
        : public RHIViewport,
          public CD3D12DeviceChildObject
    {
    public:
        explicit CD3D12Viewport(CD3D12Device* Device)
            : CD3D12DeviceChildObject{ Device }
        {
        }

        bool Init(const CD3D12RHIViewportInitArg& Arg);

        void Resize(const Vector2DInt& Size, DXGI_FORMAT Format);

        D3D12_CPU_DESCRIPTOR_HANDLE GetCurrentBackBufferView() const;

        D3D12_CPU_DESCRIPTOR_HANDLE GetDepthStencilView() const;

        ID3D12ResourceComPtr GetCurrentBackBufferResource() const;

        ID3D12ResourceComPtr GetDepthStencilBufferResource() const;

        Vector2DInt GetViewPortSize() const;

        IDXGISwapChainComPtr GetSwapChain() const;

        void Present();

    private:
        uint32_t RtvDescSize = 0;
        uint32_t DsvDescSize = 0;

        ID3D12DescriptorHeapComPtr RtvDescHeap;
        ID3D12DescriptorHeapComPtr DsvDescHeap;

        ID3D12ResourceComPtr DepthStencilView;

        static constexpr uint8_t BackBufferCount = 2;

        ID3D12ResourceComPtr BackBuffers[BackBufferCount];

        int32_t CurrentBackBufferIndex = 0;

        IDXGISwapChainComPtr DxgiSwapChain;

        HWND Hwnd = nullptr;
        Vector2DInt ViewportSize;

        DXGI_FORMAT BackBufferFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
    };
}
