﻿#pragma once

#include <any>

#include "BuddyAllocator.h"
#include "D3D12Device.h"
#include "D3D12Types.h"
#include "IRHIResourceFactory.h"
#include "Misc/Memory.h"
#include <algorithm>
#include "D3D12Helper.h"

namespace SkyGameEngine3d
{
    /**
     * \brief 封装D3d12的资源，保存一些常用的数据
     */
    class CD3D12Resource : public CNonCopyable
    {
    public:
        CD3D12Resource(
            const ID3D12ResourceComPtr& InResource, const D3D12_HEAP_TYPE InHeapType,
            const D3D12_RESOURCE_DESC& InResourceDesc)
            : Resource(InResource)
            , HeapType(InHeapType)
            , ResourceDesc(InResourceDesc)
        {
            if (Resource && ResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_BUFFER)
            {
                GpuVirtualAddress = Resource->GetGPUVirtualAddress();
            }
        }

        void* Map();

        void UnMap();

        const D3D12_GPU_VIRTUAL_ADDRESS& GetGpuAddress() const;

        ID3D12ResourceComPtr GetResource() const
        {
            return Resource;
        }

        const D3D12_RESOURCE_DESC& GetResourceDesc() const
        {
            return ResourceDesc;
        }

    private:
        void* CpuPtr = nullptr;

        int32_t MapCount = 0;

        ID3D12ResourceComPtr Resource;

        D3D12_HEAP_TYPE HeapType = D3D12_HEAP_TYPE_DEFAULT;

        D3D12_GPU_VIRTUAL_ADDRESS GpuVirtualAddress = 0;

        D3D12_RESOURCE_DESC ResourceDesc = { };
    };

    // 管理分配资源的句柄
    class CD3D12ResourceHandle : public CNonCopyable
    {
    public:
        enum class EResourceManagementMode: uint8_t
        {
            None,
            StandAlone,
            ResourceAllocator,
        };

        CD3D12ResourceHandle() = default;

        CD3D12ResourceHandle(CD3D12ResourceHandle&& Other) noexcept;

        CD3D12ResourceHandle& operator=(CD3D12ResourceHandle&& Other) noexcept;

        ~CD3D12ResourceHandle();

        void Release();

        #pragma region EResourceManagementMode::ResourceAllocator

        void SetAllocatorData(
            class ID3D12ResourceAllocator* Owner,
            const std::shared_ptr<struct CD3D12ResourceAllocatorPayloadDataBase>& PayloadData);

        std::shared_ptr<struct CD3D12ResourceAllocatorPayloadDataBase> GetAllocatorPayloadData() const;

        void SetAllocatorResource(
            const std::shared_ptr<CD3D12Resource>& InResource,
            const uint32_t SizeInBytes,
            const uint32_t InResourceOffset = 0);

        #pragma endregion

        #pragma region EResourceManagementMode::StandAlone

        void SetStandAlone(std::shared_ptr<CD3D12Resource>&& InResource, const uint32_t SizeInBytes);

        #pragma endregion

        const D3D12_GPU_VIRTUAL_ADDRESS& GetGpuAddress() const
        {
            return GpuAddr;
        }

        void* Map() const
        {
            if (Resource && !MapAddr)
            {
                MapAddr = static_cast<uint8_t*>(Resource->Map()) + ResourceOffset;
            }

            return MapAddr;
        }

        void Unmap() const
        {
            if (Resource)
            {
                Resource->UnMap();
            }
        }

        bool IsValid() const
        {
            return !!Resource;
        }

        bool IsMap() const
        {
            return !!MapAddr;
        }

        ID3D12ResourceComPtr GetResource() const
        {
            return Resource->GetResource();
        }

        uint32_t GetResourceOffset() const
        {
            return ResourceOffset;
        }

        uint32_t GetResourceSizeInBytes() const
        {
            return ResourceSizeInBytes;
        }

        D3D12_RESOURCE_DESC GetResourceDesc() const
        {
            if (Resource)
            {
                return Resource->GetResourceDesc();
            }
            return { };
        }

    private:
        EResourceManagementMode ResourceManagementMode = EResourceManagementMode::None;

        D3D12_GPU_VIRTUAL_ADDRESS GpuAddr = 0;

        std::shared_ptr<CD3D12Resource> Resource;

        std::shared_ptr<struct CD3D12ResourceAllocatorPayloadDataBase> AllocatorPayloadData;

        uint32_t ResourceOffset = 0;

        class ID3D12ResourceAllocator* OwnerAllocator = nullptr;

        mutable uint8_t* MapAddr = nullptr;

        uint32_t ResourceSizeInBytes = 0;
    };


    enum class EResourceAllocationStrategy
    {
        /**
         * \brief 使用占位资源，在一块大的 heap 中分配，分配的占位资源每种都可以有自己的状态
         */
        PlacedResource,

        /**
         * \brief 从一块大的缓存中分配，底层资源一次只能有一种状态 
         */
        SubAllocation
    };


    struct CD3D12AllocateResourceDesc
    {
        D3D12_HEAP_TYPE HeapType = D3D12_HEAP_TYPE_DEFAULT;
        D3D12_HEAP_FLAGS HeapFlags = D3D12_HEAP_FLAG_NONE;
        D3D12_RESOURCE_FLAGS ResourceFlags = D3D12_RESOURCE_FLAG_NONE;
        D3D12_RESOURCE_STATES InitialResourceState = D3D12_RESOURCE_STATE_COMMON;

        static CD3D12AllocateResourceDesc MakeForUploadHeap()
        {
            return {
                .HeapType = D3D12_HEAP_TYPE_UPLOAD,
                .HeapFlags = D3D12_HEAP_FLAG_NONE,
                .ResourceFlags = D3D12_RESOURCE_FLAG_NONE,
                .InitialResourceState = D3D12_RESOURCE_STATE_GENERIC_READ,
            };
        }

        static CD3D12AllocateResourceDesc MakeForDefaultHeap()
        {
            return {
                .HeapType = D3D12_HEAP_TYPE_DEFAULT,
                .HeapFlags = D3D12_HEAP_FLAG_NONE,
                .ResourceFlags = D3D12_RESOURCE_FLAG_NONE,
                .InitialResourceState = D3D12_RESOURCE_STATE_GENERIC_READ
            };
        }
    };

    class CD3D12ResourceAllocatorStrategy
    {
    public:
        virtual ~CD3D12ResourceAllocatorStrategy() = default;
        virtual void Init(
            CD3D12Device* Device, const CD3D12AllocateResourceDesc& ResourceDesc, uint32_t MaxMemSize) = 0;
        virtual void Destroy() = 0;
        virtual bool Allocate(uint32_t MemOffset, uint32_t MemSie, CD3D12ResourceHandle& OutHandle) = 0;
        virtual void Free(CD3D12ResourceHandle& Handle) = 0;
    };


    class ID3D12ResourceAllocator
    {
    public:
        virtual ~ID3D12ResourceAllocator() = default;

        virtual CD3D12ResourceHandle Allocate(uint32_t InBufferSize, uint32_t InAlignment) = 0;

        virtual void Free(CD3D12ResourceHandle&& Handle) = 0;

        virtual void ClearUp(uint64_t ExpiresFrameCount) = 0;

        virtual bool IsFullFree() = 0;
    };

    struct CD3D12ResourceAllocatorPayloadDataBase
    {
    };

    class CD3D12BuddyAllocator :
        public CNonCopyable,
        public ID3D12ResourceAllocator
    {
    public:
        CD3D12BuddyAllocator(
            CD3D12Device* InDevice,
            EResourceAllocationStrategy InAllocationStrategy,
            const CD3D12AllocateResourceDesc& InResourceDesc,
            uint32_t InBlockMemSize,
            uint32_t InMaxMemSize);

        virtual CD3D12ResourceHandle Allocate(uint32_t InBufferSize, uint32_t InAlignment) override;

        virtual void Free(CD3D12ResourceHandle&& Handle) override;

        virtual void ClearUp(uint64_t ExpiresFrameCount) override;

        virtual bool IsFullFree() override;

    private:
        void Init();

    private:
        CBuddyAllocator BuddyAllocator;

        std::shared_ptr<CD3D12ResourceAllocatorStrategy> AllocatorStrategy;

        CD3D12Device* OwnerDevice = nullptr;

        struct CPendingDeleteResource
        {
            CD3D12ResourceHandle DeleteHandle;
            uint64_t ReleaseFenceValue = 0;
        };

        std::list<CPendingDeleteResource> PendingDeleteList;

        bool bIsInit = false;

        EResourceAllocationStrategy AllocationStrategy;

        const CD3D12AllocateResourceDesc ResourceDesc;
    };

    template <typename Allocator>
        requires std::is_base_of_v<ID3D12ResourceAllocator, Allocator>
    class CD3D12AllocatorList : public CNonCopyable
    {
    public:
        CD3D12AllocatorList(
            CD3D12Device* InDevice,
            std::function<std::unique_ptr<Allocator>(CD3D12Device*)> InCreateAllocator
        )
            : CreateAllocatorFactory{ InCreateAllocator }
            , OwnerDevice{ InDevice }
        {
        }

        CD3D12ResourceHandle Allocate(uint32_t InBufferSize, uint32_t InAlignment)
        {
            for (const std::unique_ptr<ID3D12ResourceAllocator>& MyAllocator : AllocatorList)
            {
                auto Handle = MyAllocator->Allocate(InBufferSize, InAlignment);
                if (Handle.IsValid())
                {
                    return Handle;
                }
            }

            auto MyAllocator = this->CreateResourceAllocator();
            auto Handle = MyAllocator->Allocate(InBufferSize, InAlignment);
            AllocatorList.emplace_back(std::move(MyAllocator));

            return Handle;
        }

        void ClearUp(uint64_t ExpiresFrameCount)
        {
            for (const std::unique_ptr<ID3D12ResourceAllocator>& MyAllocator : AllocatorList)
            {
                MyAllocator->ClearUp(ExpiresFrameCount);
            }

            AllocatorList.remove_if([](const std::unique_ptr<ID3D12ResourceAllocator>& MyAllocator)
                {
                    return MyAllocator->IsFullFree();
                }
            );
        }

    private:
        std::unique_ptr<ID3D12ResourceAllocator> CreateResourceAllocator()
        {
            return CreateAllocatorFactory(OwnerDevice);
        }

    private:
        std::function<std::unique_ptr<Allocator>(CD3D12Device*)> CreateAllocatorFactory;
        CD3D12Device* OwnerDevice = nullptr;
        std::list<std::unique_ptr<ID3D12ResourceAllocator>> AllocatorList;
    };

    class CD3D12FastAllocator :
        public CNonCopyable,
        public ID3D12ResourceAllocator,
        public CD3D12DeviceChildObject
    {
        struct CD3D12FastAllocatorPage
        {
            uint32_t CurrentOffset = 0;
            std::shared_ptr<CD3D12Resource> Resource;
            uint64_t LatestFence = 0;
        };

    public:
        
        CD3D12FastAllocator(CD3D12Device* const InOwnerDevice, const uint32_t PageSize, const D3D12_HEAP_TYPE HeapTypes)
            : CD3D12DeviceChildObject{ InOwnerDevice }
            , PageSize{ PageSize }
            , HeapTypes{ HeapTypes }
        {
        }

        virtual CD3D12ResourceHandle Allocate(uint32_t InBufferSize, uint32_t InAlignment) override
        {
            const auto BufferSizeForNewPage = CalculateAllocateBufferSizeForNewPage(InBufferSize, InAlignment);

            // 如果一个Page的大小无法满足分配的大小，就直接使用单独分配，不走内存池
            if (PageSize < BufferSizeForNewPage)
            {
                return StandAloneAllocate(InBufferSize, InAlignment);
            }

            if (!CurrentPage)
            {
                CurrentPage = AllocatePage();
            }
            else
            {
                const auto Offset = MemoryLib::AlignUp(CurrentPage->CurrentOffset, InAlignment);
                if (Offset + InBufferSize > PageSize)
                {
                    ReturnPage(std::move(CurrentPage));
                    CurrentPage = AllocatePage();
                }
            }

            assert(CurrentPage);
            const auto Offset = MemoryLib::AlignUp(CurrentPage->CurrentOffset, InAlignment);
            assert(Offset+ InBufferSize <= PageSize);

            CD3D12ResourceHandle Handle;
            Handle.SetAllocatorResource(CurrentPage->Resource, InBufferSize, CurrentPage->CurrentOffset);
            Handle.SetAllocatorData(this, nullptr);

            CurrentPage->CurrentOffset = Offset + InBufferSize;
            CurrentPage->LatestFence = OwnerDevice->GetFrameFence().GetNextFenceValue();

            return Handle;
        }

        virtual void Free(CD3D12ResourceHandle&& Handle) override
        {
            // 不需要实现，当 Page 中的 fence Complete时，资源会自动回收
        }

        virtual void ClearUp(uint64_t ExpiresFrameCount) override
        {
            const auto CompleteFenceValue = OwnerDevice->GetFrameFence().GetCompleteValue();

            bool bHasClear = false;
            
            for (auto& ConditionFreePage : ConditionFreePageList)
            {
                if (ConditionFreePage->Resource.use_count() == 1 && ConditionFreePage->LatestFence < (CompleteFenceValue + ExpiresFrameCount))
                {
                    if (ReadyAllocateList.size() >= 128)
                    {
                        ConditionFreePage.reset();    
                    }
                    else
                    {
                        ReadyAllocateList.emplace_back(std::move(ConditionFreePage));
                    }
                    
                    bHasClear = true;
                }
            }

            if (bHasClear)
            {
                ConditionFreePageList.remove(nullptr);
            }
        }

        virtual bool IsFullFree() override;

    private:
        std::unique_ptr<CD3D12FastAllocatorPage> AllocatePage()
        {
            if (!ReadyAllocateList.empty())
            {
                auto TempPtr = std::move(ReadyAllocateList.back());
                ReadyAllocateList.pop_back();
                TempPtr->CurrentOffset = 0;
                TempPtr->LatestFence = 0;
                return TempPtr;
            }

            const auto Device = OwnerDevice->GetDevice();
            const auto HeapProperties = CD3DX12_HEAP_PROPERTIES(HeapTypes);
            const auto ResDesc = CD3DX12_RESOURCE_DESC::Buffer(PageSize);

            ID3D12ResourceComPtr ResPtr;

            // TODO 目前仅考虑支持CPU写的堆
            assert(IsCpuWriteable(HeapTypes));
            D3D12_RESOURCE_STATES InitState = D3D12_RESOURCE_STATE_GENERIC_READ;

            CheckFailed(
                Device->CreateCommittedResource(&HeapProperties,
                                                D3D12_HEAP_FLAG_NONE,
                                                &ResDesc,
                                                D3D12_RESOURCE_STATE_GENERIC_READ,
                                                nullptr,
                                                IID_PPV_ARGS(ResPtr.ReleaseAndGetAddressOf())
                )
            );

            auto PagePtr = std::make_unique<CD3D12FastAllocatorPage>();
            PagePtr->Resource = std::make_shared<CD3D12Resource>(ResPtr, HeapTypes, ResDesc);
            return PagePtr;
        }

        void ReturnPage(std::unique_ptr<CD3D12FastAllocatorPage> ReturnPage)
        {
            ReturnPage->LatestFence = OwnerDevice->GetFrameFence().GetNextFenceValue();
            ConditionFreePageList.emplace_back(std::move(ReturnPage));
        }

        // 对于一个崭新的 Buffer Page, 分配 InBufferSize 实际需要分配多大 （考虑内存对齐的问题）
        static uint32_t CalculateAllocateBufferSizeForNewPage(const uint32_t InBufferSize, uint32_t InAlignment)
        {
            if (InAlignment != 0)
            {
                InAlignment = (D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT % InAlignment) == 0 ? 0 : InAlignment;
            }

            // 考虑内存对齐的情况，如果 InAlignment 无法满足 D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT， 那么就需要多分配InAlignment内存
            // Buffer 必须 D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT 对齐，这是必须遵守的规则
            return InBufferSize + InAlignment;
        }

        CD3D12ResourceHandle StandAloneAllocate(const uint32_t InBufferSize, const uint32_t InAlignment)
        {
            const auto BufferSize = CalculateAllocateBufferSizeForNewPage(InBufferSize, InAlignment);

            const auto Device = OwnerDevice->GetDevice();
            const auto HeapProperties = CD3DX12_HEAP_PROPERTIES(HeapTypes);
            const auto ResDesc = CD3DX12_RESOURCE_DESC::Buffer(BufferSize);

            ID3D12ResourceComPtr ResPtr;

            // TODO 目前仅考虑支持CPU写的堆
            assert(IsCpuWriteable(HeapTypes));
            D3D12_RESOURCE_STATES InitState = D3D12_RESOURCE_STATE_GENERIC_READ;

            CheckFailed(
                Device->CreateCommittedResource(&HeapProperties,
                                                D3D12_HEAP_FLAG_NONE,
                                                &ResDesc,
                                                D3D12_RESOURCE_STATE_GENERIC_READ,
                                                nullptr,
                                                IID_PPV_ARGS(ResPtr.ReleaseAndGetAddressOf())
                )
            );

            CD3D12ResourceHandle Handle;
            Handle.SetStandAlone(std::make_shared<CD3D12Resource>(ResPtr, HeapTypes, ResDesc), BufferSize);
            return Handle;
        }

    private:
        std::unique_ptr<CD3D12FastAllocatorPage> CurrentPage;

        std::list<std::unique_ptr<CD3D12FastAllocatorPage>> ConditionFreePageList;

        // 可以被分配的Page List
        std::list<std::unique_ptr<CD3D12FastAllocatorPage>> ReadyAllocateList;

        uint32_t PageSize = 0;

        D3D12_HEAP_TYPE HeapTypes = D3D12_HEAP_TYPE_UPLOAD;
    };
}
