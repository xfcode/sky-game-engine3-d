﻿#pragma once


#include <format>
#include <cstdint>
#include <unordered_map>
#include <span>

#include <windows.h>
#include <winnt.h>
#include <winerror.h>

#include "Log/Log.h"
#include "D3D12Define.h"
#include "Math/Matrix.h"

#include "D3D12Types.h"
#include "directx/d3dx12.h"

namespace SkyGameEngine3d
{
    inline void CheckFailed(HRESULT Ret)
    {
        // 错误码 https://learn.microsoft.com/zh-cn/windows/win32/direct3ddxgi/dxgi-error
        if (FAILED(Ret))
        {
            throw std::format(L"D3D12 Runtime Error, Code : {}", Ret);
        }
    }

    //! 计算常量缓存填充后的大小
    constexpr uint32_t CalConstantBufferByteSize(uint32_t Size)
    {
        return (Size + 255) & ~255;
    }

    template <typename T, bool IsConstBuffer>
    class TUploadBuffer
    {
    public:
        TUploadBuffer(ID3D12Device* Device, int32_t InElementCount)
        {
            ElementCount = InElementCount;
            auto HeapDesc = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
            auto ResDesc = CD3DX12_RESOURCE_DESC::Buffer(ElementCount * ElementSize);
            CheckFailed(Device->CreateCommittedResource(&HeapDesc,
                                                        D3D12_HEAP_FLAG_NONE,
                                                        &ResDesc,
                                                        D3D12_RESOURCE_STATE_GENERIC_READ,
                                                        nullptr,
                                                        IID_PPV_ARGS(UploadBuffer.GetAddressOf())
                )
            );
            CheckFailed(UploadBuffer->Map(0, nullptr, reinterpret_cast<void**>(&BufferMapDataPtr)));
        }

        TUploadBuffer(const TUploadBuffer&) = delete;

        TUploadBuffer& operator=(const TUploadBuffer&) = delete;

        ~TUploadBuffer()
        {
            if (UploadBuffer)
            {
                UploadBuffer->Unmap(0, nullptr);
            }

            BufferMapDataPtr = nullptr;
        }

        ID3D12Resource* GetResource() const
        {
            return UploadBuffer.Get();
        }

        D3D12_GPU_VIRTUAL_ADDRESS GetGpuAddress(int32_t ElementIndex)
        {
            if (ElementIndex < ElementCount)
            {
                auto Address = GetResource()->GetGPUVirtualAddress();
                return Address + ElementIndex * ElementSize;
            }
            throw std::wstring(L"GetGpuAddress 越界");
        }

        void SetData(int32_t ElementIndex, const T& Data)
        {
            memcpy(BufferMapDataPtr + (ElementSize * ElementIndex), &Data, sizeof(Data));
        }

    public:
        static constexpr int32_t ElementSize = IsConstBuffer ? CalConstantBufferByteSize(sizeof(T)) : sizeof(T);

    private:
        int32_t ElementCount = 0;
        uint8_t* BufferMapDataPtr = nullptr;
        ID3D12ResourceComPtr UploadBuffer;
    };

    struct ObjectConstants
    {
        Matrix4X4 WorldViewProj;
    };

    namespace D3D12Helper
    {
        inline ID3D12ResourceComPtr CreateDefaultBuffer(
            ID3D12Device* Device,
            ID3D12GraphicsCommandList* CmdList,
            void* Data,
            int32_t DataSize,
            ID3D12ResourceComPtr& UploadBuffer)
        {
            ID3D12ResourceComPtr DefaultBuffer;
            {
                auto HeapDesc = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
                auto ResourceDesc = CD3DX12_RESOURCE_DESC::Buffer(DataSize);

                CheckFailed(
                    Device->CreateCommittedResource(&HeapDesc,
                                                    D3D12_HEAP_FLAG_NONE,
                                                    &ResourceDesc,
                                                    D3D12_RESOURCE_STATE_COMMON,
                                                    nullptr,
                                                    IID_PPV_ARGS(DefaultBuffer.GetAddressOf())
                    )
                );
            }
            {
                auto HeapDesc = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
                auto ResourceDesc = CD3DX12_RESOURCE_DESC::Buffer(DataSize);

                CheckFailed(
                    Device->CreateCommittedResource(&HeapDesc,
                                                    D3D12_HEAP_FLAG_NONE,
                                                    &ResourceDesc,
                                                    D3D12_RESOURCE_STATE_GENERIC_READ,
                                                    nullptr,
                                                    IID_PPV_ARGS(UploadBuffer.GetAddressOf())
                    )
                );
            }

            D3D12_SUBRESOURCE_DATA SubResData = {
                .pData = Data,
                .RowPitch = DataSize,
                .SlicePitch = SubResData.RowPitch
            };

            {
                auto ResTrans = CD3DX12_RESOURCE_BARRIER::Transition(DefaultBuffer.Get(),
                                                                     D3D12_RESOURCE_STATE_COMMON,
                                                                     D3D12_RESOURCE_STATE_COPY_DEST
                );
                CmdList->ResourceBarrier(1, &ResTrans);
            }

            UpdateSubresources<1>(
                CmdList,
                DefaultBuffer.Get(),
                UploadBuffer.Get(),
                0,
                0,
                1,
                &SubResData
            );

            {
                auto ResTrans = CD3DX12_RESOURCE_BARRIER::Transition(DefaultBuffer.Get(),
                                                                     D3D12_RESOURCE_STATE_COPY_DEST,
                                                                     D3D12_RESOURCE_STATE_GENERIC_READ
                );
                CmdList->ResourceBarrier(1, &ResTrans);
            }

            return DefaultBuffer;
        }


        template <typename DataType>
        ID3D12ResourceComPtr CreateDefaultBuffer(
            ID3D12Device* Device,
            ID3D12GraphicsCommandList* CmdList,
            std::span<DataType> DataSpan,
            ID3D12ResourceComPtr& UploadBuffer)
        {
            return CreateDefaultBuffer(Device, CmdList, DataSpan.data(), DataSpan.size_bytes(), UploadBuffer);
        }

        //! 编译 Shader
        //! \param FileName
        //! \param Define
        //! \param EntryPoint
        //! \param Target　vs_5_0
        //! \return
        inline ID3DBlobComPtr CompileShader(const std::wstring_view FileName, const D3D_SHADER_MACRO* Define,
                                            const std::string_view EntryPoint, const std::string Target)
        {
            uint32_t CompileFlags = 0;
            if constexpr (true)
            {
                CompileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
            }

            ID3DBlobComPtr ByteCode;
            ID3DBlobComPtr Error;
            HRESULT Hr = S_OK;

            Hr = D3DCompileFromFile(FileName.data(),
                                    Define,
                                    nullptr,
                                    EntryPoint.data(),
                                    Target.data(),
                                    CompileFlags,
                                    0,
                                    ByteCode.GetAddressOf(),
                                    Error.GetAddressOf()
            );

            if (Error != nullptr)
            {
                SE_ERROR(GLogD3D12, L"D3DCompileFromFile Error {0}", Error->GetBufferPointer());
            }

            CheckFailed(Hr);
            return ByteCode;
        }
    }

    template <typename T>
    using ComPtr = Microsoft::WRL::ComPtr<T>;

    struct SubMeshGeometry
    {
        uint32_t IndexCount = 0;
        uint32_t BaseIndexPos = 0;
        uint32_t BaseVertexPos = 0;
    };


    template <typename VertexType>
    struct MeshGeometry
    {
        std::wstring KeyName;

        //https://learn.microsoft.com/zh-cn/windows/win32/api/d3dcommon/nn-d3dcommon-id3d10blob?redirectedfrom=MSDN
        ID3DBlobComPtr VertexBufferCpu;
        ID3DBlobComPtr IndexBufferCpu;

        ID3D12ResourceComPtr VertexBufferGpu;
        ID3D12ResourceComPtr IndexBufferGpu;

        ID3D12ResourceComPtr UploadVertexBufferGpu;
        ID3D12ResourceComPtr UploadIndexBufferGpu;

        static const uint32_t VertexByteStride = sizeof(VertexType);
        static const DXGI_FORMAT IndexBufferFormat = DXGI_FORMAT_R16_UINT;

        uint32_t VertexBufferSize = 0;
        uint32_t IndexBufferSize = 0;

        std::unordered_map<std::wstring, SubMeshGeometry> DrawSubMeshArgs;

        D3D12_VERTEX_BUFFER_VIEW GetVertexBufferView()
        {
            return {
                .BufferLocation = VertexBufferGpu->GetGPUVirtualAddress(),
                .SizeInBytes = VertexBufferSize,
                .StrideInBytes = VertexByteStride,
            };
        }

        D3D12_INDEX_BUFFER_VIEW GetIndexBufferView()
        {
            return {
                .BufferLocation = IndexBufferGpu->GetGPUVirtualAddress(),
                .SizeInBytes = IndexBufferSize,
                .Format = IndexBufferFormat,
            };
        }

        void DisposeUpload()
        {
            UploadIndexBufferGpu.Reset();
            UploadVertexBufferGpu.Reset();
        }
    };
}
