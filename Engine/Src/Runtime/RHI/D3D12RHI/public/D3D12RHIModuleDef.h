#pragma once

#include "HAL/PlatformDefine.h"

#ifdef D3D12RHI
    #define D3D12RHI_API DLL_EXPORT
#else
    #define D3D12RHI_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameD3D12RHI = L"D3D12RHI";
