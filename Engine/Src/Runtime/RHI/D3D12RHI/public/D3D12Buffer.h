﻿#pragma once

#include "public/RHITypes.h"
#include "D3D12Types.h"
#include "D3D12Helper.h"
#include "D3D12RHI.h"
#include "CRHIResource.h"
#include "D3D12DeviceChild.h"
#include "D3d12Resource.h"

namespace SkyGameEngine3d
{
    class CD3D12Buffer : public CRHIBuffer
    {
    public:
        using CRHIBuffer::CRHIBuffer;

        virtual void Init(class CRHIInstance* RHI);

        virtual void* Lock(EResourceLockMode LockMode) override;

        virtual void Unlock() override;

        virtual ~CD3D12Buffer() override;

    public:
        [[nodiscard]] ID3D12Resource* GetResource() const;

        [[nodiscard]] D3D12_GPU_VIRTUAL_ADDRESS GetGpuAddress() const;

    private:
        ID3D12Interface* Interface = nullptr;

        ID3D12DeviceComPtr Device = nullptr;

        uint8_t* BufferMapDataPtr = nullptr;

        ID3D12ResourceComPtr UploadBuffer;

        ID3D12ResourceComPtr DefaultBuffer;

        ID3D12ResourceComPtr ResourceComPtr;
    };


    class CD3D12BufferV2 :
        public CD3D12DeviceChildObject,
        public CRHIBuffer
    {
    public:
        CD3D12BufferV2(CD3D12Device* Device,
                       ERHIBufferFlag InBufferFlag,
                       uint32_t InBufferSize,
                       uint32_t InStride);

        void Init();

        virtual ~CD3D12BufferV2() override;

        virtual void* Lock(EResourceLockMode LockMode) override;

        virtual void Unlock() override;

    private:
        struct CBufferLockState
        {
            bool bIsLocked = false;
            CD3D12ResourceHandle UploadResourceHandle;
        };

        CBufferLockState LockState;

        CD3D12ResourceHandle ResourceHandle;
    };
}
