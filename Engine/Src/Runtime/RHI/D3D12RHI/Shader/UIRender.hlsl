Texture2D Tex: register(t0);
SamplerState Sampler: register(s0);

struct VertexIn
{
    float3 Pos : POSITION;
    float2 UV : TEXCOORD;
};

struct VertexOut
{
    float2 UV : TEXCOORD;
};

VertexOut VS(VertexIn In)
{
    VertexOut Out;

    Out.UV = In.UV;
    return Out;
}

float4 PS(VertexOut In): SV_TARGET
{
    return Tex.Sample(Sampler, In.UV);
}