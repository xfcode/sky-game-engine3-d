cbuffer cbPreObject : register(b0)
{
    float4x4 gWorldVIewProjector;
}

struct VertexIn
{
    float3 Pos : POSITION;
    float4 Color : COLOR;
};

struct VertexOut
{
    float4 Pos : SV_POSITION;
    float4 Color : COLOR;
};

VertexOut VS(VertexIn In)
{
    VertexOut Out;
    Out.Pos = mul(float4(In.Pos, 1.f), gWorldVIewProjector);
    Out.Color = In.Color;
    return Out;
}

float4 PS(VertexOut In): SV_TARGET
{
    return In.Color;
}