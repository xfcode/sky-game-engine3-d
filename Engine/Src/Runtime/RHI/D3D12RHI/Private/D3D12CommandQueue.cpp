﻿#include "D3D12CommandQueue.h"

#include "D3D12Device.h"
#include "D3D12Helper.h"

namespace SkyGameEngine3d
{
    CD3D12CommandList::CD3D12CommandList(CD3D12Device* Device, ERHICommandQueueType QueueType)
    {
        const auto OwnerDevice = Device->GetDevice();

        CheckFailed(OwnerDevice->CreateCommandAllocator(
                ConvertD3d12CommandListType(QueueType),
                IID_PPV_ARGS(Allocator.ReleaseAndGetAddressOf())
            )
        );

        CheckFailed(OwnerDevice->CreateCommandList(0,
                                                   ConvertD3d12CommandListType(QueueType),
                                                   Allocator.Get(),
                                                   nullptr,
                                                   IID_PPV_ARGS(CommandListComPtr.ReleaseAndGetAddressOf())
            )
        );
    }

    void CD3D12CommandList::Open()
    {
        CheckFailed(CommandListComPtr->Reset(Allocator.Get(), nullptr));
        bIsOpen = true;
    }

    void CD3D12CommandList::Close()
    {
        CheckFailed(CommandListComPtr->Close());
        bIsOpen = false;
    }

    CD3D12CommandQueue::CD3D12CommandQueue(CD3D12Device* Device, ERHICommandQueueType Type)
        : CD3D12DeviceChildObject{ Device }
        , QueueType{ Type }
    {
        const auto DevicePtr = Device->GetDevice();

        const D3D12_COMMAND_QUEUE_DESC QueueDesc
        {
            .Type = ConvertD3d12CommandListType(Type),
            .Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL,
            .Flags = D3D12_COMMAND_QUEUE_FLAG_NONE,
            .NodeMask = 0
        };

        CheckFailed(DevicePtr->CreateCommandQueue(&QueueDesc, IID_PPV_ARGS(QueueComPtr.ReleaseAndGetAddressOf())));
    }

    ERHICommandQueueType CD3D12CommandQueue::GetQueueType() const
    {
        return QueueType;
    }

    std::shared_ptr<CD3D12CommandList> CD3D12CommandQueue::AllocateCommandList()
    {
        if (!CommandListFreeList.empty())
        {
            auto CommandList = CommandListFreeList.front();
            CommandListFreeList.pop_front();
            return CommandList;
        }

        return std::make_shared<CD3D12CommandList>(OwnerDevice, QueueType);
    }

    void CD3D12CommandQueue::FreeCommandList(std::shared_ptr<CD3D12CommandList>&& FreeCommandList)
    {
        assert(FreeCommandList.use_count() == 1);
        CommandListFreeList.push_back(std::move(FreeCommandList));
    }
}
