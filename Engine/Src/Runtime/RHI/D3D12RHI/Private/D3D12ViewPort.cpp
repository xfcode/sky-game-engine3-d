﻿#include "D3D12ViewPort.h"

#include "D3D12CommandQueue.h"
#include "D3D12Device.h"
#include "D3D12Helper.h"
#include "D3D12RHIContext.h"

namespace SkyGameEngine3d
{
    bool CD3D12Viewport::Init(const CD3D12RHIViewportInitArg& Arg)
    {
        Hwnd = Arg.Hwnd;

        auto DXGIFactory = OwnerDevice->GetDxgiFactory1();
        auto Device = OwnerDevice->GetDevice();
        auto CmdQueue = OwnerDevice->GetCommandQueue(ERHICommandQueueType::Graphics)->GetQueueComPtr();
        auto CmdList = OwnerDevice->GetRHIContext()->GetGraphicsCommandList();

        DXGI_SWAP_CHAIN_DESC DxgiSwapChainDesc{
            .BufferDesc = DXGI_MODE_DESC{
                .Width = static_cast<UINT>(Arg.WindowW),
                .Height = static_cast<UINT>(Arg.WindowH),
                .RefreshRate = { 60, 1 },
                .Format = Arg.BackBufferFormat,
                .ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED,
                .Scaling = DXGI_MODE_SCALING_UNSPECIFIED,
            },
            .SampleDesc = DXGI_SAMPLE_DESC{
                .Count = 1,
                .Quality = 0,
            },
            .BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT,
            .BufferCount = 2,
            .OutputWindow = Hwnd,
            .Windowed = true,
            .SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD,
            // https://learn.microsoft.com/en-us/windows/win32/api/dxgi/ne-dxgi-dxgi_swap_effect
            .Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH, // 允许使用 IDXGISwapChain::ResizeTarget 切换全屏模式
        };

        CheckFailed(
            DXGIFactory->CreateSwapChain(
                CmdQueue.Get(),
                &DxgiSwapChainDesc,
                DxgiSwapChain.ReleaseAndGetAddressOf()
            )
        );


        this->Resize(Vector2DInt{ Arg.WindowW, Arg.WindowH }, Arg.BackBufferFormat);

        return true;
    }

    void CD3D12Viewport::Resize(const Vector2DInt& Size, DXGI_FORMAT Format)
    {
        ViewportSize = Size;
        BackBufferFormat = Format;

        const auto Device = OwnerDevice->GetDevice();

        RtvDescSize = Device->GetDescriptorHandleIncrementSize(
            D3D12_DESCRIPTOR_HEAP_TYPE_RTV
        );

        DsvDescSize = Device->GetDescriptorHandleIncrementSize(
            D3D12_DESCRIPTOR_HEAP_TYPE_DSV
        );

        // 必须先全部释放后备缓存，这样 ResizeBuffer 才能成功
        for (auto& BufferRef : BackBuffers)
        {
            BufferRef = nullptr;
        }

        CheckFailed(DxgiSwapChain->ResizeBuffers(BackBufferCount,
                                                 Size.X,
                                                 Size.Y,
                                                 Format,
                                                 DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH
            )
        );

        {
            // 创建后台缓存区的 RTV
            constexpr D3D12_DESCRIPTOR_HEAP_DESC D3D12DescriptorHeapDescRtv{
                .Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV,
                .NumDescriptors = 2,
                .Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE,
                .NodeMask = 0,
            };


            CheckFailed(Device->CreateDescriptorHeap(&D3D12DescriptorHeapDescRtv,
                                                     IID_PPV_ARGS(RtvDescHeap.ReleaseAndGetAddressOf())
                )
            );

            auto Index = 0;
            CD3DX12_CPU_DESCRIPTOR_HANDLE RtvHeapHandle{ RtvDescHeap->GetCPUDescriptorHandleForHeapStart() };

            for (auto& Res : BackBuffers)
            {
                CheckFailed(DxgiSwapChain->GetBuffer(Index, IID_PPV_ARGS(Res.GetAddressOf())));

                Device->CreateRenderTargetView(Res.Get(), nullptr, RtvHeapHandle);

                RtvHeapHandle.Offset(1, RtvDescSize);

                Index++;
            }
        }

        {
            constexpr D3D12_DESCRIPTOR_HEAP_DESC D3D12DescriptorHeapDescDsv{
                .Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV,
                .NumDescriptors = 1,
                .Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE,
                .NodeMask = 0,
            };

            CheckFailed(Device->CreateDescriptorHeap(&D3D12DescriptorHeapDescDsv,
                                                     IID_PPV_ARGS(DsvDescHeap.GetAddressOf())
                )
            );


            constexpr DXGI_FORMAT DepthStencilFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;

            const D3D12_RESOURCE_DESC DepthStencilDesc{
                // 资源的类型
                .Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D,
                .Alignment = 0,
                .Width = static_cast<UINT64>(ViewportSize.X),
                .Height = static_cast<UINT>(ViewportSize.Y),
                .DepthOrArraySize = 1,
                .MipLevels = 1,
                .Format = DepthStencilFormat,
                .SampleDesc = DXGI_SAMPLE_DESC{
                    .Count = 1,
                    .Quality = 0,
                },
                .Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN,
                .Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL,
            };

            constexpr D3D12_CLEAR_VALUE OptClear{
                .Format = DepthStencilFormat,
                .DepthStencil = D3D12_DEPTH_STENCIL_VALUE{
                    .Depth = 1.f,
                    .Stencil = 0,
                }
            };

            const CD3DX12_HEAP_PROPERTIES HeapProperties{ D3D12_HEAP_TYPE_DEFAULT };
            CheckFailed(Device->CreateCommittedResource(
                    &HeapProperties,
                    D3D12_HEAP_FLAG_NONE,
                    &DepthStencilDesc,
                    D3D12_RESOURCE_STATE_COMMON,
                    &OptClear,
                    IID_PPV_ARGS(DepthStencilView.GetAddressOf())
                )
            );

            Device->CreateDepthStencilView(
                DepthStencilView.Get(),
                nullptr,
                GetDepthStencilView()
            );
        }
    }

    D3D12_CPU_DESCRIPTOR_HANDLE CD3D12Viewport::GetCurrentBackBufferView() const
    {
        return CD3DX12_CPU_DESCRIPTOR_HANDLE{
            RtvDescHeap->GetCPUDescriptorHandleForHeapStart(),
            CurrentBackBufferIndex,
            static_cast<UINT>(RtvDescSize),
        };
    }

    D3D12_CPU_DESCRIPTOR_HANDLE CD3D12Viewport::GetDepthStencilView() const
    {
        return DsvDescHeap->GetCPUDescriptorHandleForHeapStart();
    }

    ID3D12ResourceComPtr CD3D12Viewport::GetCurrentBackBufferResource() const
    {
        return BackBuffers[CurrentBackBufferIndex];
    }

    ID3D12ResourceComPtr CD3D12Viewport::GetDepthStencilBufferResource() const
    {
        return DepthStencilView;
    }

    Vector2DInt CD3D12Viewport::GetViewPortSize() const
    {
        return ViewportSize;
    }

    IDXGISwapChainComPtr CD3D12Viewport::GetSwapChain() const
    {
        return DxgiSwapChain;
    }

    void CD3D12Viewport::Present()
    {
        // 先提交全部命令
        // TODO 提交命令

        // 然后在呈现
        CheckFailed(DxgiSwapChain->Present(1, 0));
        CurrentBackBufferIndex = (CurrentBackBufferIndex + 1) % 2;
    }
}
