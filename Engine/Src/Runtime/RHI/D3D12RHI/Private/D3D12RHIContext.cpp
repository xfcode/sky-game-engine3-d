﻿#include "D3D12RHIContext.h"

#include <array>

#include "D3D12Device.h"
#include "D3D12Helper.h"
#include "D3D12Shader.h"
#include "directx/d3dx12.h"

#include "D3D12Types.h"
#include "D3D12ViewPort.h"

namespace SkyGameEngine3d
{
    CD3D12RHIContext::CD3D12RHIContext(CD3D12Device* InDevice)
        : CD3D12DeviceChildObject{ InDevice }
    {
        DeviceComPtr = InDevice->GetDevice();
    }

    void CD3D12RHIContext::BeginDrawScene()
    {
    }

    void CD3D12RHIContext::EndDrawScene()
    {
    }

    void CD3D12RHIContext::BeginDrawViewport(std::shared_ptr<RHIViewport> InViewport)
    {
        const auto Viewport = ResourceCast<CD3D12Viewport>(InViewport);
        D3D12_CPU_DESCRIPTOR_HANDLE BackBuffer = Viewport->GetCurrentBackBufferView();
        const auto CommandList = GetGraphicsCommandList();

        // this->BeginRenderPass()
        // TODO 启用 BeginRenderPass 2023-08-01 23:39:37
    }

    void CD3D12RHIContext::EndDrawViewport()
    {
        this->EndRenderPass();
        // TODO 转换后备缓存的 屏障
        // Parent
    }

    void CD3D12RHIContext::BeginRenderPass(std::shared_ptr<CRHITexture> InRenderTarget)
    {
    }

    void CD3D12RHIContext::EndRenderPass()
    {
    }

    void CD3D12RHIContext::SetViewport(float MinX, float MinY, float MinDepth, float MaxX, float MaxY, float MaxDepth)
    {
    }

    void CD3D12RHIContext::SetRenderTarget(std::shared_ptr<CRHITexture> RenderTarget)
    {
    }

    void CD3D12RHIContext::CreateUIRootSignature()
    {
        if (UIRootSignature)
        {
            return;
        }

        // 创建根参数
        std::array<CD3DX12_ROOT_PARAMETER, 1> SlotRootParameter{ };


        // 纹理
        {
            constexpr D3D12_DESCRIPTOR_RANGE DescriptorRange
            {
                .RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV,
                .NumDescriptors = 1,
                .BaseShaderRegister = 0,
                .RegisterSpace = 0,
                .OffsetInDescriptorsFromTableStart = 0
            };
            SlotRootParameter[0].InitAsDescriptorTable(1, &DescriptorRange);
        }

        // 采样器
        {
            // constexpr D3D12_DESCRIPTOR_RANGE DescriptorRange
            // {
            //     .RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER,
            //     .NumDescriptors = 1,
            //     .BaseShaderRegister = 0,
            //     .RegisterSpace = 0,
            //     .OffsetInDescriptorsFromTableStart = 0
            // };
            // 
            // SlotRootParameter[1].InitAsDescriptorTable(1, &DescriptorRange);
        }

        std::array<CD3DX12_STATIC_SAMPLER_DESC, 1> LinearSamplerDesc
        {
            CD3DX12_STATIC_SAMPLER_DESC(0, D3D12_FILTER_MIN_MAG_MIP_LINEAR)
        };


        // 创建根签名
        const CD3DX12_ROOT_SIGNATURE_DESC RootSignatureDesc
        {
            SlotRootParameter.size(),
            SlotRootParameter.data(),
            LinearSamplerDesc.size(),
            LinearSamplerDesc.data(),
            D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT
        };

        ID3DBlobComPtr SerializedRootSignature;
        ID3DBlobComPtr Error;

        const auto Hr = D3D12SerializeRootSignature(&RootSignatureDesc,
                                                    D3D_ROOT_SIGNATURE_VERSION_1,
                                                    SerializedRootSignature.GetAddressOf(),
                                                    Error.GetAddressOf()
        );
        CheckFailed(Hr);

        CheckFailed(
            DeviceComPtr->CreateRootSignature(0,
                                              SerializedRootSignature->GetBufferPointer(),
                                              SerializedRootSignature->GetBufferSize(),
                                              IID_PPV_ARGS(UIRootSignature.ReleaseAndGetAddressOf())
            )
        );
    }

    void CD3D12RHIContext::SetGraphicsPipelineState(const RHIGraphicsPipelineState& PipelineState)
    {
        const auto& ShaderStateInput = PipelineState.ShaderStateInput;

        D3D12_GRAPHICS_PIPELINE_STATE_DESC PsoDesc;
        memset(&PsoDesc, 0, sizeof(D3D12_GRAPHICS_STATES));

        const auto& ElementList = ShaderStateInput.VertexDeclare->VertexElementDeclareList;
        std::vector<D3D12_INPUT_ELEMENT_DESC> ElementDescArray;
        ElementDescArray.reserve(ElementList.size());
        uint32_t Index = 0;
        for (const auto& Element : ElementList)
        {
            ElementDescArray.push_back(D3D12_INPUT_ELEMENT_DESC{
                    .SemanticName = std::format("Semantic{0}", Index).c_str(),
                    .SemanticIndex = Index,
                    .Format = ConvertFormVertexFormat(Element.DataFormat),
                    .InputSlot = Element.InputSlot,
                    .AlignedByteOffset = Element.Offset,
                    .InputSlotClass = D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,
                    .InstanceDataStepRate = 0,
                }
            );

            Index++;
        }
        PsoDesc.InputLayout =
        {
            .pInputElementDescs = ElementDescArray.data(),
            .NumElements = static_cast<uint32_t>(ElementDescArray.size())
        };

        PsoDesc.pRootSignature = UIRootSignature.Get();

        const auto* Vs = dynamic_cast<D3D12VertexShader*>(ShaderStateInput.VS.get());
        PsoDesc.VS = Vs->GetShaderBytecode();
        const auto* Ps = dynamic_cast<D3D12VertexShader*>(ShaderStateInput.PS.get());
        PsoDesc.PS = Ps->GetShaderBytecode();


        // 光删化状态
        PsoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
        PsoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
        PsoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
        PsoDesc.SampleMask = UINT_MAX;
        PsoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
        PsoDesc.NumRenderTargets = 1;
        // TODO 应该使用一个变量来统一， BackBuffer 的格式
        PsoDesc.RTVFormats[0] = DXGI_FORMAT_B8G8R8A8_UNORM;
        PsoDesc.SampleDesc.Count = 1;
        PsoDesc.SampleDesc.Quality = 0;
        // TODO 应该统一
        PsoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
        CheckFailed(DeviceComPtr->CreateGraphicsPipelineState(&PsoDesc, IID_PPV_ARGS(UIPso.GetAddressOf())));
    }

    void CD3D12RHIContext::DrawIndexInstanced(const CRHIDrawIndexInstancedArgs& Args)
    {
        
    }

    void CD3D12RHIContext::SetVertexBuffer(const RHIBufferPtr& RHIBuffer, uint32_t Offset, uint32_t InputSlotIndex)
    {
        
    }
}
