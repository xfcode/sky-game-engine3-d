﻿#include "D3D12Device.h"

#include "D3D12CommandQueue.h"
#include "D3D12Helper.h"
#include "D3d12Resource.h"
#include "D3D12ResourceManager.h"

namespace SkyGameEngine3d
{
    void CD3D12Device::Init()
    {
        if constexpr (true)
        {
            ID3D12DebugComPtr Debug;
            CheckFailed(D3D12GetDebugInterface(IID_PPV_ARGS(Debug.GetAddressOf())));
            Debug->EnableDebugLayer();
        }

        // https://learn.microsoft.com/zh-cn/windows/win32/api/dxgi/nf-dxgi-createdxgifactory1
        CheckFailed(CreateDXGIFactory1(IID_PPV_ARGS(&DxgiFactory1)));

        ComPtr<IDXGIAdapter1> Adapter;
        UINT AdapterIndex = 0;
        while (DxgiFactory1->EnumAdapters1(AdapterIndex, Adapter.GetAddressOf()) != DXGI_ERROR_NOT_FOUND)
        {
            DXGI_ADAPTER_DESC1 Desc;
            CheckFailed(Adapter->GetDesc1(&Desc));

            if (Desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
            {
                AdapterIndex++;
                continue;
            }

            SE_LOG(GLogD3D12, L"Used Adapter : {}", Desc.Description);
            break;
        }

        DxgiAdapter1 = Adapter;
        // https://learn.microsoft.com/zh-cn/windows/win32/api/d3d12/nf-d3d12-d3d12createdevice
        CheckFailed(D3D12CreateDevice(Adapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(Device.GetAddressOf())));

        CheckFailed(Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(FrameFence.FenceComPtr.GetAddressOf())));

        ResourceManager = std::make_shared<CD3D12ResourceManager>();
        ResourceManager->Init(this);
    }

    void CD3D12Device::Shutdown()
    {
    }

    CD3D12ResourceManager* CD3D12Device::GetResourceManager() const
    {
        return ResourceManager.get();
    }

    CD3D12CommandQueue* CD3D12Device::GetCommandQueue(ERHICommandQueueType Type)
    {
        auto& QueueRef = CommandQueue[static_cast<uint8_t>(Type)];
        if (QueueRef)
        {
            return QueueRef.get();
        }

        QueueRef = std::make_shared<CD3D12CommandQueue>(this, Type);
        return QueueRef.get();
    }
}
