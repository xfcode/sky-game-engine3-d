﻿#include "D3D12ResourceManager.h"

#include "D3D12Buffer.h"
#include "D3D12Texture.h"

namespace SkyGameEngine3d
{
    void CD3D12ResourceManager::Init(CD3D12Device* InDevice)
    {
        OwnerDevice = InDevice;

        UploadAllocate = std::make_unique<BuddyAllocatorList>(OwnerDevice,
                                                              [](CD3D12Device* Device)
                                                              {
                                                                  auto UploadDesc =
                                                                      CD3D12AllocateResourceDesc::MakeForUploadHeap();
                                                                  return std::make_unique<CD3D12BuddyAllocator>(Device,
                                                                      EResourceAllocationStrategy::SubAllocation,
                                                                      UploadDesc,
                                                                      512,
                                                                      512 * 1024 * 16
                                                                  );
                                                              }
        );

        DefaultAllocate = std::make_unique<BuddyAllocatorList>(OwnerDevice,
                                                               [](CD3D12Device* Device)
                                                               {
                                                                   auto UploadDesc =
                                                                       CD3D12AllocateResourceDesc::MakeForDefaultHeap();
                                                                   return std::make_unique<CD3D12BuddyAllocator>(Device,
                                                                       EResourceAllocationStrategy::SubAllocation,
                                                                       UploadDesc,
                                                                       512,
                                                                       512 * 1024 * 16
                                                                   );
                                                               }
        );

        FastAllocator = std::make_unique<CD3D12FastAllocator>(OwnerDevice, 1024 * 1024 * 16, D3D12_HEAP_TYPE_UPLOAD);
    }

    CD3D12ResourceHandle CD3D12ResourceManager::AllocateUploadHeapBuffer(
        const uint32_t InBufferSize,
        const uint32_t InAlignment) const
    {
        return UploadAllocate->Allocate(InBufferSize, InAlignment);
    }

    CD3D12ResourceHandle CD3D12ResourceManager::AllocateDefaultHeapBuffer(
        uint32_t InBufferSize, uint32_t InAlignment) const
    {
        return DefaultAllocate->Allocate(InBufferSize, InAlignment);
    }

    CD3D12ResourceHandle CD3D12ResourceManager::FastAllocateUploadHeapBuffer(uint32_t InBufferSize,
                                                                             uint32_t InAlignment) const
    {
        return FastAllocator->Allocate(InBufferSize, InAlignment);
    }

    TRHIResourcePtr<RHIViewport> CD3D12ResourceManager::CreateRHIViewport(const RHIViewportCreateArg& Arg)
    {
        auto Viewport = MakeRHIResource<CD3D12Viewport>([&]()-> CD3D12Viewport* {
                const auto ViewportObj = new CD3D12Viewport(OwnerDevice);
                ViewportObj->Init(CD3D12RHIViewportInitArg{
                        .Hwnd = static_cast<HWND>(Arg.WindowHandle),
                        .WindowW = Arg.WindowW,
                        .WindowH = Arg.WindowH,
                        .BackBufferFormat = ConvertFormPixelFormat(Arg.BackBufferFormat)
                    }
                );
                return ViewportObj;
            }
        );

        return Viewport;
    }

    TRHIResourcePtr<CRHITexture> CD3D12ResourceManager::CreateTexture(const RHITextureCreateArgs& Args)
    {
        auto Texture = MakeRHIResource<CD3D12Texture>([&]
            {
                const CRHITextureDesc Desc
                {
                    .Dimension = Args.Dimension,
                    .Flags = Args.Flags,
                    .Format = Args.Format,
                    .Size = Args.Size,
                    .MipsCount = Args.MipsCount,
                    .DebugName = Args.DebugName,
                };

                const auto TextureObject = new CD3D12Texture(OwnerDevice, Desc);
                TextureObject->Init();
                return TextureObject;
            }
        );
        return Texture;
    }

    TRHIResourcePtr<CRHIShader> CD3D12ResourceManager::CreateShader(const RHIShaderCreateArgs& Args)
    {
        return nullptr;
    }

    TRHIResourcePtr<CRHIBuffer> CD3D12ResourceManager::CreateBuffer(const RHICreateBufferArgs& Args)
    {
        auto Buffer = MakeRHIResource<CD3D12BufferV2>([this, &Args]()-> CD3D12BufferV2* {
                const auto Obj = new CD3D12BufferV2(OwnerDevice, Args.InBufferFlag, Args.InBufferSize, Args.InStride);
                Obj->Init();
                return Obj;
            }
        );

        return Buffer;
    }
}
