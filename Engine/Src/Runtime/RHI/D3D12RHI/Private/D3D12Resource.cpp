﻿#include "D3D12Helper.h"
#include "D3d12Resource.h"
#include "D3D12RHI.h"

namespace SkyGameEngine3d
{
    class CD3D12ResourceAllocatorStrategy_PlacedResource :
        public CD3D12ResourceAllocatorStrategy
    {
    protected:
        virtual void Init(
            CD3D12Device* InDevice,
            const CD3D12AllocateResourceDesc& InResourceDesc,
            const uint32_t InMaxMemSize) override
        {
            OwnerDevice = InDevice;
            AllocateResourceDesc = InResourceDesc;
            MaxMemSize = InMaxMemSize;
            const auto D3dDevice = OwnerDevice->GetDevice();

            const D3D12_HEAP_PROPERTIES HeapProperties = CD3DX12_HEAP_PROPERTIES(AllocateResourceDesc.HeapType);

            const D3D12_HEAP_DESC HeapDesc
            {
                .SizeInBytes = InMaxMemSize,
                .Properties = HeapProperties,
                .Alignment = 0,
                .Flags = AllocateResourceDesc.HeapFlags
            };

            CheckFailed(D3dDevice->CreateHeap(&HeapDesc, IID_PPV_ARGS(HeapPtr.ReleaseAndGetAddressOf())));
        }

        virtual void Destroy() override
        {
        }

        virtual bool Allocate(
            const uint32_t MemOffset,
            const uint32_t MemSize,
            CD3D12ResourceHandle& OutHandle) override
        {
            assert(MemOffset + MemSize <= MaxMemSize);

            const D3D12_RESOURCE_DESC ResourceDesc =
                CD3DX12_RESOURCE_DESC::Buffer(MemSize, AllocateResourceDesc.ResourceFlags);

            const auto Device = OwnerDevice->GetDevice();

            ID3D12ResourceComPtr ResourceComPtr;

            CheckFailed(
                Device->CreatePlacedResource(
                    HeapPtr.Get(),
                    MemOffset,
                    &ResourceDesc,
                    AllocateResourceDesc.InitialResourceState,
                    nullptr,
                    IID_PPV_ARGS(ResourceComPtr.ReleaseAndGetAddressOf())
                )
            );

            const auto Resource = std::make_shared<CD3D12Resource>(
                ResourceComPtr,
                AllocateResourceDesc.HeapType,
                ResourceDesc
            );

            OutHandle.SetAllocatorResource(Resource, MemSize, 0);

            return true;
        }

        virtual void Free(CD3D12ResourceHandle& Handle) override
        {
        }

    private:
        uint32_t MaxMemSize = 0;
        CD3D12AllocateResourceDesc AllocateResourceDesc;
        ID3D12HeapComPtr HeapPtr;
        CD3D12Device* OwnerDevice = nullptr;
    };

    class CD3D12ResourceAllocatorStrategy_SubAllocate :
        public CD3D12ResourceAllocatorStrategy
    {
    protected:
        virtual void Init(
            CD3D12Device* InDevice,
            const CD3D12AllocateResourceDesc& InResourceDesc,
            const uint32_t InMaxMemSize) override
        {
            OwnerDevice = InDevice;
            MaxMemSize = InMaxMemSize;

            const auto Device = OwnerDevice->GetDevice();

            const auto Property = CD3DX12_HEAP_PROPERTIES(InResourceDesc.HeapType);

            const D3D12_RESOURCE_DESC ResourceDesc = CD3DX12_RESOURCE_DESC::Buffer(
                InMaxMemSize,
                InResourceDesc.ResourceFlags
            );

            CheckFailed(
                Device->CreateCommittedResource(
                    &Property,
                    InResourceDesc.HeapFlags,
                    &ResourceDesc,
                    InResourceDesc.InitialResourceState,
                    nullptr,
                    IID_PPV_ARGS(ResourcePtr.ReleaseAndGetAddressOf())
                )

            );

            Resource = std::make_shared<CD3D12Resource>(ResourcePtr, InResourceDesc.HeapType, ResourceDesc);
        }

        virtual void Destroy() override
        {
        }

        virtual bool Allocate(
            const uint32_t InMemOffset, const uint32_t InMemSie, CD3D12ResourceHandle& OutHandle) override
        {
            assert(InMemOffset + InMemSie <= MaxMemSize);
            OutHandle.SetAllocatorResource(Resource, InMemSie, InMemOffset);
            return true;
        }

        virtual void Free(CD3D12ResourceHandle& Handle) override
        {
        }

    private:
        std::shared_ptr<CD3D12Resource> Resource;
        CD3D12Device* OwnerDevice = nullptr;
        uint32_t MaxMemSize = 0;
        ID3D12ResourceComPtr ResourcePtr;
    };


    void* CD3D12Resource::Map()
    {
        if (MapCount == 0)
        {
            assert(CpuPtr == nullptr);
            CheckFailed(Resource->Map(0, nullptr, &CpuPtr));
        }

        MapCount++;

        return CpuPtr;
    }

    void CD3D12Resource::UnMap()
    {
        MapCount--;
        assert(MapCount >= 0);

        if (MapCount == 0)
        {
            assert(CpuPtr);
            Resource->Unmap(0, nullptr);
            CpuPtr = nullptr;
        }
    }

    const D3D12_GPU_VIRTUAL_ADDRESS& CD3D12Resource::GetGpuAddress() const
    {
        return GpuVirtualAddress;
    }

    CD3D12ResourceHandle::CD3D12ResourceHandle(CD3D12ResourceHandle&& Other) noexcept
    {
        Resource = std::move(Other.Resource);
        AllocatorPayloadData = std::move(Other.AllocatorPayloadData);

        GpuAddr = Other.GpuAddr;
        Other.GpuAddr = 0;
        ResourceOffset = Other.ResourceOffset;
        Other.ResourceOffset = 0;
        OwnerAllocator = Other.OwnerAllocator;
        Other.OwnerAllocator = nullptr;
        MapAddr = Other.MapAddr;
        Other.MapAddr = nullptr;
    }

    CD3D12ResourceHandle& CD3D12ResourceHandle::operator=(CD3D12ResourceHandle&& Other) noexcept
    {
        if (this == &Other)
        {
            return *this;
        }

        Resource = std::move(Other.Resource);
        AllocatorPayloadData = std::move(Other.AllocatorPayloadData);

        GpuAddr = Other.GpuAddr;
        Other.GpuAddr = 0;
        ResourceOffset = Other.ResourceOffset;
        Other.ResourceOffset = 0;
        OwnerAllocator = Other.OwnerAllocator;
        Other.OwnerAllocator = nullptr;
        MapAddr = Other.MapAddr;
        Other.MapAddr = nullptr;
        
        return *this;
    }

    CD3D12ResourceHandle::~CD3D12ResourceHandle()
    {
        this->Release();
    }

    void CD3D12ResourceHandle::Release()
    {
        if (ResourceManagementMode == EResourceManagementMode::ResourceAllocator)
        {
            if (OwnerAllocator)
            {
                assert(Resource);
                OwnerAllocator->Free(std::move(*this));
            }
        }
        else if (ResourceManagementMode == EResourceManagementMode::StandAlone)
        {
            assert(Resource);
            assert(Resource.use_count() == 1);
            GetD3D12RHIInstance()->AddPendingDeleteResource(std::move(Resource));
        }

        ResourceManagementMode = EResourceManagementMode::None;
    }

    void CD3D12ResourceHandle::SetAllocatorData(ID3D12ResourceAllocator* Owner,
                                                const std::shared_ptr<CD3D12ResourceAllocatorPayloadDataBase>&
                                                PayloadData)
    {
        assert(ResourceManagementMode==EResourceManagementMode::ResourceAllocator);
        OwnerAllocator = Owner;
        AllocatorPayloadData = PayloadData;
    }

    std::shared_ptr<CD3D12ResourceAllocatorPayloadDataBase> CD3D12ResourceHandle::GetAllocatorPayloadData() const
    {
        return AllocatorPayloadData;
    }

    void CD3D12ResourceHandle::SetAllocatorResource(const std::shared_ptr<CD3D12Resource>& InResource,
                                                    const uint32_t SizeInBytes, const uint32_t InResourceOffset)
    {
        assert(ResourceManagementMode==EResourceManagementMode::None);
        ResourceManagementMode = EResourceManagementMode::ResourceAllocator;
        Resource = InResource;
        ResourceOffset = InResourceOffset;
        GpuAddr = Resource->GetGpuAddress() + InResourceOffset;
        ResourceSizeInBytes = SizeInBytes;
    }

    void CD3D12ResourceHandle::SetStandAlone(std::shared_ptr<CD3D12Resource>&& InResource, const uint32_t SizeInBytes)
    {
        assert(ResourceManagementMode==EResourceManagementMode::None);
        ResourceManagementMode = EResourceManagementMode::StandAlone;
        Resource = std::move(InResource);
        ResourceOffset = 0;
        GpuAddr = Resource->GetGpuAddress();
        ResourceSizeInBytes = SizeInBytes;
    }

    struct CBuddyAllocatorPayloadData :
        public CD3D12ResourceAllocatorPayloadDataBase
    {
        std::shared_ptr<CBuddyAllocator::CAllocateData> Data;
    };

    CD3D12BuddyAllocator::CD3D12BuddyAllocator(
        CD3D12Device* InDevice, const EResourceAllocationStrategy InAllocationStrategy,
        const CD3D12AllocateResourceDesc& InResourceDesc, const uint32_t InBlockMemSize, const uint32_t InMaxMemSize)
        : BuddyAllocator{ InBlockMemSize, InMaxMemSize }
        , OwnerDevice{ InDevice }
        , AllocationStrategy{ InAllocationStrategy }
        , ResourceDesc{ InResourceDesc }
    {
    }

    CD3D12ResourceHandle CD3D12BuddyAllocator::Allocate(const uint32_t InBufferSize, const uint32_t InAlignment)
    {
        if (!bIsInit)
        {
            this->Init();
        }

        if (const auto AllocateData = BuddyAllocator.TryAllocate(InBufferSize, InAlignment))
        {
            CD3D12ResourceHandle Handle;
            if (AllocatorStrategy->Allocate(AllocateData->AlignmentMemOffset, InBufferSize, Handle))
            {
                auto PayloadData = std::make_shared<CBuddyAllocatorPayloadData>();
                PayloadData->Data = AllocateData;
                Handle.SetAllocatorData(this, std::move(PayloadData));
                return Handle;
            }
        }

        return { };
    }

    void CD3D12BuddyAllocator::Free(CD3D12ResourceHandle&& Handle)
    {
        const auto& FrameFence = OwnerDevice->GetFrameFence();
        auto NextFenceValue = FrameFence.GetNextFenceValue();
        PendingDeleteList.emplace_back(std::move(Handle), NextFenceValue);
    }

    void CD3D12BuddyAllocator::ClearUp(const uint64_t ExpiresFrameCount)
    {
        const auto CompleteValue = OwnerDevice->GetFrameFence().GetCompleteValue();
        if (CompleteValue < ExpiresFrameCount)
        {
            return;
        }

        const auto ExpiresValue = CompleteValue - ExpiresFrameCount;
        for (auto Iter = PendingDeleteList.begin(); Iter != PendingDeleteList.end();)
        {
            if (Iter->ReleaseFenceValue <= ExpiresValue)
            {
                auto& Handle = Iter->DeleteHandle;

                // 取消映射
                if (Handle.IsMap())
                {
                    Handle.Unmap();
                }

                // 释放资源
                const auto PayLoadData = std::static_pointer_cast<CBuddyAllocatorPayloadData>(
                    Handle.GetAllocatorPayloadData()
                );
                BuddyAllocator.Free(PayLoadData->Data);
                AllocatorStrategy->Free(Handle);
                Iter = PendingDeleteList.erase(Iter);
            }
            else
            {
                ++Iter;
            }
        }
    }

    bool CD3D12BuddyAllocator::IsFullFree()
    {
        return BuddyAllocator.GetUsedMemSize() == 0;
    }

    void CD3D12BuddyAllocator::Init()
    {
        bIsInit = true;

        if (AllocationStrategy == EResourceAllocationStrategy::PlacedResource)
        {
            AllocatorStrategy = std::make_shared<CD3D12ResourceAllocatorStrategy_PlacedResource>();
        }
        else if (AllocationStrategy == EResourceAllocationStrategy::SubAllocation)
        {
            AllocatorStrategy = std::make_shared<CD3D12ResourceAllocatorStrategy_SubAllocate>();
        }
        else
        {
            assert(false);
        }

        AllocatorStrategy->Init(OwnerDevice, ResourceDesc, BuddyAllocator.GetMaxMemSize());
    }

    bool CD3D12FastAllocator::IsFullFree()
    {
        return false;
    }
}
