﻿#include <span>
#include "D3D12RHI.h"

#include <memory>

#include "D3D12Buffer.h"
#include "D3D12Helper.h"
#include "Log/Log.h"
#include "D3D12Define.h"
#include "D3D12ResourceManager.h"
#include "D3D12RHIContext.h"
#include "D3D12Shader.h"
#include "D3D12Texture.h"
#include "directx/d3dx12.h"
#include "Misc/Paths.h"

using namespace Microsoft::WRL;

namespace SkyGameEngine3d
{
    void D3D12RHIImp::Init()
    {
        if constexpr (true)
        {
            Windows::ComPtr<ID3D12Debug> Debug;
            CheckFailed(D3D12GetDebugInterface(IID_PPV_ARGS(Debug.GetAddressOf())));
            Debug->EnableDebugLayer();
        }

        // https://learn.microsoft.com/zh-cn/windows/win32/api/dxgi/nf-dxgi-createdxgifactory1
        CheckFailed(CreateDXGIFactory1(IID_PPV_ARGS(&DXGIFactory)));

        ComPtr<IDXGIAdapter1> Adapter;
        UINT AdapterIndex = 0;
        while (DXGIFactory->EnumAdapters1(AdapterIndex, Adapter.GetAddressOf()) != DXGI_ERROR_NOT_FOUND)
        {
            DXGI_ADAPTER_DESC1 Desc;
            Adapter->GetDesc1(&Desc);

            if (Desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
            {
                AdapterIndex++;
                continue;
            }

            SE_LOG(GLogD3D12, L"Used Adapter : {}", Desc.Description);
            break;
        }

        DXGIAdapter = std::move(Adapter);
        // https://learn.microsoft.com/zh-cn/windows/win32/api/d3d12/nf-d3d12-d3d12createdevice
        CheckFailed(D3D12CreateDevice(Adapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(Device.GetAddressOf())));

        //https://learn.microsoft.com/zh-cn/windows/win32/api/d3d12/nf-d3d12-id3d12device-createcommandqueue?redirectedfrom=MSDN
        D3D12_COMMAND_QUEUE_DESC CommandQueueDesc{
            .Type = D3D12_COMMAND_LIST_TYPE_DIRECT,
            .Flags = D3D12_COMMAND_QUEUE_FLAG_NONE,
        };

        CheckFailed(Device->CreateCommandQueue(&CommandQueueDesc, IID_PPV_ARGS(CommandQueue.GetAddressOf())));

        CheckFailed(
            Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(Fence.GetAddressOf()))
        );

        CheckFailed(Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT,
                                                   IID_PPV_ARGS(CommandAllocator.GetAddressOf())
            )
        );

        CheckFailed(Device->CreateCommandList(0,
                                              D3D12_COMMAND_LIST_TYPE_DIRECT,
                                              CommandAllocator.Get(),
                                              nullptr,
                                              IID_PPV_ARGS(CommandList.GetAddressOf())
            )
        );

        CommandList->Close();
        CheckFailed(CommandList->Reset(this->CommandAllocator.Get(), nullptr));

        D3D12_CONSTANT_BUFFER_VIEW_DESC ConstantBufferViewDesc;
        if constexpr (bUsedCbBuffer)
        {
            ObjectCbBuffer = std::make_shared<CD3D12Buffer>(ERHIBufferFlag::Dynamic,
                                                            static_cast<uint32_t>(sizeof(ObjectConstants)),
                                                            0
            );
            ObjectCbBuffer->Init(this);

            D3D12_GPU_VIRTUAL_ADDRESS ObjectCbAddress = ObjectCbBuffer->GetGpuAddress();

            ConstantBufferViewDesc = {
                .BufferLocation = ObjectCbAddress,
                .SizeInBytes = ObjectCbBuffer->GetSize(), //这里必须 256 对齐
            };
        }
        else
        {
            // 构建常量缓存
            ObjectCb = std::make_unique<ObjectConstantsUploadBuffer>(Device.Get(), 1);
            int32_t BoxCbIndex = 0;
            D3D12_GPU_VIRTUAL_ADDRESS ObjectCbAddress = ObjectCb->GetGpuAddress(BoxCbIndex);

            ConstantBufferViewDesc = {
                .BufferLocation = ObjectCbAddress,
                .SizeInBytes = ObjectConstantsUploadBuffer::ElementSize,
            };
        }

        D3D12_DESCRIPTOR_HEAP_DESC CbDescriptorHeapDesc = {
            .Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV,
            .NumDescriptors = 1,
            .Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE,
            .NodeMask = 0,
        };

        CheckFailed(Device->CreateDescriptorHeap(&CbDescriptorHeapDesc,
                                                 IID_PPV_ARGS(CbDescriptorHeap.GetAddressOf())
            )
        );

        Device->CreateConstantBufferView(&ConstantBufferViewDesc,
                                         CbDescriptorHeap->GetCPUDescriptorHandleForHeapStart()
        );

        // 构建根签名
        CD3DX12_ROOT_PARAMETER SlotRootParameter[1];
        CD3DX12_DESCRIPTOR_RANGE CbvTable;
        CbvTable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
        SlotRootParameter[0].InitAsDescriptorTable(1, &CbvTable);

        CD3DX12_ROOT_SIGNATURE_DESC RootSignatureDesc = {
            1,
            SlotRootParameter,
            0,
            nullptr,
            D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT
        };

        ID3DBlobComPtr SerializedRootSig;
        ID3DBlobComPtr ErrorBlob;

        auto Hr = D3D12SerializeRootSignature(&RootSignatureDesc,
                                              D3D_ROOT_SIGNATURE_VERSION_1,
                                              SerializedRootSig.GetAddressOf(),
                                              ErrorBlob.GetAddressOf()
        );
        if (!SUCCEEDED(Hr))
        {
            //SE_ERROR(GLogD3D12, L"D3D12SerializeRootSignature {}", ErrorBlob->GetBufferPointer());
            CheckFailed(Hr);
        }

        CheckFailed(
            Device->CreateRootSignature(0,
                                        SerializedRootSig->GetBufferPointer(),
                                        SerializedRootSig->GetBufferSize(),
                                        IID_PPV_ARGS(RootSignature.GetAddressOf())
            )
        );

        VsByteCode = D3D12Helper::CompileShader((Paths::GetShaderDir() / L"Test.hlsl").wstring(),
                                                nullptr,
                                                "VS",
                                                "vs_5_0"
        );
        PsByteCode = D3D12Helper::CompileShader((Paths::GetShaderDir() / L"Test.hlsl").wstring(),
                                                nullptr,
                                                "PS",
                                                "ps_5_0"
        );

        InputLayout = {
            {
                "POSITION",
                0,
                DXGI_FORMAT_R32G32B32_FLOAT,
                0,
                0,
                D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,
                0
            },
            {
                "COLOR",
                0,
                DXGI_FORMAT_R32G32B32A32_FLOAT,
                0,
                12,
                D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA,
                0
            }
        };

        if constexpr (bMeshGeometry)
        {
            MeshGeometry = std::make_unique<GeometryMeshTest1>();
            MeshGeometry->KeyName = L"BoxGeo";

            MeshGeometry->VertexBufferGpu = CreateVertexTest(
                Device.Get(),
                CommandList.Get(),
                MeshGeometry->UploadVertexBufferGpu,
                MeshGeometry->VertexBufferSize
            );

            uint32_t IndexCount = 0;
            MeshGeometry->IndexBufferGpu = CreateIndexBufferTest(
                Device.Get(),
                CommandList.Get(),
                MeshGeometry->UploadIndexBufferGpu,
                MeshGeometry->IndexBufferSize,
                IndexCount
            );

            SubMeshGeometry SubMesh = {
                .IndexCount = IndexCount,
                .BaseIndexPos = 0,
                .BaseVertexPos = 0,
            };

            MeshGeometry->DrawSubMeshArgs.emplace(L"Box", SubMesh);
        }
        else
        {
            auto VertexData = GetVertexTest();
            auto IndexData = GetIndexTest();

            VerBuffer = std::make_shared<CD3D12Buffer>(ERHIBufferFlag::VertexBuffer,
                                                       static_cast<uint32_t>(VertexData.size_bytes()),
                                                       static_cast<uint32_t>(sizeof(VertexTest1))
            );
            VerBuffer->Init(this);
            {
                auto DataCopy = VerBuffer->Lock(EResourceLockMode::WriteOnly);
                memcpy(DataCopy, VertexData.data(), VertexData.size_bytes());
                // NOLINT(bugprone-undefined-memory-manipulation)
                VerBuffer->Unlock();
            }

            IndexBuffer = std::make_shared<CD3D12Buffer>(
                ERHIBufferFlag::IndexBuffer,
                static_cast<uint32_t>(IndexData.size_bytes()),
                static_cast<uint32_t>(sizeof(uint16_t))
            );

            IndexBuffer->Init(this);
            {
                auto Data = IndexBuffer->Lock(EResourceLockMode::WriteOnly);
                memcpy(Data, IndexData.data(), IndexData.size_bytes());
                IndexBuffer->Unlock();
            }
        }

        // 构建 PSO
        D3D12_GRAPHICS_PIPELINE_STATE_DESC PosDesc;
        memset(&PosDesc, 0, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
        PosDesc.InputLayout = {
            .pInputElementDescs = InputLayout.data(),
            .NumElements = static_cast<UINT>(InputLayout.size()),
        };
        PosDesc.pRootSignature = this->RootSignature.Get();
        PosDesc.VS = {
            .pShaderBytecode = VsByteCode->GetBufferPointer(),
            .BytecodeLength = VsByteCode->GetBufferSize(),
        };
        PosDesc.PS = {
            .pShaderBytecode = PsByteCode->GetBufferPointer(),
            .BytecodeLength = PsByteCode->GetBufferSize(),
        };

        // 光删化状态
        PosDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
        PosDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
        PosDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
        PosDesc.SampleMask = UINT_MAX;
        PosDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
        PosDesc.NumRenderTargets = 1;
        // TODO 应该使用一个变量来统一， BackBuffer 的格式
        PosDesc.RTVFormats[0] = DXGI_FORMAT_B8G8R8A8_UNORM;
        PosDesc.SampleDesc.Count = 1;
        PosDesc.SampleDesc.Quality = 0;
        // TODO 应该统一
        PosDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
        CheckFailed(Device->CreateGraphicsPipelineState(&PosDesc, IID_PPV_ARGS(PipelineStateComPtr.GetAddressOf())));

        CheckFailed(CommandList->Close());
        ID3D12CommandList* CmdsLists[] = { CommandList.Get() };
        CommandQueue->ExecuteCommandLists(_countof(CmdsLists), CmdsLists);

        this->FlushCommandQueue();
    }

    void D3D12RHIImp::Shutdown()
    {
    }

    IDXGIFactory1ComPtr D3D12RHIImp::GetDXHIFactory() const
    {
        return DXGIFactory;
    }

    IDXGIAdapter1ComPtr D3D12RHIImp::GetDXHIAdapter() const
    {
        return DXGIAdapter;
    }

    ID3D12DeviceComPtr D3D12RHIImp::GetDevice() const
    {
        return Device;
    }

    TRHIResourcePtr<RHIViewport> D3D12RHIImp::CreateRHIViewport(const RHIViewportCreateArg& Arg)
    {
        // auto Viewport = std::make_shared<D3D12RHIViewport>();

        // Viewport->Init(D3D12RHIViewportInitArg{
        //         .D3D12Interface = this,
        //         .Hwnd = static_cast<HWND>(Arg.WindowHandle),
        //         .WindowW = Arg.WindowW,
        //         .WindowH = Arg.WindowH,
        //     }
        // );

        // return Viewport;
        return nullptr;
    }

    ID3D12CommandQueueComPtr D3D12RHIImp::GetCommandQueue() const
    {
        return CommandQueue;
    }

    ID3D12GraphicsCommandListComPtr D3D12RHIImp::GetGraphicsCommandList() const
    {
        return CommandList;
    }

    IRHIResourceFactory* D3D12RHIImp::GetResourceFactory() const
    {
        return const_cast<D3D12RHIImp*>(this);
    }

    #if 0
      void D3D12RHIImp::BeginDraw(const RHIBeginDrawArgs& Args)
    {
        // 球坐标转直角坐标
        float X = Radius * sinf(Phi) * cosf(Theta);
        float Y = Radius * sinf(Phi) * sinf(Theta);
        float Z = Radius * cosf(Phi);

        this->ViewMat = Matrix4X4::MakeLookAtMat(Vector4D(X, Y, Z, 1.f),
                                                 Vector4D(),
                                                 Vector4D(0, 0.f, 1.f, 0.f)
        );

        this->ProjMat = Matrix4X4::MakeProjMat(0.25f * PI, 1920.f / 1080.f, 1.f, 1000.f);

        auto WorldViewProj = WorldMat * ViewMat * ProjMat;

        ObjectConstants ObjectConstant = {
            .WorldViewProj = WorldViewProj.Transpose()
        };

        if (bUsedCbBuffer)
        {
            auto LockData = ObjectCbBuffer->Lock();
            memcpy(LockData, &ObjectConstant, sizeof(ObjectConstant));
            ObjectCbBuffer->Unlock();
        }
        else
        {
            ObjectCb->SetData(0, ObjectConstant);
        }

        ///
        auto Viewport = std::dynamic_pointer_cast<D3D12RHIViewport>(Args.Viewport);

        // 必须保证 GPU 关联的命令执行完毕时，才能释放
        CheckFailed(this->CommandAllocator->Reset());

        // 加入队列后，即可 Reset
        CheckFailed(this->CommandList->Reset(CommandAllocator.Get(), PipelineStateComPtr.Get()));

        // 资源的不同状态 https://learn.microsoft.com/zh-cn/windows/win32/api/d3d12/ne-d3d12-d3d12_resource_states
        {
            auto BackBufferBarriers = CD3DX12_RESOURCE_BARRIER::Transition(
                Viewport->GetCurrentBackBufferResource().Get(),
                D3D12_RESOURCE_STATE_PRESENT,
                D3D12_RESOURCE_STATE_RENDER_TARGET
            );

            CommandList->ResourceBarrier(1, &BackBufferBarriers);
        }

        {
            auto BackBufferBarriers = CD3DX12_RESOURCE_BARRIER::Transition(
                Viewport->GetDepthStencilBufferResource().Get(),
                D3D12_RESOURCE_STATE_PRESENT,
                D3D12_RESOURCE_STATE_DEPTH_WRITE
            );

            CommandList->ResourceBarrier(1, &BackBufferBarriers);
        }

        auto [W, H] = Viewport->GetViewPortSize();
        D3D12_VIEWPORT Vp{
            .TopLeftX = 0.f,
            .TopLeftY = 0.f,
            .Width = static_cast<FLOAT>(W),
            .Height = static_cast<FLOAT>(H),
            .MinDepth = 0.f,
            .MaxDepth = 1.f,
        };

        CommandList->RSSetViewports(1, &Vp);
        D3D12_RECT Rect = {
            0, 0, W, H,
        };

        CommandList->RSSetScissorRects(1, &Rect);
        // 清除颜色
        auto ClearColor = ColorLinear{ 0, 1.f, 0, 1.f };
        CommandList->ClearRenderTargetView(Viewport->GetCurrentBackBufferView(),
                                           ClearColor.GetPtr(),
                                           0,
                                           nullptr
        );

        CommandList->ClearDepthStencilView(Viewport->GetDepthStencilView(),
                                           D3D12_CLEAR_FLAG_DEPTH |
                                           D3D12_CLEAR_FLAG_STENCIL,
                                           1.f, 0, 0,
                                           nullptr
        );

        auto RtDesc = Viewport->GetCurrentBackBufferView();
        auto DepthDesc = Viewport->GetDepthStencilView();
        CommandList->OMSetRenderTargets(1, &RtDesc, true, &DepthDesc);


        ID3D12DescriptorHeap* DescriptorHeaps[] = { this->CbDescriptorHeap.Get() };
        CommandList->SetDescriptorHeaps(_countof(DescriptorHeaps), DescriptorHeaps);

        CommandList->SetGraphicsRootSignature(this->RootSignature.Get());

        if constexpr (bMeshGeometry)
        {
            auto BufferView = MeshGeometry->GetVertexBufferView();
            auto IndexView = MeshGeometry->GetIndexBufferView();
            CommandList->IASetVertexBuffers(0, 1, &BufferView);
            CommandList->IASetIndexBuffer(&IndexView);
        }
        else
        {
            D3D12_VERTEX_BUFFER_VIEW VertexBufferView
            {
                VerBuffer->GetGpuAddress(),
                VerBuffer->GetSize(),
                VerBuffer->GetStride()
            };
            CommandList->IASetVertexBuffers(0, 1, &VertexBufferView);

            D3D12_INDEX_BUFFER_VIEW IndexBufferView
            {
                IndexBuffer->GetGpuAddress(),
                (IndexBuffer->GetSize()),
                DXGI_FORMAT_R16_UINT,
            };

            CommandList->IASetIndexBuffer(&IndexBufferView);
        }


        CommandList->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

        CommandList->SetGraphicsRootDescriptorTable(0, this->CbDescriptorHeap->GetGPUDescriptorHandleForHeapStart());

        if constexpr (bMeshGeometry)
        {
            CommandList->DrawIndexedInstanced(MeshGeometry->DrawSubMeshArgs[L"Box"].IndexCount, 1, 0, 0, 0);
        }
        else
        {
            CommandList->DrawIndexedInstanced(36, 1, 0, 0, 0);
        }

        {
            auto BackBufferBarriers = CD3DX12_RESOURCE_BARRIER::Transition(
                Viewport->GetCurrentBackBufferResource().Get(),
                D3D12_RESOURCE_STATE_RENDER_TARGET,
                D3D12_RESOURCE_STATE_PRESENT
            );

            CommandList->ResourceBarrier(1, &BackBufferBarriers);
        }

        {
            auto DepthStencilBufferBarriers = CD3DX12_RESOURCE_BARRIER::Transition(
                Viewport->GetDepthStencilBufferResource().Get(),
                D3D12_RESOURCE_STATE_DEPTH_WRITE,
                D3D12_RESOURCE_STATE_PRESENT
            );

            CommandList->ResourceBarrier(1, &DepthStencilBufferBarriers);
        }

        CheckFailed(CommandList->Close());

        ID3D12CommandList* ExecCmdList[]{ CommandList.Get() };
        std::span Wap{ ExecCmdList };

        CommandQueue->ExecuteCommandLists(Wap.size(), ExecCmdList);

        Viewport->Present();

        this->FlushCommandQueue();
    }
    #endif


    TRHIResourcePtr<CRHIShader> D3D12RHIImp::CreateShader(const RHIShaderCreateArgs& Args)
    {
        std::shared_ptr<CRHIShader> Ret;
        switch (Args.ShaderType)
        {
        case EShaderType::Vertex:
            Ret = std::make_shared<D3D12VertexShader>();
            break;
        case EShaderType::Pixel:
            Ret = std::make_shared<D3D12PixelShader>();
            break;
        case EShaderType::None:
            assert(false);
        }

        if (Ret)
        {
            Ret->Init(Args.ShaderPath, Args.MainName);
        }
        return Ret;
    }

    TRHIResourcePtr<CRHITexture> D3D12RHIImp::CreateTexture(const RHITextureCreateArgs& Args)
    {
        //auto Texture = std::make_shared<CD3D12Texture>();
        //
        //D3D12_RESOURCE_DESC Desc{ };
        //
        //if (Args.Dimension == ETextureDimension::Texture2D)
        //{
        //    Desc = CD3DX12_RESOURCE_DESC::Tex2D(
        //        ConvertFormPixelFormat(Args.Format),
        //        Args.Size.X,
        //        Args.Size.Y,
        //        1,
        //        static_cast<uint16_t>(Args.MipsCount),
        //        Args.SampleCount,
        //        0
        //    );
        //
        //    if (EnumHasAnyFlags(Args.Flags, ETextureFlags::RenderTarget))
        //    {
        //        Desc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;
        //    }
        //}
        //else if (Args.Dimension == ETextureDimension::Texture3D)
        //{
        //    assert(false);
        //    // Desc = CD3DX12_RESOURCE_DESC::Tex3D(ConvertFormPixelFormat(Args.Format), Args.Size.X, Args.Size.Y);
        //}
        //else
        //{
        //    assert(false);
        //}
        //
        //
        //const auto HeapProDesc = CD3DX12_HEAP_PROPERTIES{ D3D12_HEAP_TYPE_DEFAULT };
        //
        //ID3D12ResourceComPtr OutResourcePtr;
        //
        //CheckFailed(Device->CreateCommittedResource(&HeapProDesc,
        //                                            D3D12_HEAP_FLAG_NONE,
        //                                            &Desc,
        //                                            D3D12_RESOURCE_STATE_COMMON,
        //                                            nullptr,
        //                                            IID_PPV_ARGS(OutResourcePtr.GetAddressOf())
        //    )
        //);
        //
        //Texture->SetD3d12Resource(OutResourcePtr);
        //
        //return Texture;

        return nullptr;
    }


    TRHIResourcePtr<CRHIBuffer> D3D12RHIImp::CreateBuffer(const RHICreateBufferArgs& Args)
    {
        auto Buffer = MakeRHIResource<CD3D12Buffer>([this, &Args]()-> CD3D12Buffer* {
                const auto Obj = new CD3D12Buffer(Args.InBufferFlag, Args.InBufferSize, Args.InStride);
                Obj->Init(this);
                return Obj;
            }
        );

        return Buffer;
    }

    void D3D12RHIImp::FlushCommandQueue()
    {
        // 等待刷新
        CurrentFence++;
        CheckFailed(CommandQueue->Signal(Fence.Get(), CurrentFence));
        auto CurrentValue = Fence->GetCompletedValue();
        if (CurrentValue < CurrentFence)
        {
            auto EventHandle = CreateEventEx(nullptr, nullptr, CREATE_EVENT_MANUAL_RESET, EVENT_ALL_ACCESS);
            CheckFailed(Fence->SetEventOnCompletion(CurrentFence, EventHandle));

            WaitForSingleObject(EventHandle, INFINITE);
            CloseHandle(EventHandle);
        }
    }

    // ------------------------------------------------------------------------

    void CD3D12RHIInstance::Init()
    {
        RootDevice = std::make_shared<CD3D12Device>();
        RootDevice->Init();

        // 必须初始化完成，才可以赋值
        Instance = this;
    }

    void CD3D12RHIInstance::Shutdown()
    {
    }

    IRHIResourceFactory* CD3D12RHIInstance::GetResourceFactory() const
    {
        return RootDevice->GetResourceManager();
    }

    IRHICommandContext* CD3D12RHIInstance::GetCommandContext()
    {
        return RootDevice->GetRHIContext();
    }

    void CD3D12RHIInstance::AddPendingDeleteResource(std::shared_ptr<CD3D12Resource>&& Resource)
    {
        assert(Resource.use_count() == 0);
        PendingDeleteResourcesArray.emplace_back(std::move(Resource));
    }

    void CD3D12RHIInstance::OnCleanupPreFrame()
    {
        // 释放待删除的资源
        PendingDeleteResourcesArray.clear();
    }

    CD3D12RHIInstance* GetD3D12RHIInstance()
    {
        return CD3D12RHIInstance::Instance;
    }
}
