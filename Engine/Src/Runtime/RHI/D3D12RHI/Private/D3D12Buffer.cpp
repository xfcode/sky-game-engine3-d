﻿#include "D3D12Buffer.h"

#include "D3D12ResourceManager.h"
#include "D3D12RHIContext.h"

namespace SkyGameEngine3d
{
    CD3D12Buffer::~CD3D12Buffer()
    {
        if (UploadBuffer)
        {
            UploadBuffer->Unmap(0, nullptr);
        }
    }

    void CD3D12Buffer::Init(CRHIInstance* RHI)
    {
        Interface = CastToD3D12Interface(RHI);
        Device = Interface->GetDevice();

        BufferSize = CalConstantBufferByteSize(BufferSize);

        {
            const auto HeapDesc = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);

            const auto ResDesc = CD3DX12_RESOURCE_DESC::Buffer(BufferSize);

            CheckFailed(Device->CreateCommittedResource(
                    &HeapDesc,
                    D3D12_HEAP_FLAG_NONE,
                    &ResDesc,
                    D3D12_RESOURCE_STATE_GENERIC_READ,
                    nullptr,
                    IID_PPV_ARGS(UploadBuffer.GetAddressOf())
                )
            );
        }

        if (EnumHasAnyFlags(BufferFlag, ERHIBufferFlag::Dynamic))
        {
            ResourceComPtr = UploadBuffer;
        }
        else
        {
            const auto HeapDesc = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
            const auto ResDesc = CD3DX12_RESOURCE_DESC::Buffer(BufferSize);
            CheckFailed(Device->CreateCommittedResource(
                    &HeapDesc,
                    D3D12_HEAP_FLAG_NONE,
                    &ResDesc,
                    D3D12_RESOURCE_STATE_COMMON,
                    nullptr,
                    IID_PPV_ARGS(DefaultBuffer.GetAddressOf())
                )
            );

            ResourceComPtr = DefaultBuffer;
        }
    }

    void* CD3D12Buffer::Lock(EResourceLockMode LockMode)
    {
        if (!BufferMapDataPtr)
        {
            assert(UploadBuffer);
            CheckFailed(UploadBuffer->Map(0, nullptr, reinterpret_cast<void**>(&BufferMapDataPtr)));
        }

        return BufferMapDataPtr;
    }

    void CD3D12Buffer::Unlock()
    {
        if (!EnumHasAnyFlags(BufferFlag, ERHIBufferFlag::Dynamic))
        {
            assert(UploadBuffer);
            const auto CommandList = Interface->GetGraphicsCommandList();

            {
                const auto ResTrans = CD3DX12_RESOURCE_BARRIER::Transition(
                    DefaultBuffer.Get(),
                    D3D12_RESOURCE_STATE_COMMON,
                    D3D12_RESOURCE_STATE_COPY_DEST
                );

                CommandList->ResourceBarrier(1, &ResTrans);
            }

            CommandList->CopyBufferRegion(DefaultBuffer.Get(), 0, UploadBuffer.Get(), 0, BufferSize);

            {
                const auto ResTrans = CD3DX12_RESOURCE_BARRIER::Transition(
                    DefaultBuffer.Get(),
                    D3D12_RESOURCE_STATE_COPY_DEST,
                    D3D12_RESOURCE_STATE_COMMON
                );

                CommandList->ResourceBarrier(1, &ResTrans);
            }
        }
    }

    ID3D12Resource* CD3D12Buffer::GetResource() const
    {
        return ResourceComPtr.Get();
    }

    D3D12_GPU_VIRTUAL_ADDRESS CD3D12Buffer::GetGpuAddress() const
    {
        return GetResource()->GetGPUVirtualAddress();
    }

    CD3D12BufferV2::~CD3D12BufferV2()
    {
    }

    void* CD3D12BufferV2::Lock(EResourceLockMode LockMode)
    {
        assert(!LockState.bIsLocked);
        LockState.bIsLocked = true;

        if (EnumHasAnyFlags(BufferFlag, ERHIBufferFlag::Dynamic))
        {
            assert(ResourceHandle.IsValid());
            if (!ResourceHandle.IsValid())
            {
                return nullptr;
            }

            return ResourceHandle.Map();
        }
        else
        {
            const auto ResourceManager = OwnerDevice->GetResourceManager();
            LockState.UploadResourceHandle = ResourceManager->AllocateDefaultHeapBuffer(BufferSize);
            if (LockState.UploadResourceHandle.IsValid())
            {
                return LockState.UploadResourceHandle.Map();
            }
            else
            {
                return nullptr;
            }
        }
    }

    void CD3D12BufferV2::Unlock()
    {
        assert(LockState.bIsLocked);
        LockState.bIsLocked = false;

        if (EnumHasAnyFlags(BufferFlag, ERHIBufferFlag::Dynamic))
        {
            assert(ResourceHandle.IsValid());
            if (!ResourceHandle.IsValid())
            {
                return;
            }

            ResourceHandle.Unmap();
        }
        else
        {
            // 把数据从上传堆Copy到Buffer中
            assert(LockState.UploadResourceHandle.IsValid());
            if (!LockState.UploadResourceHandle.IsValid())
            {
                return;
            }

            LockState.UploadResourceHandle.Unmap();

            const auto Context = OwnerDevice->GetRHIContext();

            const auto CommandList = Context->GetGraphicsCommandList();
            {
                const auto ResTrans = CD3DX12_RESOURCE_BARRIER::Transition(
                    ResourceHandle.GetResource().Get(),
                    D3D12_RESOURCE_STATE_COMMON,
                    D3D12_RESOURCE_STATE_COPY_DEST
                );

                CommandList->ResourceBarrier(1, &ResTrans);
            }

            Context->GetGraphicsCommandList()->CopyBufferRegion(ResourceHandle.GetResource().Get(),
                                                                ResourceHandle.GetResourceOffset(),
                                                                LockState.UploadResourceHandle.GetResource().Get(),
                                                                LockState.UploadResourceHandle.GetResourceOffset(),
                                                                BufferSize
            );

            {
                const auto ResTrans = CD3DX12_RESOURCE_BARRIER::Transition(
                    ResourceHandle.GetResource().Get(),
                    D3D12_RESOURCE_STATE_COPY_DEST,
                    D3D12_RESOURCE_STATE_COMMON
                );

                CommandList->ResourceBarrier(1, &ResTrans);
            }
        }
    }

    CD3D12BufferV2::CD3D12BufferV2(CD3D12Device* Device, ERHIBufferFlag InBufferFlag, const uint32_t InBufferSize,
                                   const uint32_t InStride)
        : CD3D12DeviceChildObject{ Device }
        , CRHIBuffer{ InBufferFlag, InBufferSize, InStride }
    {
    }

    void CD3D12BufferV2::Init()
    {
        const auto ResourceManager = OwnerDevice->GetResourceManager();

        if (EnumHasAnyFlags(BufferFlag, ERHIBufferFlag::Dynamic))
        {
            ResourceHandle = ResourceManager->AllocateUploadHeapBuffer(BufferSize);
        }
        else
        {
            ResourceHandle = ResourceManager->AllocateDefaultHeapBuffer(BufferSize);
        }

        assert(ResourceHandle.IsValid());
    }
}
