﻿#include "D3D12Texture.h"

#include "D3D12Buffer.h"
#include "D3D12Device.h"
#include "D3D12ResourceManager.h"
#include "Misc/Memory.h"

namespace SkyGameEngine3d
{
    bool CD3D12Texture::Init()
    {
        const auto Device = OwnerDevice->GetDevice();

        const auto HeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);

        D3D12_RESOURCE_DESC TextureDesc{ };
        if (Desc.Dimension == ETextureDimension::Texture2D)
        {
            TextureDesc = CD3DX12_RESOURCE_DESC::Tex2D(ConvertFormPixelFormat(Desc.Format),
                                                       Desc.Size.X,
                                                       Desc.Size.Y,
                                                       1,
                                                       Desc.MipsCount
            );
        }
        else if (Desc.Dimension == ETextureDimension::Texture3D)
        {
            // TextureDesc = CD3DX12_RESOURCE_DESC::Tex3D()
            assert(false);
        }
        else
        {
            assert(false);
        }
        
        ID3D12ResourceComPtr ResourceComPtr;

        CheckFailed(Device->CreateCommittedResource(
                &HeapProperties,
                D3D12_HEAP_FLAG_NONE,
                &TextureDesc,
                D3D12_RESOURCE_STATE_COMMON,
                nullptr,
                IID_PPV_ARGS(ResourceComPtr.GetAddressOf())
            )
        );

        const auto& AllocateResourceInfo = Device->GetResourceAllocationInfo(0, 1, &TextureDesc);

        auto D3DResource = std::make_shared<CD3D12Resource>(ResourceComPtr, D3D12_HEAP_TYPE_DEFAULT, TextureDesc);

        ResourceHandle = CD3D12ResourceHandle{ };
        assert(AllocateResourceInfo.SizeInBytes < static_cast<uint64_t>(std::numeric_limits<uint32_t>::max())
        ); //确保可以安全的装换

        ResourceHandle.SetStandAlone(std::move(D3DResource), static_cast<uint32_t>(AllocateResourceInfo.SizeInBytes));

        return false;
    }

    CD3D12Texture::~CD3D12Texture()
    {
    }

    void* CD3D12Texture::Lock(uint32_t MipIndex, const uint32_t ArrayIndex, uint32_t& RowInBytes,
                              EResourceLockMode LockMode)
    {
        const uint32_t SubResource = GetSubResourceIndex(*this, MipIndex, ArrayIndex);
        const auto Device = OwnerDevice->GetDevice();

        const auto& Resource = ResourceHandle.GetResource();

        // https://learn.microsoft.com/en-us/windows/win32/direct3d12/upload-and-readback-of-texture-data

        uint64_t SubResourceSizeInBytes = 0;
        uint64_t RowSizeInBytes = 0;
        const auto& MyDesc = Resource->GetDesc();
        Device->GetCopyableFootprints(&MyDesc,
                                      SubResource,
                                      1,
                                      ResourceHandle.GetResourceOffset(),
                                      nullptr,
                                      nullptr,
                                      &RowSizeInBytes,
                                      &SubResourceSizeInBytes
        );

        // 行间距必须 D3D12_TEXTURE_DATA_PITCH_ALIGNMENT 对齐
        RowInBytes = MemoryLib::AlignUp(static_cast<uint32_t>(RowSizeInBytes), D3D12_TEXTURE_DATA_PITCH_ALIGNMENT);
        
        const auto ResourceManager = OwnerDevice->GetResourceManager();
        CLockData LockData;

        // 资源表面大小必须是 D3D12_TEXTURE_DATA_PLACEMENT_ALIGNMENT 对齐的
        LockData.UploadResourceHandle = ResourceManager->FastAllocateUploadHeapBuffer(
            static_cast<uint32_t>(SubResourceSizeInBytes),
            D3D12_TEXTURE_DATA_PLACEMENT_ALIGNMENT
        );
        
        void* MapAddr = LockData.UploadResourceHandle.Map();
        LockDataMap.emplace(SubResource, std::move(LockData));
        return MapAddr;
    }

    void CD3D12Texture::UnLock(const uint32_t MipIndex, const uint32_t ArrayIndex)
    {
        const uint32_t SubResource = GetSubResourceIndex(*this, MipIndex, ArrayIndex);
        const auto Device = OwnerDevice->GetDevice();

        const auto FindLockData = LockDataMap.find(SubResource);
        assert(FindLockData != LockDataMap.end());
        if (FindLockData == LockDataMap.end())
        {
            return;
        }

        const auto& LockData = FindLockData->second;
        LockData.UploadResourceHandle.Unmap();

        const auto CommandList = OwnerDevice->GetGraphicsCommandListComPtr();

        const D3D12_TEXTURE_COPY_LOCATION Des = CD3DX12_TEXTURE_COPY_LOCATION(
            ResourceHandle.GetResource().Get(),
            SubResource
        );

        const auto& ResourceDesc = ResourceHandle.GetResourceDesc();
        D3D12_PLACED_SUBRESOURCE_FOOTPRINT PlacedSubresourceFootprint{ };
        Device->GetCopyableFootprints(&ResourceDesc,
                                      SubResource,
                                      1,
                                      ResourceHandle.GetResourceOffset(),
                                      &PlacedSubresourceFootprint,
                                      nullptr,
                                      nullptr,
                                      nullptr
        );


        const D3D12_TEXTURE_COPY_LOCATION Src
        {
            .pResource = LockData.UploadResourceHandle.GetResource().Get(),
            .Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT,
            .PlacedFootprint = PlacedSubresourceFootprint,
        };

        CD3DX12_RESOURCE_BARRIER::Transition(ResourceHandle.GetResource().Get(),
                                             D3D12_RESOURCE_STATE_COMMON,
                                             D3D12_RESOURCE_STATE_COPY_DEST
        );

        CommandList->CopyTextureRegion(&Des, 0, 0, 0, &Src, nullptr);

        CD3DX12_RESOURCE_BARRIER::Transition(ResourceHandle.GetResource().Get(),
                                             D3D12_RESOURCE_STATE_COPY_DEST,
                                             D3D12_RESOURCE_STATE_COMMON
        );

        LockDataMap.erase(SubResource);
    }
}
