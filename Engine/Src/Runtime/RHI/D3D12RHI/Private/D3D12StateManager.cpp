﻿#include "D3D12StateManager.h"

namespace SkyGameEngine3d
{
    /**
    * \brief 计算相关的状态
    */
    struct CD3D12ComputeState
    {
    };


    /**
    * \brief 图形相关的状态
    */
    struct CD3D12GraphicsState
    {
        D3D12_INDEX_BUFFER_VIEW IndexBufferView;
    };


    /**
    * \brief 计算和图形都使用的状态
    */
    struct CD3D12CommonState
    {
    };

    CD3D12StateManager::CD3D12StateManager(CD3D12Device* Device)
        : CD3D12DeviceChildObject{ Device }
    {
        using namespace std;

        ComputeState = make_shared<CD3D12ComputeState>();
        GraphicsState = make_shared<CD3D12GraphicsState>();
        CommonState = make_shared<CD3D12CommonState>();
    }

    CD3D12StateManager::~CD3D12StateManager()
    {
    }

    void CD3D12StateManager::SetIndexBuffer(const CD3D12ResourceHandle& ResourceHandle, DXGI_FORMAT Format)
    {
        if (!ResourceHandle.IsValid())
        {
            return;
        }

        auto& IndexViewCacheRef = GraphicsState->IndexBufferView;
        const auto GpuLocation = ResourceHandle.GetGpuAddress();
        const auto Size = ResourceHandle.GetResourceSizeInBytes();

        if (IndexViewCacheRef.BufferLocation != GpuLocation
            || IndexViewCacheRef.Format != Format
            || IndexViewCacheRef.SizeInBytes != Size)
        {
            IndexViewCacheRef.BufferLocation = GpuLocation;
            IndexViewCacheRef.SizeInBytes = Size;
            IndexViewCacheRef.Format = Format;

            const auto CmdList = OwnerDevice->GetGraphicsCommandListComPtr();
            CmdList->IASetIndexBuffer(&IndexViewCacheRef);
        }
    }
}
