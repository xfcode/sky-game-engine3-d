#include "ModuleManager/ModuleInterface.h"
#include "public/RHI.h"
#include "D3D12RHI.h"


using namespace SkyGameEngine3d;


class D3D12RHIModuleInterface : public IRHIModule
{
public:
    virtual ~D3D12RHIModuleInterface() override
    {
    }

    virtual CRHIInstance* CreateRHI() override
    {
        return new CD3D12RHIInstance;
    }

    virtual void DestroyRHI(CRHIInstance* RHI) override
    {
        delete RHI;
    }

private:
    virtual ModuleSpec GetSpec() override
    {
        return { };
    }

    virtual void OnLoad() override
    {
        SetupPixeFormatInfo([](auto){});
    }

    virtual void OnUnload() override
    {
    }
};


EXPORT_MODULE(D3D12RHI, D3D12RHIModuleInterface);
