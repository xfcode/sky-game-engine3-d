#pragma once
#include <functional>
#include <memory>
#include <optional>
#include "UIModuleDef.h"
#include "UIRenderTypes.h"
#include "Input/InputTypes.h"
#include "Math/Vector.h"


namespace SkyGameEngine3d
{
    class AppRenderPipeline;
    class CWidget;
    class Window;

    struct NativeWindowFeature
    {
        std::function<void(const std::wstring& Text)> SetText;
        std::function<void*()> GetHandle;
        std::function<void(const Vector2D& Size)> SetSize;
    };

    struct NativeWindowDesc
    {
        Vector2D Size;
    };

    struct NativeWindowFactory
    {
        std::function<NativeWindowFeature(const NativeWindowDesc& Desc)> CreateWindow;
    };

    template <typename WidgetType>
        requires std::is_base_of_v<CWidget, WidgetType>
    std::shared_ptr<WidgetType> CreateWidget()
    {
        return std::make_shared<WidgetType>();
    }
    
    class UI_API UISystem
    {
    public:
        void Draw(double Dt);

        InputReply HandleMouseButtonDown(const InputPointerEvent& Event)
        {
            return InputReply::UnHandle();
        }

        void SetNativeWindowFactory(const std::optional<NativeWindowFactory>& Factory)
        {
            NativeWindowFactory = Factory;
        }

        void Init();

        void SetAppRenderPipeline(std::shared_ptr<AppRenderPipeline> InAppRenderPipeline);


        std::shared_ptr<Window> AddWindow(std::shared_ptr<Window> InWindow, bool IsShowWindow = true);

    private:
        
        std::vector<std::shared_ptr<Window>> WindowList;

        std::optional<NativeWindowFactory> NativeWindowFactory;

        std::shared_ptr<AppRenderPipeline> RenderPipeline;

    private:
        std::shared_ptr<TUIVertexBuffer<UIVertex>> VertexBuffer;
        std::shared_ptr<CUIIndexBuffer> IndexBuffer;
        std::shared_ptr<class CTexture2D> TestTexture;
    };
}
