#pragma once
#include <memory>

#include "UIModuleDef.h"
#include "ModuleManager/ModuleInterface.h"
#include "ModuleManager/ModuleManager.h"

namespace SkyGameEngine3d
{
    class UISystem;
}

class UI_API UIModuleInterface : public IModuleInterface
{
public:
    static UIModuleInterface* Get();

    virtual SkyGameEngine3d::UISystem* GetUIInstance();

    virtual SkyGameEngine3d::UISystem* CreateUIInstance();

protected:
    virtual ModuleSpec GetSpec() override;

    virtual void OnLoad() override;

    virtual void OnUnload() override;

private:
    std::shared_ptr<SkyGameEngine3d::UISystem> MyInstance;
};
