﻿#pragma once

#include <list>
#include <memory>

#include "UIModuleDef.h"
#include "Misc/UtilityTypes.h"
#include "Input/InputTypes.h"


#define DEF_WIDGET_INIT_PROPERTY(Type, Name)\
                Type Name;\
                InitArgs& SetTest(Type InValue) \
                {\
                    Name = InValue; \
                    return *this; \
                }


namespace SkyGameEngine3d
{
    struct WidgetDrawArg
    {
    };

    class UI_API CWidget
        : public CNonCopyable,
          public std::enable_shared_from_this<CWidget>
    {
    public:
        explicit CWidget(std::weak_ptr<CWidget> InParent);

        virtual ~CWidget();

        struct InitArgs
        {
            DEF_WIDGET_INIT_PROPERTY(int32_t, Test)
        };

        void Construct(const InitArgs& Args);


        virtual InputReply OnMouseButtonDown() { return InputReply::UnHandle(); }

        virtual void OnDraw(const WidgetDrawArg& DrawArg)
        {
            for (const auto& ChildWidget : ChildWidgets)
            {
                ChildWidget->OnDraw(DrawArg);
            }
        }

    private:
        std::list<std::shared_ptr<CWidget>> ChildWidgets;

        std::weak_ptr<CWidget> Parent;
    };


    namespace Imp
    {
        template <typename WidgetType>
        struct WidgetBuilder
        {
            explicit WidgetBuilder(std::weak_ptr<CWidget> Parent)
                : Widget(std::make_shared<WidgetType>(Parent))
            {
            }

            std::shared_ptr<WidgetType> operator<<(const typename WidgetType::InitArgs& Args)
            {
                Widget->Construct(Args);
                return Widget;
            }

        private:
            std::shared_ptr<WidgetType> Widget;
        };

        template <typename WidgetType>
        WidgetBuilder<WidgetType> CreateWidget(std::weak_ptr<CWidget> Parent)
        {
            return WidgetBuilder<WidgetType>(Parent);
        }
    }
}

#define CREATE_WIDGET(WidgetType, Parent) SkyGameEngine3d::Imp::CreateWidget<WidgetType>(Parent) << typename WidgetType::InitArgs{}

inline void Test()
{
    CREATE_WIDGET(SkyGameEngine3d::CWidget, {})
        .SetTest(333)
        .SetTest(222);
}
