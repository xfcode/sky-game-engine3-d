﻿#pragma once

#include "UIModuleDef.h"
#include "CWidget.h"

#include <utility>
#include "Interface/IViewport.h"

namespace SkyGameEngine3d
{
    class UI_API CViewportWidget
        : public CWidget
    {
    public:
        using CWidget::CWidget;

        struct InitArgs
        {
        };

        void Construct(const InitArgs& Args);

        void SetViewportInputListener(std::weak_ptr<class IViewportInputListener> InViewportInputListener)
        {
            ViewportInputListener = std::move(InViewportInputListener);
        }

        void SetViewportFrame(std::weak_ptr<class IViewportFrame> InViewportFrame)
        {
            ViewportFrame = std::move(InViewportFrame);
        }

    protected:
        virtual void OnDraw(const WidgetDrawArg& DrawArg) override
        {
            
        }

        virtual InputReply OnMouseButtonDown() override
        {
            if (const auto Listener = ViewportInputListener.lock())
            {
                return Listener->OnMouseButtonDown({ });
            }
            return InputReply::UnHandle();
        }

    protected:
        std::weak_ptr<class IViewportInputListener> ViewportInputListener;

        std::weak_ptr<class IViewportFrame> ViewportFrame;
    };
}
