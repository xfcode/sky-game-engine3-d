﻿#pragma once

#include "UIModuleDef.h"
#include "UISystem.h"
#include "CWidget.h"
#include "Interface/CWindowBase.h"

namespace SkyGameEngine3d
{
    class CTexture2D;
}

namespace SkyGameEngine3d
{
    class UI_API Window
        : public CWidget,
          public CWindowBase
    {
    public:
        Window(std::weak_ptr<CWidget> Parent = { });

        struct InitArgs
        {
        };

        void Construct(const InitArgs& Args);

        virtual void* GetNativeHandle() const override;

        virtual Vector2D GetSize() const override;

        virtual void SetSceneRenderTarget(std::weak_ptr<ISceneRenderTarget> RenderTarget) override;

    public:
        void SetNativeFeature(const SkyGameEngine3d::NativeWindowFeature& Feature);

        void SetTitle(const std::wstring& Text) const;

        void Show();

        void SetSize(const Vector2D& InSize);
        
        virtual std::shared_ptr<RHIViewport> GetRHIViewport() override;

        bool IsShow() const
        {
            return bIsShow;
        }
        
    protected:
        void* NativeHandle = nullptr;

        NativeWindowFeature NativeWindow;

    private:
        Vector2D WindowSize = Vector2D(720, 460);

        std::shared_ptr<class CViewportWidget> ViewportWidgetPtr;

        std::shared_ptr<CTexture2D> Texture2D;

        bool bIsShow = false;

    private:
        
        std::shared_ptr<RHIViewport> RHIViewportPtr;
    };

    using WindowPtr = std::shared_ptr<Window>;
}
