#pragma once

#include "HAL/PlatformDefine.h"

#ifdef UI
    #define UI_API DLL_EXPORT
#else
    #define UI_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameUI = L"UI";
