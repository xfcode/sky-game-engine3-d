// ReSharper disable CppMemberFunctionMayBeStatic
#include "UISystem.h"

#include <utility>

#include "UILog.h"
#include "Log/Log.h"
#include "Widget/Window.h"
#include "AppRenderPipeline.h"
#include "EngineBase.h"
#include "RHICoreModuleInterface.h"
#include "Misc/Paths.h"
#include "Texture/Public/Texture.h"
#include "UIRenderTypes.h"

namespace SkyGameEngine3d
{
    void UISystem::Draw(double Dt)
    {
        auto RHIInstance = GetRHIInstance();
        if (!RHIInstance)
        {
            return;
        }

        auto CommandContext = RHIInstance->GetCommandContext();
        if (!CommandContext)
        {
            return;
        }

        if (WindowList.empty())
        {
            return;
        }

        auto CurrentWindows = WindowList[0];
        if (!CurrentWindows->IsShow())
        {
            return;
        }

        const auto ResFactory = RHIInstance->GetResourceFactory();

        if (!TestTexture)
        {
            const auto TextureSys = GetEngine()->GetSubSystem<CTextureSubsystem>();
            TestTexture = TextureSys->CreateTexture(Paths::GetEngineResourceDir() / L"TestImage_16.dds");
        }

        CommandContext->BeginDrawViewport(CurrentWindows->GetRHIViewport());

        CommandContext->CreateUIRootSignature();

        const std::shared_ptr<RHIVertexDeclare> Declare = std::make_shared<RHIVertexDeclare>();
        Declare->AddDeclare(VERTEX_ELEMENT_DECLARE(0, 0, UIVertex, Pos));
        Declare->AddDeclare(VERTEX_ELEMENT_DECLARE(1, 0, UIVertex, UV));

        const RHIShaderCreateArgs VsShaderCreateArgs =
        {
            .ShaderType = EShaderType::Vertex,
            .ShaderPath = L"",
            .MainName = "VS"
        };

        const RHIShaderCreateArgs PsShaderCreateArgs =
        {
            .ShaderType = EShaderType::Pixel,
            .ShaderPath = L"",
            .MainName = "PS"
        };

        RHIGraphicsPipelineState PipelineState;
        PipelineState.ShaderStateInput = RHIGraphicsShaderStateInput
        {
            .VertexDeclare = Declare,
            .VS = ResFactory->CreateShader(VsShaderCreateArgs),
            .PS = ResFactory->CreateShader(PsShaderCreateArgs),
        };

        CommandContext->SetGraphicsPipelineState(PipelineState);

        CommandContext->SetVertexBuffer(VertexBuffer->GetRHIBuffer());

        const auto Size = CurrentWindows->GetSize();
        CommandContext->SetViewport(0.f, 0.f, 0.f, Size.X, Size.Y, 0.f);
        
        const CRHIDrawIndexInstancedArgs DrawArgs
        {
            IndexBuffer->GetRHIBuffer(),
            6
        };

        CommandContext->DrawIndexInstanced(DrawArgs);

        CommandContext->EndDrawViewport();
    }

    void UISystem::Init()
    {
        auto RHIInstance = GetRHIInstance();
        if (!RHIInstance)
        {
            return;
        }

        auto ResFactory = RHIInstance->GetResourceFactory();

        std::vector<UIVertex> VertexArray;
        std::vector<uint16_t> IndexArray;

        VertexArray.push_back(UIVertex::Make(Vector2D{ 0, 0 }, Vector2D{ 0, 0 }));
        VertexArray.push_back(UIVertex::Make(Vector2D{ 1, 0 }, Vector2D{ 1, 0 }));
        VertexArray.push_back(UIVertex::Make(Vector2D{ 1, 1 }, Vector2D{ 1, 0 }));
        VertexArray.push_back(UIVertex::Make(Vector2D{ 0, 1 }, Vector2D{ 0, 1 }));

        IndexArray.push_back(0);
        IndexArray.push_back(1);
        IndexArray.push_back(2);

        IndexArray.push_back(0);
        IndexArray.push_back(2);
        IndexArray.push_back(3);

        VertexBuffer = std::make_shared<TUIVertexBuffer<UIVertex>>();
        VertexBuffer->InitResource(ResFactory);
        VertexBuffer->FillBuffer(VertexArray.data(), VertexArray.size()); // NOLINT(clang-diagnostic-shorten-64-to-32)

        IndexBuffer = std::make_shared<CUIIndexBuffer>();
        IndexBuffer->InitResource(ResFactory);
        IndexBuffer->FillBuffer(IndexArray.data(), IndexArray.size());
    }

    void UISystem::SetAppRenderPipeline(std::shared_ptr<AppRenderPipeline> InAppRenderPipeline)
    {
        RenderPipeline = std::move(InAppRenderPipeline);
    }

    WindowPtr UISystem::AddWindow(WindowPtr InWindow, bool IsShowWindow)
    {
        WindowList.push_back(InWindow);

        if (!NativeWindowFactory.has_value())
        {
            SE_ERROR(GLogUI, L"NativeWindowFactory No Value");
            return InWindow;
        }

        const NativeWindowDesc Dec = {
            .Size = InWindow->GetSize()
        };

        const auto NativeWindows = NativeWindowFactory.value().CreateWindow(Dec);

        InWindow->SetNativeFeature(NativeWindows);

        if (IsShowWindow)
        {
            InWindow->Show();
        }

        return InWindow;
    }
}
