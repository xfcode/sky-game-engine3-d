#include "UIModuleInterface.h"
#include "UISystem.h"

ModuleSpec UIModuleInterface::GetSpec()
{
    return ModuleSpec{ };
}


UIModuleInterface* UIModuleInterface::Get()
{
    return ModuleManager::Get().GetModule<UIModuleInterface>(GModuleNameUI);
}

SkyGameEngine3d::UISystem* UIModuleInterface::GetUIInstance()
{
    return MyInstance.get();
}

SkyGameEngine3d::UISystem* UIModuleInterface::CreateUIInstance()
{
    MyInstance = std::make_shared<SkyGameEngine3d::UISystem>();
    return MyInstance.get();
}

void UIModuleInterface::OnLoad()
{
}

void UIModuleInterface::OnUnload()
{
}

EXPORT_MODULE(UI, UIModuleInterface);
