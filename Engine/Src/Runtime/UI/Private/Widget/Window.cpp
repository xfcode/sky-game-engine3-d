﻿#include "Widget/Window.h"

#include "EngineBase.h"
#include "Misc/Paths.h"
#include "Texture/Public/Texture.h"
#include "Widget/CViewportWidget.h"

namespace SkyGameEngine3d
{
    Window::Window(std::weak_ptr<CWidget> Parent)
        : CWidget{ Parent }
    {
    }

    void Window::Construct(const InitArgs& Args)
    {
        ViewportWidgetPtr = CREATE_WIDGET(CViewportWidget, weak_from_this());
    }

    void* Window::GetNativeHandle() const
    {
        return NativeHandle;
    }

    Vector2D Window::GetSize() const
    {
        return WindowSize;
    }

    void Window::SetSceneRenderTarget(std::weak_ptr<ISceneRenderTarget> RenderTarget)
    {
    }

    void Window::SetNativeFeature(const SkyGameEngine3d::NativeWindowFeature& Feature)
    {
        NativeWindow = Feature;
        if (NativeWindow.GetHandle)
        {
            NativeHandle = NativeWindow.GetHandle();
        }
    }

    void Window::SetTitle(const std::wstring& Text) const
    {
        if (NativeWindow.SetText)
        {
            NativeWindow.SetText(Text);
        }
    }

    void Window::Show()
    {
        if (bIsShow)
        {
            return;
        }

        bIsShow = true;
        
        RHIViewportPtr = this->CreateRHIViewport();
    }

    void Window::SetSize(const Vector2D& InSize)
    {
        if (WindowSize == InSize)
        {
            return;
        }

        WindowSize = InSize;

        if (NativeWindow.SetSize)
        {
            NativeWindow.SetSize(WindowSize);
        }
    }

    std::shared_ptr<RHIViewport> Window::GetRHIViewport()
    {
        return RHIViewportPtr;
    }
}
