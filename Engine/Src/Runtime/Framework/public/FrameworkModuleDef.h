#pragma once

#include "HAL/PlatformDefine.h"

#ifdef FRAMEWORK
    #define FRAMEWORK_API DLL_EXPORT
#else
    #define FRAMEWORK_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameFramework = L"Framework";
