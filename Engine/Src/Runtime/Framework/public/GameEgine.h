#pragma once

#include "EngineBase.h"
#include "FrameworkModuleDef.h"

namespace SkyGameEngine3d
{
    class GameEngine : public EngineBase
    {
    public:
        virtual void Init() override;

    private:
        std::shared_ptr<class Window> CreateMainWindow() const;

    private:
        std::shared_ptr<class Window> MainWindow;
        std::shared_ptr<class SceneViewport> GameSceneViewport;
        std::shared_ptr<class CViewportWidget> ViewPortWidgetIns;
    };
}
