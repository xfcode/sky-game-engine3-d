#include "Application.h"
#include "GameEgine.h"
#include "Viewport.h"
#include "Widget/CViewportWidget.h"
#include "Widget/CWidget.h"
#include "Widget/Window.h"

[[maybe_unused]] static TStaticLazyExec RegEngineFactory(
    []
    {
        RegisterEngineFactory([](Application* App)
        {
            return std::make_shared<GameEngine>();
        });   
    }
);

namespace SkyGameEngine3d
{
    void GameEngine::Init()
    {
        EngineBase::Init();

        MainWindow = this->CreateMainWindow();

        GameSceneViewport = std::make_shared<SceneViewport>();

        ViewPortWidgetIns = CREATE_WIDGET(CViewportWidget, MainWindow);
        ViewPortWidgetIns->SetViewportInputListener(GameSceneViewport);
        ViewPortWidgetIns->SetViewportFrame(GameSceneViewport);

        this->RegisterSceneViewport(L"GameSceneViewport", GameSceneViewport);
    }

    std::shared_ptr<Window> GameEngine::CreateMainWindow() const
    {
        auto WindowIns = CREATE_WIDGET(Window, {});
        
        const auto UIInstance = Application::Get()->GetUIInstanceChecked();
        UIInstance->AddWindow(WindowIns);

        WindowIns->SetSize({ 1920, 1080 });


        return WindowIns;
    }
}
