#include "Application.h"

#include <filesystem>

#include "CoreDefine.h"
#include "EngineBase.h"
#include "LogDef.h"
#include "UISystem.h"
#include "UIModuleInterface.h"
#include "Log/Log.h"
#include "Log/LogOutputDevice.h"
#include "public/RHI.h"
#include "public/RHICoreModuleInterface.h"

struct TestInput : public IApplicationInputHandler
{
    Vector2DInt LastMousePos;
    float Theta = 1.5f * PI;
    float Phi = PI / 4;
    float Radius = 5;

    virtual void OnMouseMove(const MouseEvent& Event) override
    {
        if (Event.MouseButton == EKeys::MouseButtonLeft)
        {
            float Dx = MathLib::AngleToRadian(0.25f * static_cast<float>(Event.Pos.X - LastMousePos.X));
            float Dy = MathLib::AngleToRadian(0.25f * static_cast<float>(Event.Pos.Y - LastMousePos.Y));
            Theta += Dx;
            Phi += Dy;

            Phi = std::clamp(Phi, 0.1f, PI - 0.1f);
            SE_LOG(GLogApp, L"Theta {}, Phi {}", Theta, Phi);
        }
        else if (Event.MouseButton == EKeys::MouseButtonRight)
        {
            float Dx = 0.005f * static_cast<float>(Event.Pos.X - LastMousePos.X);
            float Dy = 0.005f * static_cast<float>(Event.Pos.Y - LastMousePos.Y);
            Radius += Dx - Dy;
            Radius = std::clamp(Radius, 3.f, 15.f);
        }

        LastMousePos = Event.Pos;

        // GetRHIInstance()->TestSetValue(Theta, Phi, Radius);
    }

    virtual void OnMouseButtonDown(const MouseEvent& Event) override
    {
        LastMousePos = Event.Pos;
    }

    virtual void OnMouseButtonUp(const MouseEvent& Event) override
    {
    }
};

namespace 
{
    std::function<std::shared_ptr<EngineBase>(Application* App)> EngineFactory;
}


NativeWindowFeature Application::CreateNativeWindow(const NativeWindowDesc& Desc)
{
    return { };
}

int32_t Application::Run()
{
    using namespace SkyGameEngine3d;
    RegNamedThread(std::this_thread::get_id(), EThreadName::GameThread);

    auto& LogSubsystem = LogSubsystem::Get();
    LogSubsystem.Init_GameThread();

    this->InitPlatform();
    SE_LOG(GLogApp, L"InitPlatform");

    InitHRI();

    MyUIInstance = UIModuleInterface::Get()->CreateUIInstance();

    NativeWindowFactory Factory;
    Factory.CreateWindow = [this](const NativeWindowDesc& Desc) { return this->CreateNativeWindow(Desc); };

    MyUIInstance->SetNativeWindowFactory(Factory);
    MyUIInstance->Init();
    SE_LOG(GLogApp, L"init UI Instance");


    // TODO 这里后续会改为根据阶段加载
    ModuleManager::Get().GetModule(L"Framework");
    
    if (EngineFactory == nullptr)
    {
        SE_ERROR(GLogApp, L"EngineFactory Is null");
        return -1;
    }
    
    const auto Engine = EngineFactory(this);
    RegisterEngine(Engine);

    SE_LOG(GLogApp, L"InitEngine");

    // TODO
    this->SetInputHandler(std::make_shared<TestInput>());

    Engine->Init();

    const auto EngineClockIns = EngineClock::GetInstance();
    EngineClockIns->Start();

    while (!RequestQuit)
    {
        this->Loop();
    }

    LogSubsystem.Destroy_GameThread();
    return 0;
}

void Application::Loop()
{
    const auto EngineClockIns = EngineClock::GetInstance();
    const auto Engine = GetEngine();
    const auto RHIInstance = GetRHIInstance();

    const auto FrameDt = EngineClockIns->GetFrameDt();

    RHIInstance->CleanupPreFrame();

    this->TickForPlatform();

    Engine->Tick(FrameDt);

    MyUIInstance->Draw(FrameDt);

    EngineClockIns->Tick();
}

void RegisterEngineFactory(std::function<std::shared_ptr<EngineBase>(Application* App)> Factory)
{
    EngineFactory = Factory;
}
