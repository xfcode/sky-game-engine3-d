﻿#include "ApplicationForWindows.h"

#include <atlbase.h>
#include <atlwin.h>
#include <fstream>
#include <iostream>

#include "Application.h"

#ifdef CreateFile
#undef CreateFile
#endif

#ifdef CreateWindow
#undef CreateWindow
#endif

#include "Misc/FileHelper.h"
#include "Misc/Paths.h"

std::wstring ApplicationForWindows::WindowsClassName = L"SkyGameEngine";

static ApplicationForWindows* AppInstance = nullptr;

Application* Application::Get()
{
    return AppInstance;
}

static LRESULT WindowProc(
    HWND hWnd,
    UINT Msg,
    WPARAM wParam,
    LPARAM lParam
)
{
    if (Msg == WM_NCCREATE)
    {
        const auto* CreatePtr = reinterpret_cast<CREATESTRUCT*>(lParam);
        auto This = static_cast<ApplicationForWindows*>(CreatePtr->lpCreateParams);
        SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(This));
    }

    if (const auto This = reinterpret_cast<ApplicationForWindows*>(GetWindowLongPtr(hWnd, GWLP_USERDATA)))
    {
        if (This->HandleWindowMsg(hWnd, Msg, wParam, lParam))
        {
            return 0;
        }
    }

    return DefWindowProcW(hWnd, Msg, wParam, lParam);
}

void ApplicationForWindows::RegisterWindowClass() const
{
    WNDCLASSEXW WinClassInfo;
    WinClassInfo.cbSize = sizeof(WNDCLASSEX);
    WinClassInfo.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
    WinClassInfo.lpfnWndProc = WindowProc;
    WinClassInfo.cbClsExtra = 0;
    WinClassInfo.cbWndExtra = 0;
    WinClassInfo.hInstance = this->WindowsInstance;
    WinClassInfo.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    WinClassInfo.hCursor = LoadCursor(nullptr, IDC_ARROW);
    WinClassInfo.hbrBackground = static_cast<HBRUSH>(GetStockObject(NULL_BRUSH));
    WinClassInfo.lpszMenuName = nullptr;
    WinClassInfo.lpszClassName = WindowsClassName.c_str();
    if (!RegisterClassExW(&WinClassInfo))
    {
        throw LR"(注册窗口类失败)";
    }
}

ApplicationForWindows* ApplicationForWindows::Create(const AppCreateArg& Arg)
{
    auto App = new ApplicationForWindows;
    AppInstance = App;
    App->RegisterWindowClass();
    return App;
}

NativeWindowFeature ApplicationForWindows::CreateNativeWindow(const NativeWindowDesc& Desc)
{
    constexpr DWORD WindowExStyle = WS_EX_CLIENTEDGE;
    constexpr DWORD WindowStyle = WS_OVERLAPPEDWINDOW | WS_VISIBLE;

    auto Hwnd = CreateWindowExW(WindowExStyle,
                                WindowsClassName.c_str(),
                                L"SkyGameEngine",
                                WindowStyle,
                                0,
                                0,
                                Desc.Size.X,
                                Desc.Size.Y,
                                nullptr,
                                nullptr,
                                this->WindowsInstance,
                                this
    );

    NativeWindowFeature Windows;
    Windows.GetHandle = [Hwnd]() { return Hwnd; };
    Windows.SetText = [Hwnd](const auto& Text) { ::SetWindowTextW(Hwnd, Text.c_str()); };
    return Windows;
}

bool ApplicationForWindows::HandleWindowMsg(HWND HWnd, UINT Msg, WPARAM WParam, LPARAM LParam)
{
    if (Msg == WM_DESTROY)
    {
        this->RequestQuit = true;
        return true;
    }

    auto GetMouseKey = [](WPARAM WParam)
    {
        auto Key = EKeys::None;
        if (WParam & MK_LBUTTON)
        {
            Key = EKeys::MouseButtonLeft;
        }
        else if (WParam & MK_RBUTTON)
        {
            Key = EKeys::MouseButtonRight;
        }
        else if (WParam & MK_MBUTTON)
        {
            Key = EKeys::MouseButtonMiddle;
        }
        return Key;
    };

    switch (Msg)
    {
    case WM_MOUSEMOVE:
        {
            auto X = GET_X_LPARAM(LParam);
            auto Y = GET_Y_LPARAM(LParam);
            auto Key = GetMouseKey(WParam);
            ApplicationInputHandler->OnMouseMove(MouseEvent{ .Pos = { X, Y }, .MouseButton = Key });
            break;
        }
    case WM_RBUTTONDOWN:
    case WM_LBUTTONDOWN:
    case WM_MBUTTONDOWN:
        {
            auto X = GET_X_LPARAM(LParam);
            auto Y = GET_Y_LPARAM(LParam);
            auto Key = GetMouseKey(WParam);
            ApplicationInputHandler->OnMouseButtonDown(MouseEvent{ .Pos = { X, Y }, .MouseButton = Key });
            break;
        }
    case WM_RBUTTONUP:
    case WM_LBUTTONUP:
    case WM_MBUTTONUP:
        {
            auto X = GET_X_LPARAM(LParam);
            auto Y = GET_Y_LPARAM(LParam);
            auto Key = GetMouseKey(WParam);
            ApplicationInputHandler->OnMouseButtonUp(MouseEvent{ .Pos = { X, Y }, .MouseButton = Key });
            break;
        }
    case WM_ENTERSIZEMOVE: // 进入移动窗口的状态   https://learn.microsoft.com/en-us/windows/win32/winmsg/wm-entersizemove
    case WM_ACTIVATEAPP: //  https://learn.microsoft.com/en-us/windows/win32/winmsg/wm-activateapp
    case WM_EXITSIZEMOVE: // https://learn.microsoft.com/en-us/windows/win32/winmsg/wm-exitsizemove
    case WM_PAINT: // https://learn.microsoft.com/en-us/windows/win32/gdi/the-wm-paint-message
        {
        }
    }

    return false;
}

void ApplicationForWindows::TickForPlatform()
{
    MSG Msg{ };
    while (PeekMessageW(&Msg, nullptr, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }
}


namespace SkyGameEngine3d
{
    static const auto Zone = std::chrono::current_zone();

    struct DebugLogOutputDevice final : public ILogOutputDevice
    {
        virtual void OutputLog(ELogLevel LogLevel,
                               const TimePoint& Time, const LogCategory& Category, std::wstring_view LogStr) override
        {
            const auto Data = std::format(L"[{0:%F} {0:%T}]<{1}> {2}: {3}",
                                          std::chrono::zoned_time(Zone, Time),
                                          Category.GetName(),
                                          ToString(LogLevel),
                                          LogStr
            );

            OutputDebugStringW((Data + L"\n").c_str());
            std::wcout << Data << std::endl;
        }

        virtual void Init_GameThread() override
        {
        }

        virtual void Destroy_GameThread() override
        {
        }
    };

    struct FileLogOutputDevice final : public ILogOutputDevice
    {
        virtual void OutputLog(ELogLevel LogLevel,
                               const TimePoint& Time, const LogCategory& Category, std::wstring_view LogStr) override
        {
            const auto Data = std::format(L"[{0:%F} {0:%T}] {1}: {2}",
                                          std::chrono::zoned_time(Zone, Time),
                                          ToString(LogLevel),
                                          LogStr
            );
            Stream << Data << std::endl;
        }

        virtual void Init_GameThread() override
        {
            const auto FilePath = std::format(
                "{}/{:%Y.%m.%d-%H.%M}.log",
                Paths::GetLogDir().string(),
                std::chrono::zoned_time(Zone, std::chrono::system_clock::now())
            );
            FileHelper::TryCreateFile(FilePath);
            Stream.open(FilePath);
            assert(Stream.is_open());
        }

        virtual void Destroy_GameThread() override
        {
            Stream.close();
        }

        std::wofstream Stream;
    };
}

void ApplicationForWindows::InitPlatform()
{
    using namespace SkyGameEngine3d;
    auto& LogSys = LogSubsystem::Get();
    LogSys.RegisterOutputDevice(std::make_shared<DebugLogOutputDevice>());
    LogSys.RegisterOutputDevice(std::make_shared<FileLogOutputDevice>());
}
