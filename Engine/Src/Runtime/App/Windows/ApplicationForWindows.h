#pragma once

#include "string"
#include "Application.h"
#include "AppModuleDef.h"
#include "Log/LogOutputDevice.h"
#include "Windows.h"

class APP_API ApplicationForWindows : public Application
{
private:
    void RegisterWindowClass() const;

public:


public:
    static std::wstring WindowsClassName;

    struct AppCreateArg
    {
    };

    static ApplicationForWindows* Create(const AppCreateArg& Arg);

    virtual NativeWindowFeature CreateNativeWindow(const NativeWindowDesc& Desc) override;

    virtual void TickForPlatform() override;

    virtual void InitPlatform() override;

    bool HandleWindowMsg(HWND HWnd, UINT Msg, WPARAM WParam, LPARAM LParam);

protected:


private:
    HINSTANCE WindowsInstance;
};
