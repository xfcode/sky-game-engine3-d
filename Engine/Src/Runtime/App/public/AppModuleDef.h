#pragma once

#include "HAL/PlatformDefine.h"

#ifdef APP
    #define APP_API DLL_EXPORT
#else
    #define APP_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameApp = L"App";
