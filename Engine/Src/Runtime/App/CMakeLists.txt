# 模块名
set(MODULE_NAME App)

# 模块源文件
set(SRC_FILES Application.h Application.cpp Windows/ApplicationForWindows.cpp Windows/ApplicationForWindows.h LogDef.h)

# 模块依赖项 私有 仅供模块实现使用
set(DEPEND_MODULE_PRIVATE)

# 模块依赖项 公有 其他依赖当前模块的模块 也会依赖这些 公有 依赖项
set(DEPEND_MODULE_PUBLIC Core Engine RHICore UI)

# 包含的头文件，默认已经包含当前模块的文件
set(INCLUDE_DIR)

add_library(${MODULE_NAME} SHARED ${SRC_FILES} ${MODULE_NAME}ModuleDef.h)

build_module(
        MODULE_NAME ${MODULE_NAME}
        DEPEND_MODULE_PRIVATE ${DEPEND_MODULE_PRIVATE}
        DEPEND_MODULE_PUBLIC ${DEPEND_MODULE_PUBLIC}
        SRC_DIR ${SRC_FILES}
        INCLUDE_DIR ${INCLUDE_DIR}
)