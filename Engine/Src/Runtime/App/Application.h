﻿#pragma once
#include <memory>
#include "AppModuleDef.h"
#include "EngineBase.h"
#include "Math/Vector.h"
#include "UISystem.h"

namespace SkyGameEngine3d
{
    class UISystem;
    class Window;

    enum class EKeys
    {
        None,
        MouseButtonLeft,
        MouseButtonMiddle,
        MouseButtonRight,
    };

    struct MouseEvent
    {
        Vector2DInt Pos;
        EKeys MouseButton = EKeys::None;
    };

    class APP_API IApplicationInputHandler
    {
    public:
        virtual void OnMouseMove(const MouseEvent& Event)
        {
        }

        virtual void OnMouseButtonDown(const MouseEvent& Event)
        {
        }

        virtual void OnMouseButtonUp(const MouseEvent& Event)
        {
        }
    };
}

// TODO
using namespace SkyGameEngine3d;

class APP_API Application
{
public:
    //! 获取应用程序的实例
    //! \return
    static Application* Get();

    void SetInputHandler(std::shared_ptr<IApplicationInputHandler> Handler)
    {
        ApplicationInputHandler = Handler;
    }

    [[nodiscard]] UISystem* GetUIInstanceChecked() const
    {
        assert(MyUIInstance);
        return MyUIInstance;
    }

protected:
    virtual void TickForPlatform()
    {
    }

    virtual void InitPlatform()
    {
    }

    virtual NativeWindowFeature CreateNativeWindow(const NativeWindowDesc& Desc);

public:
    int32_t Run();

    void Loop();

protected:
    std::shared_ptr<IApplicationInputHandler> ApplicationInputHandler;

    UISystem* MyUIInstance = nullptr;

    bool RequestQuit = false;
};

APP_API void RegisterEngineFactory(std::function<std::shared_ptr<EngineBase>(Application* App)> Factory);