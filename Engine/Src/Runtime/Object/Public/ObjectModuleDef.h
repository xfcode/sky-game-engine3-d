#pragma once

#include "HAL/PlatformDefine.h"

#ifdef OBJECT
    #define OBJECT_API DLL_EXPORT
#else
    #define OBJECT_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameObject = L"Object";
