﻿#pragma once

#include <string>
#include <vector>

#include "ObjectModuleDef.h"

namespace SkyGameEngine3d
{
    struct ReflectField
    {
        ReflectField(std::wstring FieldName, const uint32_t Offset)
            : FieldName(std::move(FieldName))
            , Offset(Offset)
        {
        }

        std::wstring FieldName;

        uint32_t Offset = 0;
    };

    struct ReflectFieldBool : ReflectField
    {
        using ReflectField::ReflectField;
    };


    class ReflectClass
    {
    public:
        std::wstring GetClassName() const
        {
            return L"";
        }

        ReflectField* FindFieldByName(std::wstring_view FieldName)
        {
            return nullptr;
        }

        ReflectClass* SuperClass;

        std::vector<ReflectField*> Fields;
    };


    struct ReflectClassDefine
    {
        std::wstring ClassName;
        std::vector<ReflectField*> Fields;
    };


    class Test
    {
        int32_t Value = 0;
        bool ValueBool = false;
    };

    inline void Test()
    {
        auto Define = ReflectClassDefine
        {
            .ClassName = L"Test",
            .Fields = {
                new ReflectFieldBool{
                    L"Value", 0,
                },
                // new ReflectField{
                // 
                // },
            }

        };
    }
}
