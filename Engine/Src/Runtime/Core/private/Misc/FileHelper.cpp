﻿#include "Misc/FileHelper.h"
#include <stack>
#include <fstream>


namespace SkyGameEngine3d::FileHelper
{
    bool TryCreateFile(const std::filesystem::path& CreatePath)
    {
        std::filesystem::path TargetPath = CreatePath;
        std::filesystem::path CopyFilePath = "";

        if (!is_directory(TargetPath))
        {
            TargetPath = TargetPath.parent_path();
            CopyFilePath = TargetPath;
        }

        std::stack<std::filesystem::path> PathStack;
        while (!exists(TargetPath))
        {
            PathStack.push(TargetPath);
            TargetPath = TargetPath.parent_path();
        }

        while (!PathStack.empty())
        {
            create_directory(PathStack.top());
            PathStack.pop();
        }

        if (!is_empty(CopyFilePath))
        {
            std::fstream Stream;
            Stream.open(CreatePath.generic_wstring().c_str(), std::ios_base::in);
            Stream.close();
        }

        return true;
    }
}
