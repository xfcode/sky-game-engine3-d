#include "ModuleManager/ModuleManager.h"
#include "HAL/PlatformUtility.h"
#include "Log/Log.h"
#include "CoreDefine.h"
#include "Misc/Paths.h"
#include "ModuleManager/ModuleInterface.h"

IModuleInterface::~IModuleInterface()
{
}


IModuleInterface* ModuleManager::GetModuleImp(const std::wstring& ModuleName)
{
    if (auto F = ModuleMap.find(ModuleName); F != ModuleMap.end())
    {
        if (F->second)
        {
            return F->second;
        }
    }

    using namespace SkyGameEngine3d;

    void* Handle = PlatformUtility::LoadDll((Paths::GetBinaryDir() / (ModuleName + L".dll")).wstring());
    if (Handle)
    {
        using CreateModulePtr = IModuleInterface* (*)();
        auto CreateModuleFunc = static_cast<CreateModulePtr>(PlatformUtility::GetDllExport(Handle,
            L"CreateModule_" +
            ModuleName
        ));
        if (CreateModuleFunc)
        {
            auto ModuleInterface = CreateModuleFunc();

            if (ModuleInterface)
            {
                ModuleMap.emplace(ModuleName, ModuleInterface);
                ModuleInterface->OnLoad();
                SE_LOG(GLogCore, L"Load Module Succeed : {}", ModuleName);
                return ModuleInterface;
            }
            SE_WARRING(GLogCore, L"Load Module Fail : {}", ModuleName);
        }
    }
    return nullptr;
}

void ModuleManager::Init()
{
}

ModuleManager& ModuleManager::Get()
{
    static ModuleManager S;
    return S;
}
