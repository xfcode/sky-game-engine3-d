﻿#include "CoreDefine.h"
#include <map>

namespace SkyGameEngine3d
{
    static std::map<std::thread::id, EThreadName> GThreadMapName;

    void RegNamedThread(std::thread::id Id, EThreadName Name)
    {
        GThreadMapName.emplace(Id, Name);
    }

    EThreadName SkyGameEngine3d::GetCurrentThreadName()
    {
        auto Value = GThreadMapName.find(std::this_thread::get_id());
        if (Value != GThreadMapName.end())
        {
            return Value->second;
        }
        return EThreadName::Unknown;
    }
}
