#include "Math/Vector.h"

namespace SkyGameEngine3d
{
    Vector4D::Vector4D(FloatType InX, FloatType InY, FloatType InZ, FloatType InW)
        : Comps{ InX, InY, InZ, InW }
    {
    }

    Vector4D::Vector4D(const Vector3D& Vec, FloatType InW)
        : Comps{ Vec.X, Vec.Y, Vec.Z, InW }
    {
    }


    Vector3D::Vector3D(FloatType InX, FloatType InY, FloatType InZ)
        : Comps{ InX, InY, InZ }
    {
    }

    Vector3D::Vector3D(const Vector4D& Vec)
        : Comps{ Vec.X, Vec.Y, Vec.Z }
    {
    }

    Vector2D::Vector2D(FloatType InX, FloatType InY)
        : Comps{ InX, InY }
    {
    }
}
