#include "Math/Matrix.h"

#ifdef USED_DX_MATH

#include "PlatformMath/DXMath.h"

#endif

namespace SkyGameEngine3d
{
    Matrix4X4 Matrix4X4::CreateIdentity()
    {
        Matrix4X4 Ret(EMatrixConstructorType::InitZero);
        Ret.M[0][0] = 1;
        Ret.M[1][1] = 1;
        Ret.M[2][2] = 1;
        Ret.M[3][3] = 1;
        return Ret;
    }


    const Matrix4X4 Matrix4X4::Identity = CreateIdentity();

    Matrix4X4::Matrix4X4(EMatrixConstructorType Type)
    {
        switch (Type)
        {
        case EMatrixConstructorType::InitZero:
            memset(this, 0, sizeof(Matrix4X4));
            break;
        case EMatrixConstructorType::Identity:
            memcpy(this, &Matrix4X4::Identity, sizeof(Matrix4X4));
            break;
        }
    }

    Matrix4X4::Matrix4X4(const Vector3D& InX, const Vector3D& InY, const Vector3D& InZ, const Vector3D& InW)
    {
        // @formatter:off
        M[0][0] = InX.X; M[0][1] = InX.Y; M[0][2] = InX.Z; M[0][3] = 0.f;
        M[1][0] = InY.X; M[1][1] = InY.Y; M[1][2] = InY.Z; M[1][3] = 0.f;
        M[2][0] = InZ.X; M[2][1] = InZ.Y; M[2][2] = InZ.Z; M[2][3] = 0.f;
        M[3][0] = InW.X; M[3][1] = InW.Y; M[3][2] = InW.Z; M[3][3] = 0.f;
        // @formatter:on
    }

    Matrix4X4::Matrix4X4(const Matrix4X4& Other)
    {
        memcpy(this, &Other, sizeof(Matrix4X4));
    }

    Matrix4X4 Matrix4X4::operator*(const Matrix4X4& Other) const
    {
        Matrix4X4 Ret(EMatrixConstructorType::NoInit);
        Private::MultMatrix(*this, Other, Ret);
        return Ret;
    }

    Matrix4X4& Matrix4X4::operator*=(const Matrix4X4& Other)
    {
        Private::MultMatrix(*this, Other, *this);
        return *this;
    }

    Matrix4X4 Matrix4X4::operator*(FloatType Scalar) const
    {
        Matrix4X4 Ret(*this);
        return Ret *= Scalar;
    }

    Matrix4X4& Matrix4X4::operator*=(FloatType Scalar)
    {
        for (int Index = 0; Index < 4; ++Index)
        {
            M[Index][0] *= Scalar;
            M[Index][1] *= Scalar;
            M[Index][2] *= Scalar;
            M[Index][3] *= Scalar;
        }
        return *this;
    }

    Vector3D Matrix4X4::TransformVector(const Vector3D& Vector) const
    {
        return Vector3D(TransformVector(Vector4D(Vector, 1.f)));
    }

    Vector4D Matrix4X4::TransformVector(const Vector4D& Vector) const
    {
        Vector4D Ret(EVectorConstructorType::NoInit);
        Private::TransformVector(Vector, *this, Ret);
        return Ret;
    }

    Matrix4X4 Matrix4X4::Inverse() const
    {
        Matrix4X4 Ret(EMatrixConstructorType::NoInit);
        Private::InverseMatrix(*this, Ret);
        return Ret;
    }

    bool Matrix4X4::IsNearly(const Matrix4X4& Other, FloatType Tolerance) const
    {
        if (!GetCompVector<0>().IsNearly(Other.GetCompVector<0>()))
        {
            return false;
        }
        if (!GetCompVector<1>().IsNearly(Other.GetCompVector<1>()))
        {
            return false;
        }
        if (!GetCompVector<2>().IsNearly(Other.GetCompVector<2>()))
        {
            return false;
        }
        if (!GetCompVector<3>().IsNearly(Other.GetCompVector<3>()))
        {
            return false;
        }
        return true;
    }

    Matrix4X4 Matrix4X4::MakeLookAtMat(const Vector4D& Pos, const Vector4D& Target, const Vector4D& Up)
    {
        Matrix4X4 Ret(EMatrixConstructorType::NoInit);
        Private::LookAtMat(Pos, Target, Up, Ret);
        return Ret;
    }

    Matrix4X4 Matrix4X4::MakeProjMat(float Fov, float Aspect, float Near, float Far)
    {
        // Matrix4X4 Ret(EMatrixConstructorType::InitZero);

        // float    SinFov = sin( 0.5f * Fov);
        // float    CosFov  = cos(0.5f * Fov);

        // float Height = CosFov / SinFov;
        // float Width = Height / Aspect;
        // float fRange = Far / (Far - Near);

        //Ret.M[0][0] = Width;
        //Ret.M[1][1] = Height;

        ////Ret.M[0][0] = Height;
        ////Ret.M[1][1] = Aspect * Height;

        // Ret.M[2][2] = fRange;
        // Ret.M[2][3] = 1.0f;

        // Ret.M[3][2] = -fRange * Near;

        Matrix4X4 Ret(EMatrixConstructorType::NoInit);
        Private::ProjMat(Fov, Aspect, Near, Far, Ret);
        return Ret;
    }

    Matrix4X4 Matrix4X4::Transpose() const
    {
        Matrix4X4 Ret(EMatrixConstructorType::NoInit);
        Private::TransposeMatrix(*this, Ret);
        return Ret;
    }


    Matrix3X3::Matrix3X3(const EMatrixConstructorType Type)
    {
        switch (Type)
        {
        case EMatrixConstructorType::InitZero:
            memset(this, 0, sizeof(Matrix3X3));
            break;
        case EMatrixConstructorType::Identity:
            memcpy(this, &Matrix4X4::Identity, sizeof(Matrix3X3));
            break;
        }
    }

    Matrix3X3::Matrix3X3(FloatType UniformScale, const Vector2D& Translation)
    {
        *this = Identity;
        M[0][0] = UniformScale;
        M[1][1] = UniformScale;

        M[2][0] = Translation.X;
        M[2][1] = Translation.Y;
    }

    Matrix3X3::Matrix3X3(const Vector2D& Scale, const Vector2D& Translation)
    {
        *this = Identity;
        M[0][0] = Scale.X;
        M[1][1] = Scale.Y;

        M[2][0] = Translation.X;
        M[2][1] = Translation.Y;
    }

    Vector2D Matrix3X3::TransformVector(const Vector2D& InVector) const
    {
        const FloatType X = InVector.X * M[0][0] + InVector.Y * M[1][0] + M[2][0];
        const FloatType Y = InVector.X * M[0][1] + InVector.Y * M[1][1] + M[2][1];
        return { X, Y };
    }

    Vector2D Matrix3X3::TransformPoint(const Vector2D& InPoint) const
    {
        return { InPoint.X + M[2][0], InPoint.Y + M[2][1] };
    }

    Matrix3X3 Matrix3X3::CreateIdentity()
    {
        Matrix3X3 Ret(EMatrixConstructorType::InitZero);
        Ret.M[0][0] = 1;
        Ret.M[1][1] = 1;
        Ret.M[2][2] = 1;
        return Ret;
    }

    const Matrix3X3 Matrix3X3::Identity = Matrix3X3::CreateIdentity();
}
