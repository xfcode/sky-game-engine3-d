﻿#include "Math/SimpleShape.h"

namespace SkyGameEngine3d
{
    Box3D::Box3D()
        : Min()
        , Max()
    {
    }

    Box3D::Box3D(const Vector3D& InMin, const Vector3D& InMax)
        : Min(InMin)
        , Max(InMax)
    {
    }

    void Box3D::Add(const Vector3D& Point)
    {
        Point.ForeachComp([this](const auto& Value, uint8_t Index)
            {
                if (Value < Min.Comps[Index])
                {
                    Min.Comps[Index] = Value;
                }
                else if (Value > Max.Comps[Index])
                {
                    Max.Comps[Index] = Value;
                }
            }
        );
    }

    void Box3D::Add(const Box3D& Point)
    {
        this->Add(Point.Min);
        this->Add(Point.Max);
    }

    bool Box3D::IsEmpty() const
    {
        return Min.X >= Max.X || Min.Y >= Max.Y || Min.Z >= Max.Z;
    }

    bool Box3D::Contains(const Vector3D& Point) const
    {
        uint8_t Index = 0;
        for (const auto Comp : Point.Comps)
        {
            if (Comp > Max.Comps[Index] || Comp < Min.Comps[Index])
            {
                return false;
            }
            ++Index;
        }
        return true;
    }

    bool Box3D::IntersectBox(const Box3D& Other, Box3D* OutIntersectBox) const
    {
        if (Min.X > Other.Max.X || Min.Y > Max.Y || Min.Z > Max.Z)
        {
            return false;
        }

        if (Max.X < Other.Min.X || Max.Y < Other.Min.Y || Max.Z < Other.Min.Z)
        {
            return false;
        }

        if (OutIntersectBox)
        {
            OutIntersectBox->Min.ForeachComp([&](Vector3D::DataType& ValueRef, uint8_t Index)
                {
                    ValueRef = std::max(Min.Comps[Index], Other.Min.Comps[Index]);
                }
            );

            OutIntersectBox->Max.ForeachComp([&](Vector3D::DataType& ValueRef, uint8_t Index)
                {
                    ValueRef = std::min(Max.Comps[Index], Other.Max.Comps[Index]);
                }
            );
        }

        return true;
    }
}
