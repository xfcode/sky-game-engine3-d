#include "Math/MathLib.h"
#include "cmath"
#include "Math/Vector.h"

namespace SkyGameEngine3d
{
    FloatType MathLib::Sqrt(FloatType Value)
    {
        return std::sqrt(Value);
    }

    bool MathLib::IsEq(FloatType A, FloatType B, FloatType Tolerance)
    {
        return Abs(A - B) <= Tolerance;
    }

    FloatType MathLib::Abs(FloatType A)
    {
        return std::abs(A);
    }

    Vector3D MathLib::CrossProduct(const Vector3D& A, const Vector3D& B)
    {
        return {
            A.Y * B.Z - A.Z * B.Y,
            A.Z * B.X - A.X * B.Z,
            A.X * B.Y - A.Y * B.X
        };
    }

    float MathLib::RadianToAngle(float Radian)
    {
        constexpr float Value = 180 / PI;
        return Radian * Value;
    }

    float MathLib::AngleToRadian(float Angle)
    {
        constexpr float Value = PI / 180;
        return Angle * Value;
    }
}
