﻿#pragma once

#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include "Math/Matrix.h"

namespace SkyGameEngine3d::Private
{
    inline DirectX::XMMATRIX LoadMat(const Matrix4X4& Mat)
    {
        return XMLoadFloat4x4A((const DirectX::XMFLOAT4X4A*)(&Mat));
    }

    inline void StoreMat(const DirectX::XMMATRIX& InMat, Matrix4X4& Out)
    {
        XMStoreFloat4x4A((DirectX::XMFLOAT4X4A*)&Out, InMat);
    }

    inline DirectX::XMVECTOR LoadVector(const Vector4D& Vector)
    {
        return XMLoadFloat4A((const DirectX::XMFLOAT4A*)(&Vector));
    }

    inline void StoreVector(const DirectX::XMVECTOR& InVector, Vector4D& Out)
    {
        XMStoreFloat4A((DirectX::XMFLOAT4A*)&Out, InVector);
    }

    inline void MultMatrix(const Matrix4X4& InA, const Matrix4X4& InB, Matrix4X4& Out)
    {
        using namespace DirectX;
        XMMATRIX XMatrix1 = LoadMat(InA);
        XMMATRIX XMatrix2 = LoadMat(InB);
        XMMATRIX XMatrixRet = XMMatrixMultiply(XMatrix1, XMatrix2);
        XMStoreFloat4x4A((XMFLOAT4X4A*)(&Out), XMatrixRet);
    }

    inline void TransformVector(const Vector4D& InVector, const Matrix4X4& InMat, Vector4D& Out)
    {
        using namespace DirectX;
        XMVECTOR XVec = LoadVector(InVector);
        XMMATRIX XMat = LoadMat(InMat);
        XMVECTOR Ret = XMVector4Transform(XVec, XMat);
        StoreVector(Ret, Out);
    }

    // 逆矩阵
    inline void InverseMatrix(const Matrix4X4& Mat, Matrix4X4& Out)
    {
        using namespace DirectX;
        auto XMat = LoadMat(Mat);
        auto Ret = XMMatrixInverse(nullptr, XMat);
        StoreMat(Ret, Out);
    }

    // 转置矩阵
    inline void TransposeMatrix(const Matrix4X4& Mat, Matrix4X4& Out)
    {
        using namespace DirectX;
        auto XMat = LoadMat(Mat);
        auto Ret = XMMatrixTranspose(XMat);
        StoreMat(Ret, Out);
    }

    inline void LookAtMat(const Vector4D& Pos, const Vector4D& Target, const Vector4D& Up, Matrix4X4& Out)
    {
        using namespace DirectX;
        XMVECTOR XPos = LoadVector(Pos);
        XMVECTOR XTarget = LoadVector(Target);
        XMVECTOR XUp = LoadVector(Up);
        XMMATRIX Ret = XMMatrixLookAtLH(XPos, XTarget, XUp);
        StoreMat(Ret, Out);
    }

    inline void ProjMat(float Fov, float Aspect, float Near, float Far, Matrix4X4& Out)
    {
        using namespace DirectX;
        auto Ret = XMMatrixPerspectiveFovLH(Fov, Aspect, Near, Far);
        StoreMat(Ret, Out);
    }
}
