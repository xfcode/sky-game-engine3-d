#include "HAL/Windows/WindowsStringUtility.h"
#include "CoreDefine.h"
#include <Windows.h>
#include <cassert>

namespace SkyGameEngine3d::Private
{
    std::wstring WindowsStringUtility::Utf8ToWChar(std::string_view Value)
    {
        // https://learn.microsoft.com/en-us/windows/win32/api/stringapiset/nf-stringapiset-multibytetowidechar
        std::wstring Ret;
        auto Len = MultiByteToWideChar(CP_UTF8, 0, Value.data(), Value.size(), nullptr, 0);
        if (Len > 0)
        {
            Ret.resize(Len);
            Len = MultiByteToWideChar(CP_UTF8, 0, Value.data(), Value.size(), Ret.data(), Ret.size());
            assert(Len > 0);
        }
        return Ret;
    }

    std::string WindowsStringUtility::WCharToAnsi(std::wstring_view Value)
    {
        std::string Ret;

        auto Len = WideCharToMultiByte(CP_ACP,
                                       0,
                                       Value.data(),
                                       -1,
                                       nullptr,
                                       0,
                                       nullptr,
                                       nullptr
        );
        if (Len > 0)
        {
            Ret.resize(Len);
            Len = WideCharToMultiByte(CP_ACP,
                                      0,
                                      Value.data(),
                                      -1,
                                      Ret.data(),
                                      Ret.size(),
                                      nullptr,
                                      nullptr
            );
            if (Len == 0)
            {
                auto Error = GetLastError();
                SE_ERROR(GLogCore, L"WCharToAnsi Error {}", Error);
            }
            assert(Len > 0);
        }
        return Ret;
    }
}
