#include "HAL/Windows/WindowsPlatformUtility.h"
#include <Windows.h>
#include "Misc/StringUtility.h"
#include "Log/Log.h"
#include "CoreDefine.h"

namespace SkyGameEngine3d::Private
{
    void* WindowsPlatformUtility::LoadDll(std::wstring_view DllPath)
    {
        return LoadLibraryW(DllPath.data());
    }

    void WindowsPlatformUtility::UnloadDll(void* DllHandle)
    {
        if (DllHandle)
        {
            FreeLibrary(static_cast<HMODULE>(DllHandle));
        }
        else
        {
            SE_WARRING(GLogCore, L"WindowsPlatformUtility::UnloadDll Args Is Null");
        }
    }

    void* WindowsPlatformUtility::GetDllExport(void* DllHandle, std::wstring_view ExportSymbolName)
    {
        auto ProcName = WCharToAnsi(ExportSymbolName);
        auto Func = GetProcAddress(static_cast<HMODULE>(DllHandle), ProcName.c_str());
        return (void*)Func;
    }
}
