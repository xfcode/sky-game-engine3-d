#include "Log/LogOutputDevice.h"
#include "CoreDefine.h"

namespace SkyGameEngine3d
{
    namespace Ranges = std::ranges;

    int32_t LogSubsystem::RegisterOutputDevice(std::shared_ptr<ILogOutputDevice> Device)
    {
        assert(IsInGameThread() && "必须在游戏线程RegisterOutputDevice");
        if (Ranges::find(DeviceList, Device, &DeviceInfo::Device) != DeviceList.end())
        {
            return -1;
        }
        auto NewId = AllocId++;
        while (DeviceLockFlag.test_and_set())
        {
        }
        DeviceList.emplace_back(DeviceInfo{ Device, NewId });
        Device->Init_GameThread();
        DeviceLockFlag.clear();
        return NewId;
    }

    void LogSubsystem::UnregisterOutputDevice(int32_t DeviceId)
    {
        assert(IsInGameThread() && "必须在游戏线程UnregisterOutputDevice");
        while (DeviceLockFlag.test_and_set())
        {
        }
        auto P = [DeviceId](const auto& Info) { return Info.Id == DeviceId; };
        auto F = std::ranges::find_if(DeviceList, P);
        if (F != DeviceList.end())
        {
            F->Device->Destroy_GameThread();
        }
        std::erase_if(DeviceList, P);
        DeviceLockFlag.clear();
    }

    void LogSubsystem::Start()
    {
        LogThread = std::move(std::thread([this]()
                {
                    while (!Quit)
                    {
                        while (this->LogQueue.IsEmpty() && !Quit)
                        {
                            std::unique_lock Lock{ ConditionMutex };
                            Condition.wait(Lock);
                        }

                        LogQueue.ConsumeUntilEmpty([this](const LogLineInfo* Info)
                            {
                                while (DeviceLockFlag.test_and_set())
                                {
                                }
                                for (const auto& DeviceInfo : DeviceList)
                                {
                                    DeviceInfo.Device->
                                               OutputLog(Info->Level, Info->Time, Info->Category, Info->LogData);
                                }
                                DeviceLockFlag.clear();
                            }
                        );
                    }
                }
            )
        );
    }

    void LogSubsystem::Stop()
    {
        Quit = true;
        this->Condition.notify_one();
        LogThread.join();

        assert(IsInGameThread());
        for (auto& Device : DeviceList)
        {
            Device.Device->Destroy_GameThread();
        }
    }

    void LogSubsystem::OutputLog(ELogLevel LogLevel,
                                 const TimePoint& Time,
                                 const LogCategory& Category,
                                 std::wstring_view LogStr)
    {
        if (LogQueue.EnqueueAndReturnWasEmpty(LogLevel, Time, Category, LogStr))
        {
            Condition.notify_one();
        }
    }

    void LogSubsystem::LogImp(ELogLevel Level, const LogCategory& Category, std::wstring_view LogData)
    {
        auto Time = Chrono::system_clock::now();
        this->OutputLog(Level, Time, Category, LogData);

        if (Level == ELogLevel::Fatal)
        {
            // TODO 临时使用 Assert
            assert(false);
        }
    }
}
