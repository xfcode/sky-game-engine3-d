#pragma once

#include <type_traits>

#define ENUM_CLASS_FLAGS(Enum) \
    inline           Enum& operator|=(Enum& Lhs, Enum Rhs) { return Lhs = (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) | static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    inline           Enum& operator&=(Enum& Lhs, Enum Rhs) { return Lhs = (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) & static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    inline           Enum& operator^=(Enum& Lhs, Enum Rhs) { return Lhs = (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) ^ static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    inline constexpr Enum  operator| (Enum  Lhs, Enum Rhs) { return (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) | static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    inline constexpr Enum  operator& (Enum  Lhs, Enum Rhs) { return (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) & static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    inline constexpr Enum  operator^ (Enum  Lhs, Enum Rhs) { return (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) ^ static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    inline constexpr bool  operator! (Enum  E)             { return !static_cast<std::underlying_type<Enum>::type>(E); } \
    inline constexpr Enum  operator~ (Enum  E)             { return (Enum)~static_cast<std::underlying_type<Enum>::type>(E); }

#define FRIEND_ENUM_CLASS_FLAGS(Enum) \
    friend           Enum& operator|=(Enum& Lhs, Enum Rhs) { return Lhs = (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) | static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    friend           Enum& operator&=(Enum& Lhs, Enum Rhs) { return Lhs = (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) & static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    friend           Enum& operator^=(Enum& Lhs, Enum Rhs) { return Lhs = (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) ^ static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    friend constexpr Enum  operator| (Enum  Lhs, Enum Rhs) { return (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) | static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    friend constexpr Enum  operator& (Enum  Lhs, Enum Rhs) { return (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) & static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    friend constexpr Enum  operator^ (Enum  Lhs, Enum Rhs) { return (Enum)(static_cast<std::underlying_type<Enum>::type>(Lhs) ^ static_cast<std::underlying_type<Enum>::type>(Rhs)); } \
    friend constexpr bool  operator! (Enum  E)             { return !static_cast<std::underlying_type<Enum>::type>(E); } \
    friend constexpr Enum  operator~ (Enum  E)             { return (Enum)~static_cast<std::underlying_type<Enum>::type>(E); }

template <typename Enum>
    requires std::is_enum_v<Enum>
constexpr bool EnumHasAllFlags(Enum Flags, Enum Contains)
{
    using EnumUnderlyingType = std::underlying_type_t<Enum>;
    return (static_cast<EnumUnderlyingType>(Flags) & static_cast<EnumUnderlyingType>(Contains)) ==
        static_cast<EnumUnderlyingType>(Contains);
}

template <typename Enum>
    requires std::is_enum_v<Enum>
constexpr bool EnumHasAnyFlags(Enum Flags, Enum Contains)
{
    using EnumUnderlyingType = std::underlying_type_t<Enum>;
    return (static_cast<EnumUnderlyingType>(Flags) & static_cast<EnumUnderlyingType>(Contains)) != 0;
}

template <typename Enum>
    requires std::is_enum_v<Enum>
void EnumAddFlags(Enum& Flags, Enum FlagsToAdd)
{
    Flags |= FlagsToAdd;
}

template <typename Enum>
    requires std::is_enum_v<Enum>
void EnumRemoveFlags(Enum& Flags, Enum FlagsToRemove)
{
    Flags &= ~FlagsToRemove;
}
