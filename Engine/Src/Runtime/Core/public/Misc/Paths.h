#pragma once

#include "CoreModuleDef.h"

#include <filesystem>

namespace SkyGameEngine3d::Paths
{
    namespace Fs = std::filesystem;

    inline Fs::path GetAppDataDir()
    {
        return Fs::current_path() / L"Engine/AppData";
    }

    inline Fs::path GetLogDir()
    {
        return GetAppDataDir() / L"Log";
    }

    inline Fs::path GetBinaryDir()
    {
        return Fs::current_path() / L"Engine/Build/Output/Bin/Debug";
    }

    inline Fs::path GetShaderDir()
    {
        return Fs::current_path() / L"Engine/Src/Runtime/RHI/D3D12RHI/Shader";
    }

    inline Fs::path GetEngineResourceDir()
    {
        return Fs::current_path() / L"Engine/Resource";
    }
}
