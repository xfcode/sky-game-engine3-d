#pragma once

#include "CoreModuleDef.h"
#include "HAL/PlatformUtility.h"

namespace SkyGameEngine3d
{
    inline CORE_API std::wstring Utf8ToWChar(std::string_view Str)
    {
        return PlatformStringUtility::Utf8ToWChar(Str);
    }

    inline CORE_API std::string WCharToAnsi(std::wstring_view Str)
    {
        return PlatformStringUtility::WCharToAnsi(Str);
    }
}
