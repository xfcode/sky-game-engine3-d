#pragma once

#include <type_traits>

namespace SkyGameEngine3d
{
    struct CNonCopyable
    {
        CNonCopyable() = default;
        CNonCopyable(const CNonCopyable&) = delete;
        CNonCopyable& operator=(const CNonCopyable&) = delete;
    };

    template <class Type>
    struct TSingletonInstance : public CNonCopyable
    {
        static Type* GetInstance()
        {
            static Type T;
            return &T;
        }
    };

    template <typename Call, typename... ArgsType>
    struct TStaticLazyExec
    {
        explicit TStaticLazyExec(Call&& LambdaCall, ArgsType&&... Args)
        {
            LambdaCall(std::forward<ArgsType>(Args)...);
        }
    };

    template <typename EnterCall, typename ExitCall>
    struct TScopeExec
    {
        explicit TScopeExec(EnterCall&& LambdaEnterCall)
        {
            LambdaEnterCall();
        }

        explicit TScopeExec(EnterCall&& LambdaEnterCall, ExitCall&& LambdaExitCall)
            : ExitCallIns{ std::forward<ExitCall>(LambdaExitCall) }
        {
            LambdaEnterCall();
        }

        ~TScopeExec()
        {
            ExitCallIns();
        }

        ExitCall ExitCallIns;
    };
}
