﻿#pragma once

#include "CoreModuleDef.h"
#include <filesystem>

namespace SkyGameEngine3d::FileHelper
{
    /**
         * \brief 尝试创建文件或目录，函数会自动创建路径中缺失的目录
         * \param CreatePath 要创建的路径
         * \return 如果创建失败返回 false
         */
    CORE_API bool TryCreateFile(const std::filesystem::path& CreatePath);
}
