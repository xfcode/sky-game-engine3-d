﻿#pragma once

#include <assert.h>
#include <type_traits>

#include "CoreModuleDef.h"
#include "Math/MathLib.h"

namespace SkyGameEngine3d::MemoryLib
{
    /**
     * \brief Val 向上对齐
     * \tparam T 
     * \param Val 
     * \param Alignment 对齐值，必须为2的次幂
     * \return 
     */
    template <typename T>
        requires std::is_integral_v<T> || std::is_pointer_v<T>
    constexpr T AlignUp(T Val, const uint64_t Alignment)
    {
        assert(MathLib::IsPowerOfTow(Alignment));
        uint64_t Value = (static_cast<uint64_t>(Val) + Alignment - 1) & ~(Alignment - 1);
        assert(std::numeric_limits<T>::max() > Value); 
        return static_cast<T>(Value);
    } 

    /**
     * \brief Val 向下对齐
     * \tparam T 
     * \param Val 
     * \param Alignment 对齐值，必须为2的次幂
     * \return 
     */
    template <typename T>
        requires std::is_integral_v<T> || std::is_pointer_v<T>
    constexpr T AlignDown(T Val, const uint64_t Alignment)
    {
        assert(MathLib::IsPowerOfTow(Alignment));
        return static_cast<T>(static_cast<uint64_t>(Val) & ~(Alignment - 1));
    }
}
