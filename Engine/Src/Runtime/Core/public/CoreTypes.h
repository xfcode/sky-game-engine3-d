#pragma once
#include <cstdint>

#include "CoreModuleDef.h"

namespace SkyGameEngine3d
{
    constexpr inline uint32_t INDEX_NONE = -1;
}
