﻿#pragma once

#include <thread>
#include <cassert>

namespace SkyGameEngine3d
{
    //! 多线程生产，单线程消费 的队列
    //! \tparam Type 数据类型
    template <typename Type>
        requires(std::is_destructible_v<Type>)
    class MpscQueue
    {
        struct Node
        {
            std::atomic<Node*> Next = nullptr;
            uint8_t Value[sizeof(Type)]{ };
        };

        // 一个哨兵节点  Sentinel->Next 是这个队的头部
        Node Sentinel;
        std::atomic<Node*> Tail = &Sentinel;

    public:
        ~MpscQueue()
        {
            auto Node = Sentinel.Next.load();
            while (Node)
            {
                auto Next = Node->Next.load();
                reinterpret_cast<Type*>(Node->Value)->~Type();
                delete Node;
                Node = Next;
            }
        }

        //! 把数据入队，根据参数构造对象
        //! \tparam Arg
        //! \param Args 构造对象的参数
        template <typename... Arg>
        void Enqueue(Arg... Args)
        {
            EnqueueAndReturnWasEmpty(std::forward<Arg>(Args)...);
        }

        //! 把数据入队，根据参数构造对象，并且返回入队前队列是否为空
        //! \tparam Arg
        //! \param Args 构造对象的参数
        //! \return 返回入队前队列是否为空
        template <typename... Arg>
        bool EnqueueAndReturnWasEmpty(Arg... Args)
        {
            Node* NewNode = new Node;
            new(&NewNode->Value)Type(Args...);

            //  std::memory_order_release 保证之前的写操作执行完毕，才能执行本操作
            // （确保 NewNode 构造完成后才执行 Exchange，这样消费者线程看到的都一定是完整的 Value）
            Node* OldTail = Tail.exchange(NewNode, std::memory_order_release);
            assert(OldTail->Next.load(std::memory_order_relaxed) == nullptr && "末尾的节点的Next一定是空的");
            OldTail->Next.store(Tail, std::memory_order_relaxed);
            // 如果 之前的尾 == 哨兵节点，说明入队前是空的
            return OldTail == &Sentinel;
        }

        //! 消费队列中的全部数据
        //! \tparam CallFunc
        //! \param Consumer 消费者回调
        template <typename CallFunc>
        void ConsumeUntilEmpty(CallFunc&& Consumer)
        {
            // 尝试获取头节点，这里的顺序是没关系
            Node* HeaderNode = Sentinel.Next.load(std::memory_order_relaxed);
            if (!HeaderNode)
            {
                return;
            }

            // 清空头节点
            Sentinel.Next.store(nullptr, std::memory_order_relaxed);
            // 交互尾节点 std::memory_order_acquire 保证之后的读操作 都发生在 本操作之后 （相当于 与 EnqueueAndReturnWasEmpty 的一个同步点）
            Node* TailNode = Tail.exchange(&Sentinel, std::memory_order_acquire);

            // 遍历头尾之间的数据
            auto GetNextNode = [](Node* NodePtr)
            {
                Node* P = nullptr;
                while (!P)
                {
                    P = NodePtr->Next.load(std::memory_order_relaxed);
                }
                return P;
            };

            while (HeaderNode != TailNode)
            {
                Consumer(reinterpret_cast<Type*>(HeaderNode->Value));
                auto Next = GetNextNode(HeaderNode);
                reinterpret_cast<Type*>(HeaderNode->Value)->~Type();
                delete HeaderNode;
                HeaderNode = Next;
            }
            Consumer(reinterpret_cast<Type*>(TailNode->Value));
            delete TailNode;
        }

        //! 是否为空
        //! \return
        bool IsEmpty() const
        {
            return Tail.load(std::memory_order_relaxed) == &Sentinel;
        }
    };
}
