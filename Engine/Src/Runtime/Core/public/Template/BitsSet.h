﻿#pragma once

#include <bitset>
#include <cstdint>

#include "CoreModuleDef.h"
#include "Math/MathLib.h"

namespace SkyGameEngine3d
{
    template <uint32_t BitsCount>
    class TBitsSet
    {
    public:
        using IntType = uint64_t;

        /**
        * \brief 设置指定值在Index位置
        * \param Index 
        */
        template <bool Value>
        void SetValue(const uint32_t Index)
        {
            if (MaxBitsCount <= Index)
            {
                assert(false);
                return;
            }

            uint32_t ArrayIndex = (BitsArrayCount - 1) - (Index / IntTypeBitsCount);
            const uint32_t SubBitsIndex = Index % IntTypeBitsCount;
            IntType Mask = static_cast<IntType>(1) << SubBitsIndex;
            if constexpr (Value)
            {
                BitsArray[ArrayIndex] |= Mask;
            }
            else
            {
                BitsArray[ArrayIndex] &= ~Mask;
            }
        }

        /**
         * \brief 设置指定值在Index位置
         * \param Index 
         * \param Value 
         */
        void SetValue(const uint32_t Index, const bool Value)
        {
            if (Value)
            {
                SetValue<true>(Index);
            }
            else
            {
                SetValue<false>(Index);
            }
        }

        /**
         * \brief 查找第一个值为指定值的Index
         * \param Value 
         * \return 如果没找到，返回 -1
         */
        int32_t FindFirstValueIndex(const bool Value)
        {
            if (Value)
            {
                return FindFirstValueIndex<true>();
            }
            else
            {
                return FindFirstValueIndex<false>();
            }
        }

        /**
         * \brief 查找第一个值为指定值的Index
         * \tparam Value
         * \return 如果没找到，返回 -1
         */
        template <bool Value>
        int32_t FindFirstValueIndex()
        {
            constexpr int32_t LastIndex = BitsArrayCount - 1;
            if constexpr (Value)
            {
                for (int ArrayIndex = LastIndex; ArrayIndex >= 0; --ArrayIndex)
                {
                    const IntType IntValue = BitsArray[ArrayIndex];
                    if (IntValue > 0)
                    {
                        for (uint32_t Offset = 0; Offset < IntTypeBitsCount; ++Offset)
                        {
                            if (IntValue & (static_cast<IntType>(1) << Offset))
                            {
                                return (LastIndex - ArrayIndex) * IntTypeBitsCount + Offset;
                            }
                        }
                    }
                }
                return -1;
            }
            else
            {
                for (int ArrayIndex = LastIndex; ArrayIndex >= 0; --ArrayIndex)
                {
                    const IntType IntValue = BitsArray[ArrayIndex];

                    if (IntValue == 0)
                    {
                        return (LastIndex - ArrayIndex) * IntTypeBitsCount;
                    }

                    for (uint32_t Offset = 0; Offset < IntTypeBitsCount; ++Offset)
                    {
                        if ((IntValue & (static_cast<IntType>(1) << Offset)) == 0)
                        {
                            return (LastIndex - ArrayIndex) * IntTypeBitsCount + Offset;
                        }
                    }
                }
                return -1;
            }
        }

        
        /**
         * \brief 全部清除为0
         */
        void Clear()
        {
            memset(BitsArray, 0, BitsArrayCount * sizeof(IntType));
        }

        /**
         * \brief 全部填充为1
         */
        void Fill()
        {
            memset(BitsArray, ~0, BitsArrayCount * sizeof(IntType));
        }

        static constexpr uint32_t IntTypeBitsCount = sizeof(IntType) * 8;
        static constexpr uint32_t BitsArrayCount = MathLib::DivisionCeilResult(BitsCount, IntTypeBitsCount);
        static constexpr uint32_t MaxBitsCount = BitsCount;

    private:
        IntType BitsArray[BitsArrayCount] = { };
    };
}
