#pragma once

#include <thread>
#include "CoreModuleDef.h"
#include "Log/Log.h"

namespace SkyGameEngine3d
{
    enum class EThreadName
    {
        Unknown,
        GameThread,
    };

    CORE_API void RegNamedThread(std::thread::id Id, EThreadName Name);

    CORE_API EThreadName GetCurrentThreadName();

    CORE_API inline bool IsInGameThread()
    {
        return GetCurrentThreadName() == EThreadName::GameThread;
    }
}

DEFINE_EXPORT_LOG_CATEGORY(LogCore);
