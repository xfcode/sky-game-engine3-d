﻿#pragma once

#include "CoreModuleDef.h"
#include "Vector.h"
#include <array>

// ReSharper disable CppClangTidyModernizePassByValue

namespace SkyGameEngine3d
{
    /**
     * \brief 轴对齐 Box
     */
    class CORE_API Box3D
    {
    public:
        Box3D();

        Box3D(const Vector3D& InMin, const Vector3D& InMax);

        [[nodiscard]] Vector3D GetCenter() const
        {
            return (Min + Max) * 0.5f;
        }

        void Add(const Vector3D& Point);

        void Add(const Box3D& Point);

        [[nodiscard]] bool IsEmpty() const;

        [[nodiscard]] bool Contains(const Vector3D& Point) const;

        bool IntersectBox(const Box3D& Other, Box3D* OutIntersectBox = nullptr) const;

        [[nodiscard]] const Vector3D& GetMin() const
        {
            return Min;
        }

        [[nodiscard]] const Vector3D& GetMax() const
        {
            return Max;
        }

        /**
         * \brief 分裂 Box
         * \tparam XSplitCount X 轴分裂的次数
         * \tparam YSplitCount Y 轴分裂的次数
         * \tparam ZSplitCount Z 轴分裂的次数
         * \tparam BoxCount 不需要填写
         * \param TargetBox 
         * \return 
         */
        template <size_t XSplitCount, size_t YSplitCount, size_t ZSplitCount = 0,
                  size_t BoxCount = (XSplitCount + 1) * (YSplitCount + 1) * (ZSplitCount + 1)>
        [[nodiscard]] static std::array<Box3D, BoxCount> SplitBox(const Box3D& TargetBox)
        {
            constexpr auto XSplitBoxCount = XSplitCount + 1;
            constexpr auto YSplitBoxCount = YSplitCount + 1;
            constexpr auto ZSplitBoxCount = ZSplitCount + 1;

            const auto XStep = (TargetBox.Max.X - TargetBox.Min.X) / XSplitBoxCount;
            const auto YStep = (TargetBox.Max.Y - TargetBox.Min.Y) / YSplitBoxCount;
            const auto ZStep = (TargetBox.Max.Z - TargetBox.Min.Z) / ZSplitBoxCount;

            std::array<Box3D, BoxCount> Ret;

            int32_t Index = 0;
            for (int XIndex = 0; XIndex < XSplitBoxCount; ++XIndex)
            {
                for (int YIndex = 0; YIndex < YSplitBoxCount; ++YIndex)
                {
                    for (int ZIndex = 0; ZIndex < ZSplitBoxCount; ++ZIndex)
                    {
                        Vector3D Min{
                            TargetBox.Min.X + XStep * XIndex,
                            TargetBox.Min.Y + YStep * YIndex,
                            TargetBox.Min.Z + ZStep * ZIndex
                        };

                        Vector3D Max{
                            TargetBox.Min.X + XStep * (XIndex + 1),
                            TargetBox.Min.Y + YStep * (YIndex + 1),
                            TargetBox.Min.Z + ZStep * (ZIndex + 1)
                        };

                        Ret[Index] = Box3D(Min, Max);

                        Index++;
                    }
                }
            }

            return Ret;
        }

    private:
        Vector3D Min;

        Vector3D Max;
    };
}
