﻿#pragma once

#include <cstdint>
#include "MathDef.h"
#include "CoreModuleDef.h"

namespace SkyGameEngine3d
{
    struct Color32;

    //! 线性空间的颜色值
    struct CORE_API ColorLinear
    {
        float R;
        float G;
        float B;
        float A;

        ColorLinear();

        ColorLinear(NoInitPlaceholderType);

        ColorLinear(float InR, float InG, float InB, float InA);

        static ColorLinear CreateFromSrgb(const Color32& Color);

        Color32 ToColorSrgb() const;

        const float* GetPtr() const
        {
            return reinterpret_cast<const float*>(this);
        }
    };


    //! 32 位颜色值
    struct CORE_API Color32
    {
        // 注意这几个值在计算时会打包到一个 uint32 中，所以会有字节序的问题，目前只考虑的 小端 的情况，这样进行排布
        uint8_t B;
        uint8_t G;
        uint8_t R;
        uint8_t A;

        Color32()
        {
            GetValueRef() = 0;
        };

        Color32(uint8_t InR, uint8_t InG, uint8_t InB, uint8_t InA)
            : B(InB)
            , G(InG)
            , R(InR)
            , A(InA)
        {
        }

        uint32_t GetValue() const
        {
            return *(uint32_t*)(this);
        }

        uint32_t& GetValueRef()
        {
            return *(uint32_t*)(this);
        }
    };
}
