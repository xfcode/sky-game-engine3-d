﻿#pragma once

#include "CoreModuleDef.h"
#include "Vector.h"

namespace SkyGameEngine3d
{
    class Geometry
    {
    public:
        Vector2D Size;

        Vector2D Pos;
    };
}
