﻿#pragma once

#include <cstdint>
#include <cstring>
#include <functional>
#include <cassert>
#include "CoreModuleDef.h"
#include "MathLib.h"

namespace SkyGameEngine3d
{
    namespace ECompType
    {
        enum Index
        {
            X,
            Y,
            Z,
            W,
        };
    }

    enum class EVectorConstructorType : uint8_t
    {
        NoInit,
        InitZero,
    };

    using ECompTypeIndex = ECompType::Index;

    template <typename VectorType>
    class TVector
    {
    public:
        using DataType = FloatType;

    public:
        explicit TVector(EVectorConstructorType Type = EVectorConstructorType::InitZero)
        {
            if (Type == EVectorConstructorType::InitZero)
            {
                memset(this, 0, GetSizeBytes()); // NOLINT(bugprone-undefined-memory-manipulation)
            }
        }

        TVector(const TVector& Other)
        {
            memcpy(this, &Other, GetSizeBytes()); // NOLINT(bugprone-undefined-memory-manipulation)
        }

        void ForeachComp(const std::function<void(const DataType&, uint8_t)>& Body) const
        {
            for (uint8_t I = 0; I < GetCompCount(); ++I)
            {
                Body(static_cast<const VectorType*>(this)->Comps[I], I);
            }
        }

        void ForeachComp(const std::function<void(DataType&, uint8_t)>& Body)
        {
            for (uint8_t I = 0; I < GetCompCount(); ++I)
            {
                Body(static_cast<VectorType*>(this)->Comps[I], I);
            }
        }

        bool operator==(const TVector& Other) const
        {
            if (this == &Other)
            {
                return true;
            }
            for (uint8_t I = 0; I < GetCompCount(); ++I)
            {
                // 有时需要比较两个是否完全一样
                if (Get(I) != Other.Get(I)) // NOLINT(clang-diagnostic-float-equal)
                {
                    return false;
                }
            }
            return true;
        }

        [[nodiscard]] bool IsNearly(const TVector& Other, FloatType Tolerance = FloatToleranceValue) const
        {
            if (this == &Other)
            {
                return true;
            }
            for (uint8_t I = 0; I < GetCompCount(); ++I)
            {
                if (!MathLib::IsEq(Get(I), Other.Get(I), Tolerance))
                {
                    return false;
                }
            }
            return true;
        }

        bool operator!=(const TVector& Other) const
        {
            return !this->operator==(Other);
        }

        VectorType operator-() const
        {
            VectorType Ret(EVectorConstructorType::NoInit);
            ForeachComp([&](const auto& ValueRef, auto Index)
                {
                    Ret.GetRef(Index) = -ValueRef;
                }
            );
            return Ret;
        }

        VectorType operator+(const VectorType& Other) const
        {
            VectorType Ret(EVectorConstructorType::NoInit);
            ForeachComp([&](const auto& ValueRef, auto Index)
                {
                    Ret.GetRef(Index) = ValueRef + Other.Get(Index);
                }
            );
            return Ret;
        }

        VectorType operator-(const VectorType& Other) const
        {
            VectorType Ret(EVectorConstructorType::NoInit);
            ForeachComp([&](const auto& ValueRef, auto Index)
                {
                    Ret.GetRef(Index) = ValueRef - Other.Get(Index);
                }
            );
            return Ret;
        }

        VectorType operator*(FloatType Scala) const
        {
            VectorType Ret(EVectorConstructorType::NoInit);
            ForeachComp([&](const auto& ValueRef, auto Index)
                {
                    Ret.GetRef(Index) = ValueRef * Scala;
                }
            );
            return Ret;
        }

        VectorType operator/(FloatType Scala) const
        {
            VectorType Ret(EVectorConstructorType::NoInit);
            ForeachComp([&](const auto& ValueRef, auto Index)
                {
                    Ret.GetRef(Index) = ValueRef / Scala;
                }
            );
            return Ret;
        }

        VectorType& operator+=(const VectorType& Other)
        {
            ForeachComp([&](DataType& ValueRef, auto Index)
                {
                    ValueRef += Other.Get(Index);
                }
            );
            return static_cast<VectorType&>(*this);
        }

        VectorType& operator-=(const VectorType& Other)
        {
            ForeachComp([&](DataType& ValueRef, auto Index)
                {
                    ValueRef -= Other.Get(Index);
                }
            );
            return static_cast<VectorType&>(*this);
        }

        VectorType& operator*=(FloatType Scala)
        {
            ForeachComp([&](DataType& ValueRef, auto Index)
                {
                    ValueRef *= Scala;
                }
            );
            return static_cast<VectorType&>(*this);
        }

        VectorType& operator/=(FloatType Scala)
        {
            ForeachComp([&](DataType& ValueRef, auto Index)
                {
                    ValueRef /= Scala;
                }
            );
            return static_cast<VectorType&>(*this);
        }

        void Normalize()
        {
            const auto Length = GetLength();
            if (Length > 0)
            {
                const DataType TempValue = 1.0f / Length;
                this->ForeachComp([&](DataType& Ref, auto)
                    {
                        Ref *= TempValue;
                    }
                );
            }
        }

        [[nodiscard]] DataType Dot(const TVector& Other) const
        {
            DataType Ret = 0;
            ForeachComp([&](const auto& Value, auto Index)
                {
                    Ret += (Value * Other.Get(Index));
                }
            );
            return Ret;
        }

        [[nodiscard]] constexpr uint8_t GetCompCount() const
        {
            return VectorType::CompCount;
        }

        [[nodiscard]] constexpr uint8_t GetSizeBytes() const
        {
            return sizeof(DataType) * GetCompCount();
        }

        template <ECompTypeIndex CompTypeIndex>
        [[nodiscard]] constexpr DataType Get() const
        {
            if constexpr (CompTypeIndex < GetCompCount())
            {
                return reinterpret_cast<VectorType*>(this)->Comps[CompTypeIndex];
            }
            else
            {
                return 0;
            }
        }

        template <ECompTypeIndex CompTypeIndex>
        DataType& GetRef()
        {
            static_assert(CompTypeIndex < GetCompCount(), "超出了Index");
            return reinterpret_cast<VectorType*>(this)->Comps[CompTypeIndex];
        }

        DataType& GetRef(uint8_t Index)
        {
            assert(Index < GetCompCount() && "超出了Index");
            return reinterpret_cast<VectorType*>(this)->Comps[Index];
        }

        [[nodiscard]] DataType Get(uint8_t Index) const
        {
            if (Index < GetCompCount())
            {
                return reinterpret_cast<const VectorType*>(this)->Comps[Index];
            }
            return 0;
        }

        [[nodiscard]] DataType GetLength() const
        {
            return MathLib::Sqrt(GetLengthSquared());
        }

        [[nodiscard]] DataType GetLengthSquared() const
        {
            DataType Ret = 0;
            ForeachComp([&](const auto& V, auto)
                {
                    Ret += V * V;
                }
            );
            return Ret;
        }

        friend VectorType operator*(float Scalar, const VectorType& Other)
        {
            VectorType Ret;
            Ret.ForeachComp([&](DataType& Ref, auto Index)
                {
                    Ref = Scalar * Other.Get(Index);
                }
            );
            return Ret;
        }
    };

    class Vector3D;

    class alignas(16) CORE_API Vector4D // NOLINT(cppcoreguidelines-pro-type-member-init)
        : public TVector<Vector4D>
    {
    public:
        static constexpr uint8_t CompCount = 4;
        using TVector<Vector4D>::TVector;

        Vector4D(FloatType InX, FloatType InY, FloatType InZ, FloatType InW);

        Vector4D(const Vector3D& Vec, FloatType InW);

        union
        {
            FloatType Comps[CompCount];

            struct
            {
                FloatType X;
                FloatType Y;
                FloatType Z;
                FloatType W;
            };
        };
    };

    class CORE_API Vector3D // NOLINT(cppcoreguidelines-pro-type-member-init)
        : public TVector<Vector3D>
    {
    public:
        static constexpr uint8_t CompCount = 3;
        using TVector<Vector3D>::TVector;

        Vector3D(FloatType InX, FloatType InY, FloatType InZ);

        explicit Vector3D(const Vector4D& Vec);

        union
        {
            struct
            {
                FloatType X;
                FloatType Y;
                FloatType Z;
            };

            FloatType Comps[CompCount];
        };
    };

    class CORE_API Vector2D // NOLINT(cppcoreguidelines-pro-type-member-init)
        : public TVector<Vector2D>
    {
    public:
        static constexpr uint8_t CompCount = 2;
        using TVector<Vector2D>::TVector;

        Vector2D(FloatType InX, FloatType InY);

        union
        {
            struct
            {
                FloatType X;
                FloatType Y;
            };

            FloatType Comps[CompCount];
        };
    };

    class CORE_API Vector2DInt
    {
    public:
        Vector2DInt() = default;

        Vector2DInt(int32_t InX, int32_t InY)
            : X(InX)
            , Y(InY)
        {
        }

        int32_t X = 0;
        int32_t Y = 0;
    };
}
