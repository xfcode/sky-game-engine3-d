﻿#pragma once

#include "CoreModuleDef.h"

namespace SkyGameEngine3d
{
    //! 不进行初始化的一个占位类型
    struct CORE_API NoInitPlaceholderType
    {
    };

    inline const NoInitPlaceholderType NoInitPlaceholder = { };
}
