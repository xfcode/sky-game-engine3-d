﻿#pragma once

#include <assert.h>

#include "CoreModuleDef.h"
#include <limits>
#include <type_traits>

#include "CoreModuleDef.h"

namespace SkyGameEngine3d
{
    using FloatType = float;

    constexpr FloatType FloatToleranceValue = std::numeric_limits<FloatType>::min();
    constexpr FloatType PI = 3.1415926f;
    constexpr FloatType PI2 = 2.f * PI;
    class Vector3D;

    class Vector2D;

    namespace MathLib
    {
        CORE_API FloatType Sqrt(FloatType Value);

        CORE_API bool IsEq(FloatType A, FloatType B, FloatType Tolerance = FloatToleranceValue);

        CORE_API FloatType Abs(FloatType A);

        CORE_API Vector3D CrossProduct(const Vector3D& A, const Vector3D& B);

        template <typename VectorType>
            requires (std::is_same_v<VectorType, Vector2D> || std::is_same_v<VectorType, Vector3D>)
        FloatType DistanceSquared(const VectorType& A, const VectorType& B)
        {
            return (A - B).GetLengthSquared();
        }

        template <typename VectorType>
            requires (std::is_same_v<VectorType, Vector2D> || std::is_same_v<VectorType, Vector3D>)
        FloatType Distance(const VectorType& A, const VectorType& B)
        {
            return Sqrt(DistanceSquared(A, B));
        }

        [[maybe_unused]] CORE_API float AngleToRadian(float Angle);

        [[maybe_unused]] CORE_API float RadianToAngle(float Radian);

        CORE_API inline bool IsPowerOfTow(const uint64_t Value)
        {
            return (Value & (Value - 1)) == 0;
        }

        /**
         * \brief 做整数除法，得数向上取整
         * \param Dividend 被除数
         * \param Divisor 除数
         * \return 
         */
        CORE_API constexpr uint32_t DivisionCeilResult(const uint32_t Dividend, const uint32_t Divisor)
        {
            assert(Divisor > 0);
            return (Dividend + (Divisor - 1)) / Divisor;
        }

        template <typename T>
            requires std::is_arithmetic_v<T>
        constexpr T Max(T A, T B)
        {
            return A > B ? A : B;
        }

        template <typename T>
            requires std::is_arithmetic_v<T>
        constexpr T Min(T A, T B)
        {
            return A < B ? A : B;
        }

        template <typename T>
            requires std::is_arithmetic_v<T>
        constexpr T Clamp(T A, T Min, T Max)
        {
            return A < Min ? Min : (A > Max) ? Max : A;
        }
        
    };
}
