﻿#pragma once

#include "MathLib.h"
#include "Vector.h"

namespace SkyGameEngine3d
{
    /*
     *  标准
     *  向量类型： 引擎的所有地方统一使用 行向量
     *  也就意味着向量通过矩阵可以非常符合预期的变换 （从左向右）
     *  [x, y, z, w] * Ma * Mb * Mc = [x1, y1, z1, w1]
     *
     *  理解参考
     *  https://www.bilibili.com/video/BV1ib411t7YR?t=187.9&p=3
     *  https://www.bilibili.com/video/BV1ib411t7YR?t=397.5&p=4
     *  注意：视频使用的是列向量
     *
     *  基础的设定： 一个2维向量可以用 2 个基向量的线性组合来表示  V = aX + bY  <向量用大写字母表示>  所以我们可以说 (a, b) 是 V向量
     *  矩阵的每一行可以看做是一个维度的基向量坐标
     *              x1, y1, z1     x * (x1, y1, z1)
     *  [x, y, z] * x2, y2, z2 = + y * (x2, y2, z2) = (x*x1 + y*x2 + z*x3, x*y1 + y*y2 + z*y3, x*z1 + y*z2 + z*z3)
     *              x3, y3, z3   + z * (x3, y3, z3)
     *
     *  变换后的坐标 可以看做是 旧的向量 用新的 基向量表示的线性组合的 系数
     */

    enum class EMatrixConstructorType : uint8_t
    {
        NoInit,
        InitZero,
        Identity,
    };

    //! 4X4 的方阵
    class alignas(16) CORE_API Matrix4X4
    {
    public:
        //! 单位矩阵
        static const Matrix4X4 Identity;

        //! 构构建一个矩阵，根据类型
        //! \param InitType 初始化的类型
        explicit Matrix4X4(EMatrixConstructorType Type = EMatrixConstructorType::Identity);

        //! 使用基向量来构建一个矩阵
        //! \param InX X轴
        //! \param InY Y轴
        //! \param InZ Z轴
        //! \param InW 这个轴是一个 *齐次坐标轴* 用来实现3D坐标的平移
        Matrix4X4(const Vector3D& InX, const Vector3D& InY, const Vector3D& InZ, const Vector3D& InW);

        //! 拷贝构造
        Matrix4X4(const Matrix4X4& Other);

        Matrix4X4 operator*(const Matrix4X4& Other) const;

        Matrix4X4& operator*=(const Matrix4X4& Other);

        Matrix4X4 operator*(FloatType Scalar) const;

        Matrix4X4& operator*=(FloatType Scalar);

        [[nodiscard]] Vector3D TransformVector(const Vector3D& Vector) const;

        [[nodiscard]] Vector4D TransformVector(const Vector4D& Vector) const;

        //! 生成矩阵的逆
        //! \return 返回这个矩阵的逆
        [[nodiscard]] Matrix4X4 Inverse() const;

        Matrix4X4 Transpose() const;

        //! 获取矩阵指定行的向量 编译时Check
        //! \tparam Index 行的Index
        //! \return
        template <uint8_t Index>
        [[nodiscard]] Vector4D GetCompVector() const
        {
            static_assert(Index < 4, L"超出了范围");
            auto& Row = M[Index];
            return Vector4D(Row[0], Row[1], Row[2], Row[3]);
        }

        //! 获取矩阵指定行的向量 运行时Check
        //! \param Index 行的Index
        //! \return
        [[nodiscard]] Vector4D GetCompVector(uint8_t Index) const
        {
            assert(Index < 4 && L"超出了范围");
            auto& Row = M[Index];
            return { Row[0], Row[1], Row[2], Row[3] };
        }

        //! 判定两个矩阵是否接近
        //! \param Other
        //! \param Tolerance
        //! \return
        [[nodiscard]] bool IsNearly(const class Matrix4X4& Other, FloatType Tolerance = FloatToleranceValue) const;

        static Matrix4X4 MakeLookAtMat(const Vector4D& Pos, const Vector4D& Target, const Vector4D& Up);

        static Matrix4X4 MakeProjMat(float Fov, float Aspect, float Near, float Far);

    private:
        static Matrix4X4 CreateIdentity();

    private:
        FloatType M[4][4];
    };

    CORE_API inline Vector3D operator*(const Vector3D& LeftVec, const Matrix4X4& RightMat)
    {
        return RightMat.TransformVector(LeftVec);
    }


    class alignas(16) CORE_API Matrix3X3
    {
    public:
        static const Matrix3X3 Identity;

        explicit Matrix3X3(const EMatrixConstructorType Type = EMatrixConstructorType::Identity);

        explicit Matrix3X3(FloatType UniformScale, const Vector2D& Translation = Vector2D{ });

        explicit Matrix3X3(const Vector2D& Scale, const Vector2D& Translation = Vector2D{ });

        [[nodiscard]] Vector2D TransformVector(const Vector2D& InVector) const;

        [[nodiscard]] Vector2D TransformPoint(const Vector2D& InPoint) const;

    private:
        static Matrix3X3 CreateIdentity();

    private:
        FloatType M[3][3];
    };

    CORE_API inline Vector2D operator*(const Vector2D& InVector, const Matrix3X3& Mat)
    {
        return Mat.TransformVector(InVector);
    }
}
