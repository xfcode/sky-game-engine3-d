﻿#pragma once

#include <string>

#include "CoreModuleDef.h"

namespace SkyGameEngine3d
{
    namespace Private
    {
        //! Windows 平台相关的一些工具函数
        struct CORE_API WindowsPlatformUtility
        {
            //! 加载动态链接库
            //! \param DllPath 动态库的路径
            //! \return
            static void* LoadDll(std::wstring_view DllPath);

            //! 卸载动态库
            //! \param DllHandle
            static void UnloadDll(void* DllHandle);

            //! 获取动态库导出的 C 符号
            //! \param DllHandle 动态库的句柄
            //! \param ExportSymbolName 符号的名称
            //! \return
            static void* GetDllExport(void* DllHandle, std::wstring_view ExportSymbolName);
        };
    }

    using PlatformUtility = Private::WindowsPlatformUtility;
}
