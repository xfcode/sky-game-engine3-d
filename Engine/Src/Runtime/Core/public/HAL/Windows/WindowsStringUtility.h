#pragma once

#include <string>


namespace SkyGameEngine3d
{
    namespace Private
    {
        struct WindowsStringUtility
        {
            static std::wstring Utf8ToWChar(std::string_view Value);

            static std::string WCharToAnsi(std::wstring_view Value);
        };
    }

    using PlatformStringUtility = Private::WindowsStringUtility;
}
