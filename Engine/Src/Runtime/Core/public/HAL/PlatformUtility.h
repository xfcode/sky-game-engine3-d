#pragma once

#include "MacroHelper.h"

#define HAL_INCLUDE(Path) HELPER_TO_STRING(HELPER_JOIN(PLATFORM_NAME/PLATFORM_NAME, Path))

#include HAL_INCLUDE(StringUtility.h)
#include HAL_INCLUDE(PlatformUtility.h)
