#pragma once


#ifndef DLL_IMPORT
#ifdef MSVC
#define DLL_IMPORT __declspec(dllimport)
#else
#define DLL_IMPORT
#endif
#endif

#ifndef DLL_EXPORT
#ifdef MSVC
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT
#endif
#endif
