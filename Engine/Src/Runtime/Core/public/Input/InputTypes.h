﻿#pragma once

#include <cstdint>
#include <string_view>

#include "CoreModuleDef.h"
#include "CoreTypes.h"

namespace SkyGameEngine3d
{
    struct InputKey
    {
    public:
        static InputKey MakeInputKey(std::wstring_view KeyName);


        std::weak_ordering operator<=>(const InputKey& Other) const
        {
            return Index <=> Other.Index;
        }

    private:
        uint32_t Index = INDEX_NONE;
    };


    struct CORE_API EInputKeys
    {
        inline static InputKey MouseLeftButton = InputKey::MakeInputKey(L"MouseLeftButton");
        inline static InputKey MouseRightButton = InputKey::MakeInputKey(L"MouseRightButton");
    };


    struct InputEvent
    {
    };

    struct InputPointerEvent
    {
    };

    struct InputKeyEvent
    {
    };

    struct InputCharEvent
    {
    };

    struct InputReply
    {
        static InputReply Handle()
        {
            return InputReply{ true };
        }

        static InputReply UnHandle()
        {
            return InputReply{ false };
        }

        explicit InputReply(const bool InIsHandle)
            : IsHandled(InIsHandle)
        {
        }

        bool IsHandled = false;
    };
}
