#pragma once

#define HELPER_TO_STRING(x) __HELPER_TO_STRING(x)
#define __HELPER_TO_STRING(x) #x

#define HELPER_JOIN(x, y) __HELPER_JOIN_INNER(x, y)
#define __HELPER_JOIN_INNER(x, y) x##y

#define STRUCT_MEMBER_OFFSET(Type, Member) ((uint64_t) (&((Type*) 0)->Member))
