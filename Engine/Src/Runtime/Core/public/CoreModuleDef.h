#pragma once

#include "HAL/PlatformDefine.h"

#ifdef CORE
    #define CORE_API DLL_EXPORT
#else
    #define CORE_API DLL_IMPORT
#endif

inline const wchar_t* const GModuleNameCore = L"Core";
