#pragma once

#include <memory>
#include <vector>
#include <chrono>
#include <source_location>
#include "CoreModuleDef.h"
#include "Template/QueueThreadSafe.h"
#include "Misc/StringUtility.h"
#include "LogType.h"

namespace SkyGameEngine3d
{
    class CORE_API LogSubsystem : public ILogOutputDevice
    {
    public:
        static LogSubsystem& Get()
        {
            static LogSubsystem Sys;
            return Sys;
        }

        LogSubsystem() = default;

        virtual void Init_GameThread() override
        {
            this->Start();
        }

        virtual void Destroy_GameThread() override
        {
            this->Stop();
        }

        int32_t RegisterOutputDevice(std::shared_ptr<ILogOutputDevice> Device);

        void UnregisterOutputDevice(int32_t DeviceId);

        template <ELogLevel Level, typename... Args>
        void Log(const LogCategory& Category,
                 const std::source_location& Location, std::wstring_view Format, Args... Arg)
        {
            std::wstring LogData = std::vformat(Format, std::make_wformat_args(Arg...));
            if constexpr (Level == ELogLevel::Error || Level == ELogLevel::Fatal)
            {
                auto Str = Utf8ToWChar(Location.file_name());

                std::wstring LogStr
                    = std::format(L"{}{}[{}]:{}",
                                  Utf8ToWChar(Location.file_name()),
                                  Utf8ToWChar(Location.function_name()),
                                  Location.line(),
                                  LogData
                    );
                this->LogImp(Level, Category, LogStr);
            }
            else
            {
                this->LogImp(Level, Category, LogData);
            }
        }

    private:
        void Start();

        void Stop();

        void LogImp(ELogLevel Level, const LogCategory& Category, std::wstring_view LogData);

    protected:
        virtual void OutputLog(ELogLevel LogLevel, const TimePoint& Time,
                               const LogCategory& Category, std::wstring_view LogStr) override;

    private:
        struct DeviceInfo
        {
            std::shared_ptr<ILogOutputDevice> Device;
            int32_t Id = -1;
        };

        std::vector<DeviceInfo> DeviceList;

        struct LogLineInfo
        {
            ELogLevel Level;
            TimePoint Time;
            LogCategory Category;
            std::wstring LogData;

            LogLineInfo(ELogLevel InLevel,
                        const TimePoint& InTime,
                        const LogCategory& InCategory,
                        std::wstring_view InLogData)
                : Level(InLevel)
                , Time(InTime)
                , Category(InCategory)
                , LogData(InLogData)
            {
            }

            ~LogLineInfo()
            {
            }
        };

        MpscQueue<LogLineInfo> LogQueue;

        std::atomic_bool Quit = false;
        std::thread LogThread;
        std::condition_variable Condition;
        std::mutex ConditionMutex;
        std::atomic_flag DeviceLockFlag;
        int32_t AllocId = 0;
    };
}
