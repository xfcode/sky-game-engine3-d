#pragma once
#include "CoreModuleDef.h"
#include <string>

namespace SkyGameEngine3d
{
    enum class ELogLevel
    {
        Log,
        Warring,
        Error,
        Fatal,
    };

    inline std::wstring ToString(ELogLevel Level)
    {
        switch (Level)
        {
        case ELogLevel::Log:
            return L"Log";
        case ELogLevel::Warring:
            return L"Warring";
        case ELogLevel::Error:
            return L"Error";
        case ELogLevel::Fatal:
            return L"Fatal";
        }
        return L"None";
    }

    struct CORE_API LogCategory
    {
        explicit LogCategory(std::wstring_view InName)
            : Name(InName)
        {
        }

        const std::wstring& GetName() const
        {
            return Name;
        }

    private:
        std::wstring Name;
    };

    namespace Chrono = std::chrono;

    class CORE_API ILogOutputDevice
        : public std::enable_shared_from_this<ILogOutputDevice>
    {
    public:
        virtual ~ILogOutputDevice() = default;
        using TimePoint = Chrono::system_clock::time_point;

        virtual void
        OutputLog(ELogLevel LogLevel, const TimePoint& Time, const LogCategory& Category, std::wstring_view LogStr) = 0;

        virtual void Init_GameThread() = 0;

        virtual void Destroy_GameThread() = 0;
    };
}
