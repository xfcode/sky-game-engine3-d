#pragma once

#include "LogOutputDevice.h"
#include "LogType.h"

#define SE_LOG(LogCategory, Format, ...) \
    SkyGameEngine3d::LogSubsystem::Get() \
        .Log<SkyGameEngine3d::ELogLevel::Log>(LogCategory, std::source_location::current(), Format, ##__VA_ARGS__)

#define SE_WARRING(LogCategory, Format, ...) \
    SkyGameEngine3d::LogSubsystem::Get() \
        .Log<SkyGameEngine3d::ELogLevel::Warring>(LogCategory, std::source_location::current(), Format, ##__VA_ARGS__)

#define SE_ERROR(LogCategory, Format, ...)\
    SkyGameEngine3d::LogSubsystem::Get() \
        .Log<SkyGameEngine3d::ELogLevel::Error>(LogCategory, std::source_location::current(), Format, ##__VA_ARGS__)

#define SE_FATAL(LogCategory, Format, ...)    \
    SkyGameEngine3d::LogSubsystem::Get() \
        .Log<SkyGameEngine3d::ELogLevel::##Fatal>(LogCategory, std::source_location::current(), Format, ##__VA_ARGS__)

#define DEFINE_EXPORT_LOG_CATEGORY(Category) inline const SkyGameEngine3d::LogCategory G##Category { L#Category };
#define DEFINE_STATIC_LOG_CATEGORY(Category) static const SkyGameEngine3d::LogCategory G##Category { L#Category };
