﻿#pragma once

#include <string>
#include "CoreModuleDef.h"
#include "Misc/UtilityTypes.h"

struct ModuleSpec
{
};

class CORE_API IModuleInterface : public SkyGameEngine3d::CNonCopyable
{
public:
    virtual ~IModuleInterface();

private:
    virtual ModuleSpec GetSpec()
    {
        return ModuleSpec{ };
    }

    virtual void OnLoad()
    {
    }

    virtual void OnUnload()
    {
    }

    friend class ModuleManager;

public:
    void SetupInfo(const std::wstring& InModuleName)
    {
        this->ModuleName = InModuleName;
    }

protected:
    std::wstring ModuleName;
};

#define EXPORT_MODULE(ModuleName, ModuleClass) \
    using ModuleClass = ModuleClass;           \
    static ModuleClass* _Ins = nullptr;\
    extern "C" DLL_EXPORT IModuleInterface* CreateModule_##ModuleName()\
    {\
        static_assert(std::is_base_of_v<IModuleInterface, ModuleClass>, "必须继承至 IModuleInterface");\
        ModuleClass* Ins = new ModuleClass; \
        Ins->SetupInfo(L#ModuleName);         \
        _Ins = Ins;                                \
        return Ins;\
    }                                          \
    extern "C" DLL_EXPORT void DestroyModule_##ModuleName()\
    {\
        delete _Ins;\
        _Ins = nullptr;\
    }
