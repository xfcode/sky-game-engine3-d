﻿#pragma once

#include <string>
#include <map>
#include "CoreModuleDef.h"

class IModuleInterface;

class CORE_API ModuleManager
{
public:
    //! 获取模块管理器
    //! \return
    static ModuleManager& Get();

    //! 获取一个模块
    //! \tparam ModuleClass 模块的类
    //! \param ModuleName 模块的名字
    //! \return
    template <typename ModuleClass = IModuleInterface>
    ModuleClass* GetModule(const std::wstring& ModuleName)
    {
        return dynamic_cast<ModuleClass*>(GetModuleImp(ModuleName));
    }

    //! 初始化 Manager
    void Init();

private:
    IModuleInterface* GetModuleImp(const std::wstring& ModuleName);

    std::map<std::wstring, IModuleInterface*> ModuleMap;
};
