#include <cuchar>
#include <filesystem>

#include "Windows.h"

#ifdef CreateWindow
#undef CreateWindow
#endif

#include "Windows/ApplicationForWindows.h"




int WINAPI wWinMain(HINSTANCE WInstance, HINSTANCE, PTSTR CmdLine, int CmdShow)
{
    constexpr ApplicationForWindows::AppCreateArg Arg;
    Application* App = ApplicationForWindows::Create(Arg);

    return App->Run();
}
